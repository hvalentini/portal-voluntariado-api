var Joi = require('joi');
var Boom = require('boom');
var DB = require('./db');
var noAccents = require('remove-accents');
var types = require('mongoose').Types;
const Filtro = require('../utils/filtro');

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/local',
    config: {
      description: 'Busca de Locais',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      }
    },
    handler: function (request, reply) {
      // console.log('LOCAL ======>', request.query.filtro)
      var ordem = request.query.ordenacao  ? JSON.parse(request.query.ordenacao) : { nomeNormalizado: 1 };
      var filtro = request.query.filtro ? Filtro.formatar(request.query.filtro) : {};
      //
      // console.log('locais', filtro)

      DB.local.find(filtro).sort(ordem).exec(function(err, locais) {
        if (err) return console.error(err);
        reply(locais);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/local/{id}',
    config: {
      description: 'Detalhe de um Local',
      validate: {
        params: {
          id: Joi.string().required().length(24).description('ID do local a ser detalhado!')
        }
      }
    },
    handler: function (request, reply) {
      DB.local.findOne({ _id: request.params.id }, function(err, loc) {
        if (err) return console.error(err);
        if (!loc)
          return reply(Boom.badRequest(`Local de ID [${request.params.id}] inexistente!`));

        reply(loc);
      });
    }
  });

  server.route({
    method: 'DELETE',
    path: '/local/{id}',
    config: {
      description: 'Deleta um Local',
      validate: {
        params: {
          id: Joi.string().required().description('ID do local a ser deletado!')
        }
      }
    },
    handler: function (request, reply) {
      DB.local.findOne({ _id: request.params.id }, function(err, loc) {
        if (err) return console.error(err);
        if (!loc)
          return reply(Boom.badRequest(`Local de ID [${request.params.id}] inexistente!`));

        DB.acao.findOne({ local: loc._id }, function(erroAcao, acaoLocal) {
          if (erroAcao) return console.error(erroAcao);
          if (acaoLocal)
            return reply(Boom.badRequest(`Não é possível excluir o Local, pois ele está vinculado a Ação "${acaoLocal.nome}"`));

          loc.remove();
          reply({ success: true });
        })
      });
    }
  });

  server.route({
    method: 'POST',
    path: '/local',
    config: {
      description: 'Cadastra um Local',
      validate: {
          payload: {
            nome: Joi.string().required().description('Nome do local'),
            endereco: Joi.string().required().description('Endereço do local'),
            telefone: Joi.string().required().description('Telefone do local'),
            responsavelNome: Joi.string().required().description('Responsavel pelo local'),
            responsavelEmail: Joi.string().optional().allow("").description('Email do responsavel pelo local'),
            observacao: Joi.string().optional().allow("").description('Obervações do local'),
            responsavelNome2: Joi.string().optional().allow("").description('Segundo responsavel pelo local'),
            responsavelEmail2: Joi.string().optional().allow("").description('Email do segundo responsavel pelo local'),
            escola: Joi.boolean().required().description('Informa se o local é uma escola'),
            qtdeSalas: Joi.number().optional().allow("").description('Quantidade de salas'),
            qtdeAlunos: Joi.number().optional().allow("").description('Quantidade de alunos'),
            serie:Joi.string().optional().allow("").description('Informa a série/ano'),
            criadoPor: Joi.string().required().description('Criado por')
          }
      },
    },
    handler: function (request, reply) {
      DB.local.find({endereco: { $regex: request.payload.endereco, $options: 'i' }}, function(err, locais) {
        if (err) return console.error(err);
        if (locais.length > 0)
          return reply(Boom.conflict('Já existe um local cadastrado com este endereço!', { success: false }));

        var NOVO = true;
        request.payload['nomeNormalizado'] = noAccents(request.payload.nome).toLowerCase();
        var loc = obtemObjetoLocal(request.payload, new DB.local({}), NOVO);
        loc.save(function(err, res) {
          if (err) return console.error(err);
          reply({ success: true, data: res });
        });
      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/local',
    config: {
      description: 'Edita um Local',
      validate: {
          payload: {
            _id: Joi.string().required().length(24).description('ID do Local'),
            nome: Joi.string().description('Nome do local'),
            endereco: Joi.string().description('Endereço do local'),
            telefone: Joi.string().description('Telefone do local'),
            responsavelNome: Joi.string().description('Responsavel pelo local'),
            responsavelEmail: Joi.string().optional().allow("").description('Email do responsavel pelo local'),
            observacao: Joi.string().optional().allow("").description('Obervações do local'),
            responsavelNome2: Joi.string().optional().allow("").description('Segundo responsavel pelo local'),
            responsavelEmail2: Joi.string().optional().allow("").description('Email do segundo responsavel pelo local'),
            escola: Joi.boolean().description('Informa se o local é uma escola'),
            qtdeSalas: Joi.number().optional().allow(["", null]).description('Quantidade de salas'),
            qtdeAlunos: Joi.number().optional().allow(["", null]).description('Quantidade de alunos'),
            serie:Joi.string().optional().allow(["", null]).description('Informa a série/ano'),
            editadoPor: Joi.string().required().description('Editado por')
          }
      },
    },
    handler: function (request, reply) {
      var query = { $and: [
        { _id: { $not: {$eq: types.ObjectId(request.payload._id) } } },
        { endereco: request.payload.endereco }
      ]}
      DB.local.find(query, function(err, locais) {
        if (err) return console.error(err);
        DB.local.findOne({_id: request.payload._id}).select({_id: 1}).exec(function(err, localId) {
          if (locais.filter(validaEnderecoMesmoID.bind(this, localId._id)).length > 0)
            return reply(Boom.conflict(`Já existe um local cadastrado com o endereço [${request.payload.endereco}]!`, { success: false }));

          var NOVO = false;
          request.payload['nomeNormalizado'] = noAccents(request.payload.nome).toLowerCase();

          var loc = obtemObjetoLocal(request.payload, localId, NOVO);

          loc.save(function(err) {
            if (err) return console.error(err);
            reply({ success: true });
          });
        });
      });
    }
  });

  return next();

}

exports.register.attributes = {
    name: 'local',
    version: require('../package.json').version
};

function validaEnderecoMesmoID(id, local) {
  return local._id !== id;
}

function obtemObjetoLocal(request, loc, novo) {
  Object.keys(request).map(function (key, index) {
    loc[key] = request[key];
  });

  if (novo)
    loc['dataCadastro'] = obtemDataAtual();
  else
    loc['dataEdicao'] = obtemDataAtual();

  return loc;
}

function obtemDataAtual() {
  var now = new Date();
  now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));
  return now.toISOString();
}
