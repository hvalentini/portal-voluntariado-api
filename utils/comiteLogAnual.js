var ASQ = require('asynquence');

module.exports = function(Mongoose, DB, Boom) {
  var module = {};
  module.POST =  function (logAnual, callback) {
    ASQ()
    .then(function(done) {
      const idComite = typeof logAnual.comiteId === 'string' ? { _id : Mongoose.Types.ObjectId(logAnual.comiteId) } : logAnual.comiteId;
      DB.comite.findOne(idComite, (err, comites) => {
        if (err) {
          done(err, null);
        }

        if (!comites) {
          var error = `Comitê [${logAnual.comiteId}] não existe`;
          var response = Boom.badRequest(`Comitê [${logAnual.comiteId}] não existe`);
          done(error, response);
        }

        var now = new Date();
        now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));

        const ANO_ANTERIOR = (now.getFullYear() -1);
        logAnual['ano'] = ANO_ANTERIOR;
        logAnual['dataCadastro'] = now;

        logAnual.save(function(errSave) {
          if (errSave) {
            done(errSave, null);
          }

          response = { success: true };
          done(null, response);
        });
      });
    })
    .then(function(done, error, response) {
      callback(error, response);
      done();
    });
  };

  return module;
}
