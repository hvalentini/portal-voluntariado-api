var Joi = require('joi');
var Boom = require('boom');
var Mongoose = require('mongoose');
var DB = require('./db');
const Filtro = require('../utils/filtro');

exports.register = (server, options, next) => {

  server.route({
    method: 'GET',
    path: '/faixa-etaria',
    config: {
      description: 'Busca de faixas etarias',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      },
    },
    handler: (request, reply) => {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "nome": 1 };
      const filtro = rFiltro.formatar(request.query.filtro);

      DB.faixaEtaria.find(filtro).sort(ordem).exec((err, objects) => {
        if (err) return console.error(err);
        if (!objects) { return reply(Boom.conflict("Não foram encontradas faixas etárias para a pesquisa informada")) }
        reply(objects);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/faixa-etaria/{id}',
    config: {
      description: 'Detalha uma faixa etária pelo id',
    },
    handler: (request, reply) => {
      DB.faixaEtaria.findOne({ _id: request.params.id }).exec((err, object) => {
        if (err) return console.error(err);
        if (!object) { return reply(Boom.conflict("O id da faixa etária informado não foi encontrado")) }
        reply(object);
      })
    }
  });

  server.route({
    method: 'POST',
    path: '/faixa-etaria',
    config: {
      description: 'Cadastra uma faixa etária',
      validate: {
          payload: {
              nome: Joi.string().required().description('nome da faixa etária'),
              valor: Joi.number().description('valor da faixa etária'),
          }
      },
    },
    handler: (request, reply) => {
      const nome = request.payload.nome;
      const query = { nome: { $regex : new RegExp(nome, "i") } }

      DB.faixaEtaria.find(query, (err, objects) => {
        if (err) { return console.error(err); }
        if (objects.length > 0) {
          return reply(Boom.conflict(`Já existe uma faixa etária com o nome ${nome}`));
        }

        const obj = new DB.faixaEtaria(request.payload)
        obj.save((err, docs) => {
          if (err) { return console.error(err); }

          reply({ success: true, data: docs });
        }); // save
      }); // obj.find
    } // handler
  });

  server.route({
    method: 'PUT',
    path: '/faixa-etaria',
    config: {
      description: 'Edita uma faixa etária',
      validate: {
          payload: {
              _id: Joi.string().required().description('id da faixa etária'),
              nome: Joi.string().required().description('nome da faixa etária'),
              valor: Joi.number().description('valor da faixa etária'),
          }
      },
    },
    handler: (request, reply) => {
      const _id = request.payload._id;
      const nome = request.payload.nome;
      const query = { nome: { $regex : new RegExp(nome, "i") } }

      DB.faixaEtaria.find(query, (er, objects) => {
        if (er) { return console.error(er); }
        if (objects.length > 1 || (objects[0] && (_id != objects[0]._id))) {
          return reply(Boom.conflict(`Já existe uma faixa etária com o nome ${nome}`));
        }

        DB.faixaEtaria.findByIdAndUpdate(_id, request.payload).exec()
          .then((object) => {
            reply({ success: true, data: Object.assign(object, request.payload)});
          }, (erro) => {
            console.error(erro);
            reply(Boom.badRequest(erro));
          });
      });
    }
  });

  server.route({
    method: 'DELETE',
    path: '/faixa-etaria/{id}',
    config: {
      description: 'Deleta uma faixa etária',
    },
    handler: (request, reply) => {
      const id = objectId(request.params.id);
      DB.faixaEtaria.findOne({ _id: id }, (er, obj) => {
        if (er) { return console.error(er); }
        if (obj === null) { return reply(Boom.conflict("O id da faixa etária informada não foi encontrada")); }

        DB.voluntario.findOne({ faixaEtaria: id }, (erro, voluntario) => {
          if (erro) { return console.error(erro); }
          if (voluntario !== null) { return reply(Boom.conflict("O id da faixa etária informada está vinculado a um voluntário")); }

          obj.remove((err) => {
            if (err) return console.error(err);
            reply({ success: true, message: 'Faixa etária excluída com sucesso'});
          });
        }); // voluntario
      }); //obj
    } // handler
  }); // route

  function objectId(id) {
    return Mongoose.Types.ObjectId(id);
  }

  return next();
}

exports.register.attributes = {
    name: 'faixaEtaria',
    version: require('../package.json').version
};
