var DB = require('./db.js');
var Joi = require('joi');
var Mongoose = require('mongoose');
var Boom = require('boom');
const ComiteLogAnual = require('./../utils/comiteLogAnual.js')(Mongoose, DB, Boom);

exports.register = function (server, options, next) {

  server.route({
    method: 'POST',
    path: '/comite-log-anual',
    config: {
      description: 'Cadastro de Logs Anuais do Comitê',
      validate: {
        payload: {
          comiteId: Joi.string().required().length(24).description('ID do comitê'),
          verbaInicial: Joi.number().required().description('Verba inicial'),
          verbaExtra: Joi.number().description('Verba extra'),
          liderSocial: Joi.object().keys({
            voluntarioId: Joi.string().required().description('ID do voluntário'),
            nome: Joi.string().description('Nome do voluntário'),
            urlAvatar: Joi.string().description('Url do Avatar'),
            cargo: Joi.string().description('Cargo do voluntário'),
            email: Joi.string().description('Email do voluntário')
          }).description('Voluntario resumido do Lider Social'),
          saldoRemanescente: Joi.number().required().description('Saldo Remanescente')
        }
      }
    },
    handler: function(request, reply) {
      var now = new Date();
      now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));

      var logAnual = new DB.comiteLogAnual(request.payload, reply);
      const ANO_ANTERIOR = (now.getFullYear() -1);
      logAnual['ano'] = ANO_ANTERIOR;
      logAnual['dataCadastro'] = now.toISOString();

      ComiteLogAnual.POST(logAnual, function(err, response) {
        if (err) console.error(err);

        reply(response);
      });
    }
  });

  return next();
}

exports.register.attributes = {
    name: 'comiteLogAnual',
    version: require('../package.json').version
};
