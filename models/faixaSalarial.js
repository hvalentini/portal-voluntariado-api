module.exports = (Mongoose) => {
  const schema = new Mongoose.Schema({
    'nome': { type: String, required: true },
    'valor': { type: Number },
    '__v': { type: Number, select: false},
  });

  return Mongoose.model('faixaSalarial', schema, 'faixasSalariais');
}
