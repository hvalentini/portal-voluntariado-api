var Joi = require('joi');
var DB = require('./db');
var Mongoose = require('mongoose');
var types = require('mongoose').Types;
var Boom = require('boom');
const findIndex = require('lodash/findIndex');
const Filtro = require('../utils/filtro');
var noAccents = require('remove-accents');

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/tipo-de-acao',
    config: {
      description: 'Busca os Tipos de Ação cadastrados',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta'),
          naoOrdernadarPersonalizado: Joi.bool().optional().allow('').description('Flag para não fazer ordenacao customizada'),
        }
      }
    },
    handler: function (request, reply) {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : {nomeNormalizado: 1, exclusivoParaLider: 1, informarQtdeCartas: 1};
      const filtro = request.query.filtro ? Filtro.formatar(request.query.filtro, '_id') : {};

      DB.tipoDeAcao.find(filtro).sort(ordem).exec(function(err, acoes) {
        if (err) return console.error(err);
        var index, endArray = []
        if(!request.query.naoOrdernadarPersonalizado) {
          while((index = findIndex(acoes, o => o.nome.indexOf('Outra') >= 0 )) >= 0) {
            endArray = endArray.concat(acoes.splice(index, 1))
          }
        }
        return reply([].concat(acoes, endArray))
      });
    }
  });


  server.route({
    method: 'GET',
    path: '/tipo-de-acao/{id}',
    config: {
      description: 'Detalha um Tipo de Ação',
      validate: {
        params: {
          id: Joi.string().min(24).max(24).required().description('Id da ação é requerido'),
        }
      },
    },
    handler: function (request, reply) {
      DB.tipoDeAcao.findOne({ _id: request.params.id }, function(err, acoes) {
        if (acoes === null) {
          return reply(Boom.conflict(`Não há nenhuma ação com o ID: ${request.params.id} solicitado.`));
        }
        if (err) return console.error(err);
        return reply(acoes);
      })
    }
  });


  server.route({
    method: 'POST',
    path: '/tipo-de-acao',
    config: {
      description: 'Cadastra um Tipo de Ação',
      validate: {
          payload: {
              nome: Joi.string().required().description('Nome da ação'),
              exclusivoParaLider: Joi.boolean().required().description('Ação exclusiva para Líder'),
              informarQtdeCartas: Joi.boolean().required().description('Ação que necessita do número de cartas'),
              tipo: Joi.number().required().description('Tipo do tipo de ação')
          }
      },
    },
    handler: function (request, reply) {
      const nome = request.payload.nome.replace(/^\s+|\s+$/g,"");
      const exclusivoParaLider = request.payload.exclusivoParaLider;
      const informarQtdeCartas = request.payload.informarQtdeCartas;
      const tipo = request.payload.tipo;
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")

      DB.tipoDeAcao.find({nome: { $regex : new RegExp(nome, "i") }}, function(err, acoesNome) {
        if (acoesNome.length > 0) {
          return reply(Boom.conflict(`A ação ${nome} já está cadastrada no sistema`));
        }
        var acao = new DB.tipoDeAcao({ nome, exclusivoParaLider, informarQtdeCartas, tipo, nomeNormalizado })
        acao.save(function(err, res) {
          if (err) return console.error(err);
          return reply({ res, message: 'O tipo de ação foi criado com sucesso.' });
        })
      })
    }
  });


  server.route({
    method: 'PUT',
    path: '/tipo-de-acao',
    config: {
      description: 'Edita um Tipo de Ação',
      validate: {
          payload: {
              _id: Joi.string().min(24).max(24).required().description('Id da ação é requerido'),
              nome: Joi.string().required().description('Nome do usuário'),
              exclusivoParaLider: Joi.boolean().required().description('Ação exclusiva para Líder'),
              informarQtdeCartas: Joi.boolean().required().description('Ação que necessita do número de cartas'),
              tipo: Joi.number().required().description('Tipo do tipo de ação'),
              nomeNormalizado: Joi.string().optional().description('Nome normalizado'),
          }
      },
    },
    handler: function (request, reply) {
      const nome = request.payload.nome.replace(/^\s+|\s+$/g,"");
      const exclusivoParaLider = request.payload.exclusivoParaLider;
      const informarQtdeCartas = request.payload.informarQtdeCartas;
      const tipo = request.payload.tipo;
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")

      const id = {
        _id: Mongoose.Types.ObjectId(request.payload._id),
      }
      var editarAcao = {
        $set: {
          nome: nome,
          exclusivoParaLider: exclusivoParaLider,
          informarQtdeCartas: informarQtdeCartas,
          tipo: tipo,
          nomeNormalizado: nomeNormalizado,
        }
      }

      DB.tipoDeAcao.findOne(id, (error, tipoAcaoBd) => {
        if (error) { throw error }
        if(!tipoAcaoBd) {
          return reply(Boom.conflict(`O tipo de ação ${nome} não foi encontrado.`));
        }
        DB.tipoDeAcao.find({
          _id: { $ne: id._id },
          tipo: { $ne: tipo },
          nome: nome,
        }, (erro, val) => {
          if (erro) { throw erro }
          if (val.length > 0) {
            return reply(Boom.conflict(`Já exite um tipo de ação com o nome "${nome}"`));
          }
          if (tipo !== tipoAcaoBd.tipo) {
            DB.acao.findOne({tipoAcao: id._id}, (error, acao) => {
              if (error) { throw error }
              if (acao) {
                return reply(Boom.conflict(`Já existem ações cadastradas com este tipo de ação`));
              }
              DB.tipoDeAcao.update( id, editarAcao, function(err, res) {
                if (err){ throw err }
                if (res.nModified < 1 ) { return reply({ res, message: 'O tipo de ação não foi editado.' }) };

                DB.acao.find({tipoAcao: id._id}, (error, acoes) => {
                  if (error) { throw error }
                  if(acoes.length < 1) { return reply({ res, message: 'O tipo de ação foi editado com sucesso.' }) }
                  const control = []
                  const updatedValue = {
                    $set: {
                      nomeTipoAcao: nome,
                    }
                  }
                  acoes.forEach(element => {
                    (function(control, element, value, types, total){
                      DB.acao.update({_id: types.ObjectId(element._id)}, value, (error, doc) => {
                        if (error){ throw error }
                        control.push(doc)
                        if(control.length === total) { return reply({ res, message: 'O tipo de ação foi editado com sucesso.' }) }
                      });
                    })(control, element, updatedValue, Mongoose.Types, acoes.length)
                  });
                });
              });
            })
          } else {
              DB.tipoDeAcao.update( id, editarAcao, function(err, res) {
                if (err){ throw err }
                if (res.nModified < 1 ) return reply({ res, message: 'O tipo de ação não foi editado.' });

                DB.acao.find({tipoAcao: id._id}, (error, acoes) => {
                  if (error) { throw error }
                  if(acoes.length < 1) { return reply({ res, message: 'O tipo de ação foi editado com sucesso.' }) }
                  const control = []
                  const updatedValue = {
                    $set: {
                      nomeTipoAcao: nome,
                    }
                  }
                  acoes.forEach(element => {
                    (function(control, element, value, types, total){
                      DB.acao.update({_id: types.ObjectId(element._id)}, value, (error, doc) => {
                        if (error){ throw error }
                        control.push(doc)
                        if(control.length === total) { return reply({ res, message: 'O tipo de ação foi editado com sucesso.' }) }
                      });
                    })(control, element, updatedValue, Mongoose.Types, acoes.length)
                  });
                });
              });
          }
        });
      });
    }
  });


  server.route({
    method: 'DELETE',
    path: '/tipo-de-acao/{id}',
    config: {
      description: 'Deleta um Tipo de Ação',
    },
    handler: function (request, reply) {
      console.log(request.params.id)
      var id = Mongoose.Types.ObjectId(request.params.id)

      DB.tipoDeAcao.find({ _id: id }, (error, removerAcao) => {
        if (removerAcao.length === 0) {
          return reply(Boom.conflict(`Não há nenhum tipo de ação com o ID: ${request.params.id} solicitado.`));
        }

        DB.acao.findOne({ tipoAcao: id }, function(erro, acaoRelacionada) {
          if (erro) return console.error(erro);
          if (acaoRelacionada !== null) {
            return reply(Boom.conflict(`Há uma ação relacionada a este tipo de ação. Não é possível exclui-la.`));
          }

          DB.tipoDeAcao.remove({ _id: id }, function(err, res) {
            if (err) return console.error(err);
            return reply({ res, message: 'O tipo de ação foi excluído com sucesso.' });
          })
        })
      })
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/tipo-de-acao/normalizar',
    config: {
      description: 'Normaliza nome de tipo de ação',
    },
    handler: function (request, reply) {
      DB.tipoDeAcao.find({}, (err, docs) => {
        if (err) { throw err }

        if(docs.length < 1) { return reply(Boom.unauthorized("Não a tipo de ação cadastrada"))}

        var control = []

        docs.forEach((type) => {
          console.log('type =====>', type);
          (function(control){
            var updateValue = {
              $set: {
                nomeNormalizado: noAccents(type.nome).toLowerCase(),
              }
            }
            DB.tipoDeAcao.update({ _id: types.ObjectId(type._id) }, updateValue, (err, resp) => {
              control.push(resp)
              if(control.length === docs.length) { return reply({ message: "Tipos de ações normalizados com sucesso" }) }
            })
          })(control)
        });
      });
    }
  });

  return next();

}


exports.register.attributes = {
    name: 'tipoDeAcao',
    version: require('../package.json').version
};

function obtemFiltro(query){
  var filtro = {};

  Object.keys(query).map(function(key, index) {
    console.log(key, index);

    if (key === '_id')
      filtro[key] = Mongoose.Types.ObjectId(query[key])
    else if(typeof query[key] === 'number')
      filtro[key] = { $gte: query[key] };
    else if (typeof query[key] === 'string')
      filtro[key] = { $regex: query[key], $options: 'i'};
    else
      filtro[key] = query[key];
  });

  return filtro;
}
