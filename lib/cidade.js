const Joi = require('joi');
const DB = require('./db');
const Boom = require('boom');
var types = require('mongoose').Types;
const Mongoose = require('mongoose');
const Filtro = require('../utils/filtro');
var noAccents = require('remove-accents');

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/cidade',
    config: {
      description: 'Busca de todas as cidades',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      }
    },
    handler: (request, reply) => {
      var ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : {nomeNormalizado: 1};
      // console.log('CIDADE', request.query.filtro);
      var filtro = Filtro.formatar(request.query.filtro, ['empresa._id','cidade']);
      // console.log('cidades', filtro)

      DB.cidade.find(filtro).sort(ordem).exec((err, cidades) => {
        if (err) return console.error(err);
        reply(cidades);
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/cidade/{id}',
    config: {
      description: 'Busca de cidade por id',
      validate: {
          params: {
            id: Joi.string().length(24).required().description('ID da cidade')
          }
      },
    },
    handler: (request, reply) => {
      const _id = request.params.id;

      DB.cidade.findOne({_id},(err, cidades) => {
        if (cidades === null) {
         return reply(Boom.conflict('Não existe cidade para esse ID')) ;
        }
        if (err) return console.error(err);
        reply(cidades);
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/cidade/uf',
    config: {
      description: 'Busca de todas UFs ja cadastradas',
      validate: {
        query: {
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      }
    },
    handler: (request, reply) => {
      var ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : {_id: 1};

       DB.cidade.aggregate(
         [
             { '$group': { '_id': '$uf' }},
             { '$sort': ordem}
         ])
         .exec(function(err,uf) {
             if (err) {
              return console.error(err);
             }
             var obj = [];
             uf.map(item => {
                obj.push({'uf': item._id});
              });
            reply(obj);
         });
    }
  });

  server.route({
    method: 'POST',
    path: '/cidade',
    config: {
      description: 'Cadastra uma cidade',
      validate: {
          payload: {
            nome: Joi.string().required().description('Nome da cidade'),
            uf: Joi.string().required().description('UF da cidade')
          }
      },
    },
    handler: (request, reply) => {
      const nome = request.payload.nome.replace(/^\s+|\s+$/g,"");
      const uf = request.payload.uf;
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")

      DB.cidade.find({ nomeNormalizado: nomeNormalizado,
                       uf: { $regex : new RegExp(uf, "i") }}, (err, cidades) => {
        if (cidades.length > 0) {
          return reply(Boom.conflict(`A cidade ${nome} já está cadastrada para ${uf}`));
        }

        const cidade = new DB.cidade({ nome, uf, nomeNormalizado });
        cidade.save((err, doc) => {
          if (err) { return console.error(err); }
          return reply(doc);
        });
      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/cidade',
    config: {
      description: 'Cadastra uma cidade',
      validate: {
          payload: {
            nome: Joi.string().required().description('Nome da cidade'),
            uf: Joi.string().required().description('UF da cidade'),
            _id: Joi.string().required().description('ID da cidade'),
          }
      },
    },
    handler: (request, reply) => {
      const query = { _id : Mongoose.Types.ObjectId(request.payload._id) };
      const nome = request.payload.nome.replace(/^\s+|\s+$/g,"");
      const uf = request.payload.uf;
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"");
      const queryCidade = { "cidade": Mongoose.Types.ObjectId(request.payload._id) };

      var editarCidade = {
        $set: {
          nome: request.payload.nome.replace(/^\s+|\s+$/g,""),
          uf: request.payload.uf,
          nomeNormalizado: nomeNormalizado.replace(/^\s+|\s+$/g,""),
        }
      }

      DB.cidade.find({ _id: { $ne: query._id },
                       nome: { $regex : new RegExp(nome, "i") },
                       uf: { $regex : new RegExp(uf, "i") }}, (err, cidades) => {
        if (cidades.length > 0) {
          return reply(Boom.conflict(`A cidade ${nome} já está cadastrada para ${uf}`));
        }

        DB.comite.find(queryCidade, (errComite, docComite) => {
          if (errComite) throw errComite;

          if(docComite.length > 0) {
            docComite.map((item) => {
              item.cidadeNome = request.payload.nome
              item.cidadeUF = request.payload.uf
              item.save((err, res) => {
                if (err) throw err;
              });
            })
          }
        })

        DB.cidade.update(query, editarCidade, (err, res) => {
          if (err) return console.error(err);
          reply({ res });
        })
      })
    }
  });

  server.route({
    method: 'DELETE',
    path: '/cidade',
    config: {
      description: 'Deleta uma cidade',
      validate: {
          query: {
            _id: Joi.string().required().length(24).description('ID da cidade')
          }
      },
    },
    handler: (request, reply) => {
      const query = { cidade : request.query._id };

      DB.cidade.find({ _id: Mongoose.Types.ObjectId(request.query._id) },(err, cidades) => {
        if (cidades.length === 0) {
         return reply(Boom.conflict(`Cidade de ID: "${request.query._id}" não existe.`)) ;
        }

        DB.comite.findOne(query, (err, comites) => {
          if (comites !== null) {
            return reply(Boom.conflict('Não foi possível exclui-la, pois a cidade está associada a um comite'));
          }

          DB.cidade.remove({ _id: Mongoose.Types.ObjectId(request.query._id) }, (err, res) => {
            if (err) return console.error(err);
            return reply({res, message:'Cidade excluída com sucesso.'});
          });

        });
      });
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/cidade/normalizar',
    config: {
      description: 'Normaliza nome de cidades',
    },
    handler: function (request, reply) {
      DB.cidade.find({}, (err, docs) => {
        if (err) { throw err }

        if(docs.length < 1) { return reply(Boom.unauthorized("Não a empresas cadastradas"))}

        var control = []

        docs.forEach((city) => {
          (function(control){
            var updateValue = {
              $set: {
                nomeNormalizado: noAccents(city.nome).toLowerCase(),
              }
            }
            DB.cidade.update({ _id: types.ObjectId(city._id) }, updateValue, (err, resp) => {
              control.push(resp)
              if(control.length === docs.length) { return reply({ message: "Cidades normalizadas com sucesso" }) }
            })
          })(control)
        });
      });
    }
  });
  return next();

}

exports.register.attributes = {
    name: 'cidade',
    version: require('../package.json').version
};
