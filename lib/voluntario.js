var ASQ             = require('asynquence');
var DB              = require('./db');
var Boom            = require('boom');
var validaCPF       = require('./../utils/validaCPF.js')(Boom);
var cargaHoraria    = require('../utils/carga-horaria.js')(DB, Boom);
var Crypto          = require('crypto');
var Filtro          = require('../utils/filtro');
var fs              = require('fs');
var geraCertificado = require('../utils/gera-certificado.js');
var Joi             = require('joi');
var loginv2         = require('./../utils/loginV2.js');
var mail            = require('../utils/send-mail.js');
var noAccents       = require('remove-accents');
var path            = require('path');
var sr              = require('simple-random');
var types           = require('mongoose').Types;

exports.register = (server, options, next) => {

  server.route({
    method: 'POST',
    path: '/voluntario',
    config: {
      description: 'Cadastra um voluntario',
      validate: {
        payload: {
          comite: Joi.string().required().description('id do comite'),
          cpf: Joi.string().required().description('cpf do voluntario'),
          nome: Joi.string().required().description('nome do voluntario'),
          nomeNormalizado: Joi.string().optional().allow("").description('nome do voluntario completo'),
          cargoNome: Joi.string().required().description('nome do cargo do voluntario'),
          cargoId: Joi.number().required().description('id do cargo do voluntario'),
          centroResultadoNome: Joi.string().required().description('nome do centro de resultado'),
          centroResultadoId: Joi.number().optional().allow(null).description('id do centro de resultado'),
          email: Joi.string().required().description('email do voluntario'),
          emailSuperior: Joi.string().required().description('email do superior'),
          telefoneProfissional: Joi.string().optional().allow("").description('numero do telefone profissional do voluntario'),
          telefonePessoal: Joi.string().optional().allow("").description('numero do telefone pessoal do voluntario'),
          endereco: Joi.string().required().description('endereco do voluntario'),
          bairro: Joi.string().required().description('bairro do voluntario'),
          cidade: Joi.string().required().description('id da cidade'),
          telefonePessoal: Joi.string().optional().allow("").description('numero do telefone pessoal do voluntario'),
          genero: Joi.string().required().description('genero do voluntario'),
          faixaEtaria: Joi.object().optional().allow(null).description('faixa etaria do voluntario'),
          faixaSalarial: Joi.object().optional().allow(null).description('faixa salarial do voluntario'),
          escolaridade: Joi.string().optional().allow(null).description('escolaridade voluntario'),
          tamanhoCamiseta: Joi.string().required().description('tamanho da camiseta do voluntario'),
          associadoExecutivo: Joi.boolean().required().description('Se o voluntario e um executivo'),
          aceitouTermo: Joi.boolean().required().description('Se o voluntario aceitou o termo'),
          aceitouTermoEsseAno: Joi.boolean().optional().allow("").description('Se o voluntario aceitou o termo no ano atual'),
          usuarioSharePoint: Joi.boolean().required().description('Se o o voluntario possui email corporativo'),
          cadastradoPeloLider: Joi.boolean().required().description('Se foi cadastrado pelo lider social'),
          urlAvatar: Joi.string().optional().allow("").description('url do avatar do voluntario'),
          senha: Joi.string().optional().allow("").description('senha do voluntario'),
          senhaTemp: Joi.string().optional().allow([null, '']).description('senha temporaria do voluntario'),
          bloqueado: Joi.boolean().optional().allow("").description('se o usuario esta bloqueado'),
          dataCadastro: Joi.date().optional().description('data do cadastro'),
          dataEdicao: Joi.date().optional().allow(null).description('data da última edição'),
          dataUltimoAcesso: Joi.date().optional().allow(null).description('data da última edição'),
          jaParticipouAcao: Joi.boolean().optional().allow(null).description('se o usuario já participou de alguma ação'),
          jaParticipouAcaoEsseAno: Joi.boolean().optional().allow(null).description('se o usuario já participou de alguma ação neste ano'),
        }
      }
    },
    handler: (request, reply) => {

      if(!request.payload.aceitouTermo) {
        return reply(Boom.unauthorized('É obrigatorio aceitar o termo de compromisso'));
      }

      if(request.payload.usuarioSharePoint){
        if(request.payload.senha || request.payload.senhaTemp || request.payload.cadastradoPeloLider ){
          return reply(Boom.unauthorized(`Usuário do SharePoint não pode conter
            ${request.payload.senha ? 'senha,' : ''}
            ${request.payload.senha ? 'senha temporaria,' : ''}
            ${request.payload.senha ? 'ser cadastrado por um lider' : ''}`));
        }
      }
      var nomeNormalizado = noAccents(request.payload.nome).toLowerCase();
      var date = obtemDataAtual();
      var vont = new DB.voluntario({
        comite: request.payload.comite,
        cpf: request.payload.cpf,
        nome: request.payload.nome,
        nomeNormalizado: nomeNormalizado,
        cargoNome: request.payload.cargoNome,
        cargoId: request.payload.cargoId,
        centroResultadoNome: request.payload.centroResultadoNome,
        centroResultadoId: request.payload.centroResultadoId,
        email: request.payload.email,
        emailSuperior: request.payload.emailSuperior,
        telefoneProfissional: request.payload.telefoneProfissional,
        telefonePessoal: request.payload.telefonePessoal,
        endereco: request.payload.endereco,
        bairro: request.payload.bairro,
        cidade: request.payload.cidade,
        genero: request.payload.genero,
        faixaEtaria: request.payload.faixaEtaria,
        faixaSalarial: request.payload.faixaSalarial,
        escolaridade: request.payload.escolaridade,
        tamanhoCamiseta: request.payload.tamanhoCamiseta,
        associadoExecutivo: request.payload.associadoExecutivo,
        aceitouTermo: request.payload.aceitouTermo,
        aceitouTermoEsseAno: true,
        usuarioSharePoint:request.payload.usuarioSharePoint,
        cadastradoPeloLider: request.payload.cadastradoPeloLider,
        urlAvatar: request.payload.urlAvatar,
        senha: request.payload.senha
                      ? Crypto.createHash('md5').update(request.payload.senha).digest('hex')
                      : "",
        senhaTemp: request.payload.senhaTemp
                      ? Crypto.createHash('md5').update(request.payload.senhaTemp).digest('hex')
                      : "",
        bloqueado: false,
        jaParticipouAcao: false,
        jaParticipouAcaoEsseAno: false,
        dataCadastro: date,
        dataUltimoAcesso: date
      })
      if (!vont.usuarioSharePoint && !vont.senhaTemp) {
        request.payload.senhaTemp = Math.random().toString(36).slice(-8)
        vont.senhaTemp = Crypto.createHash('md5').update(request.payload.senhaTemp).digest('hex')
      }
      vont.save(function(err, doc) {
        if (err){
          if(err.code === 11000 ){
            return reply(Boom.unauthorized(`
              ${err.message.match(/cpf/g)? 'CPF' : '' }
              ${err.message.match(/email/g)? 'Email' : '' }
              já cadastrado`));
          }
          throw err;
        }

        ASQ(doc)
        .all(
          (done, doc) => enviaNotificacao(done, doc),
          (done, doc) => sendMailRegisterVol(done, doc, request.payload.senhaTemp),
          (done, doc) => sendMailLiderRegisterVol(done, doc)
        )
        .val(() => {
          reply({ success: true, data: doc })
        })

      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/voluntario',
    config: {
      description: 'Edita um voluntario',
      validate: {
        payload: {
          _id: Joi.string().required().description('id do voluntario'),
          comite: Joi.string().required().description('id do comite'),
          cpf: Joi.string().required().description('cpf do voluntario'),
          nome: Joi.string().required().description('nome do voluntario'),
          nomeNormalizado: Joi.string().optional().allow("").description('nome do voluntario completo'),
          cargoNome: Joi.string().required().description('nome do cargo do voluntario'),
          cargoId: Joi.number().required().description('id do cargo do voluntario'),
          centroResultadoNome: Joi.string().required().description('nome do centro de resultado'),
          centroResultadoId: Joi.number().optional().allow(null).description('id do centro de resultado'),
          email: Joi.string().required().description('email do voluntario'),
          emailSuperior: Joi.string().required().description('email do superior'),
          telefoneProfissional: Joi.string().optional().allow("").description('numero do telefone profissional do voluntario'),
          telefonePessoal: Joi.string().optional().allow("").description('numero do telefone pessoal do voluntario'),
          endereco: Joi.string().required().description('endereco do voluntario'),
          bairro: Joi.string().required().description('bairro do voluntario'),
          cidade: Joi.string().required().description('id da cidade'),
          genero: Joi.string().required().description('genero do voluntario'),
          faixaEtaria: Joi.object().optional().allow(null).description('faixa etaria do voluntario'),
          faixaSalarial: Joi.object().optional().allow(null).description('faixa salarial do voluntario'),
          escolaridade: Joi.string().optional().allow([null, '']).description('escolaridade do voluntario'),
          tamanhoCamiseta: Joi.string().required().description('tamanho da camiseta do voluntario'),
          associadoExecutivo: Joi.boolean().required().description('Se o voluntario e um executivo'),
          aceitouTermo: Joi.boolean().required().description('Se o voluntario aceitou o termo'),
          aceitouTermoEsseAno: Joi.boolean().optional().allow("").description('Se o voluntario aceitou o termo no ano atual'),
          usuarioSharePoint: Joi.boolean().required().description('Se o o voluntario possui email corporativo'),
          cadastradoPeloLider: Joi.boolean().required().description('Se foi cadastrado pelo lider social'),
          urlAvatar: Joi.string().optional().allow("").description('url do avatar do voluntario'),
          senha: Joi.string().optional().allow("").description('senha do voluntario'),
          senhaTemp: Joi.string().optional().allow([null, '']).description('senha temporaria do voluntario'),
          bloqueado: Joi.boolean().optional().allow("").description('se o usuario esta bloqueado'),
          dataCadastro: Joi.date().description('data do cadastro'),
          dataEdicao: Joi.date().optional().allow(null).description('data da última edição'),
          dataUltimoAcesso: Joi.date().optional().allow(null).description('data da última edição'),
          jaParticipouAcao: Joi.boolean().optional().allow(null).description('se o usuario já participou de alguma ação'),
          jaParticipouAcaoEsseAno: Joi.boolean().optional().allow(null).description('se o usuario já participou de alguma ação neste ano'),
        }
      }
    },
    handler: function (request, reply) {

      if(!request.payload.aceitouTermo) {
        return reply(Boom.unauthorized('É obrigatorio aceitar o termo de compromisso'));
      }

      if(request.payload.usuarioSharePoint){
        if(request.payload.senha || request.payload.senhaTemp || request.payload.cadastradoPeloLider ){
          return reply(Boom.unauthorized(`Usuário do SharePoint não pode conter
            ${request.payload.senha ? 'senha,' : ''}
            ${request.payload.senhaTemp ? 'senha temporaria,' : ''}
            ${request.payload.cadastradoPeloLider ? 'ser cadastrado por um lider' : ''}`));
        }
      } 

      ASQ()
        .then((done) => {
          if (request.payload.usuarioSharePoint) {
            return done();
          }

          DB.voluntario.findById(request.payload._id, (err, doc) => {
            return done();
          });
        })
        .then((done) => {
          DB.voluntario.find({_id: types.ObjectId(request.payload._id)}, (err, res) => {
            const termo = res.aceitouTermo ? res.aceitouTermo : request.payload.aceitouTermoEsseAno;
            return done(termo)
          });
        })
        .then((done, termo) => {
          var nomeNormalizado = noAccents(request.payload.nome).toLowerCase();
          var date = obtemDataAtual();
          var volunt = {
            $set: {
              comite: request.payload.comite,
              cpf: request.payload.cpf,
              nome: request.payload.nome,
              nomeNormalizado: nomeNormalizado,
              cargoNome: request.payload.cargoNome,
              cargoId: request.payload.cargoId,
              centroResultadoNome: request.payload.centroResultadoNome,
              centroResultadoId: request.payload.centroResultadoId,
              email: request.payload.email,
              emailSuperior: request.payload.emailSuperior,
              telefoneProfissional: request.payload.telefoneProfissional,
              telefonePessoal: request.payload.telefonePessoal,
              endereco: request.payload.endereco,
              bairro: request.payload.bairro,
              cidade: request.payload.cidade,
              genero: request.payload.genero,
              faixaEtaria: request.payload.faixaEtaria,
              faixaSalarial: request.payload.faixaSalarial,
              escolaridade: request.payload.escolaridade,
              tamanhoCamiseta: request.payload.tamanhoCamiseta,
              associadoExecutivo: request.payload.associadoExecutivo,
              aceitouTermo: termo,
              aceitouTermoEsseAno: request.payload.aceitouTermoEsseAno,
              usuarioSharePoint:request.payload.usuarioSharePoint,
              cadastradoPeloLider: request.payload.cadastradoPeloLider,
              urlAvatar: request.payload.urlAvatar,
              senha: request.payload.senha
                              ? Crypto.createHash('md5').update(request.payload.senha).digest('hex')
                              : "",
              senhaTemp: request.payload.senhaTemp
                              ? Crypto.createHash('md5').update(request.payload.senhaTemp).digest('hex')
                              : "",
              bloqueado: request.payload.bloqueado,
              jaParticipouAcao: request.payload.jaParticipouAcaoEsseAno ? true : "",
              jaParticipouAcaoEsseAno: request.payload.jaParticipouAcaoEsseAno,
              dataEdicao: date,
            }
          };
          if (!volunt.$set.usuarioSharePoint && !volunt.$set.senhaTemp) {
            request.payload.senhaTemp = Math.random().toString(36).slice(-8)
            volunt.$set.senhaTemp = Crypto.createHash('md5').update(request.payload.senhaTemp).digest('hex')
          }

          DB.voluntario.update({_id: types.ObjectId(request.payload._id)}, volunt, (err, res) => {
            if (err) throw err;

            return done(res, termo);
          });
        })
        .then((done, res, termo) => {
          if(!termo) { return done.fail(res) }
          done(request.payload, res)
        })
        .all(
          (done, doc) => enviaNotificacao(done, doc),
          (done, doc, res) => sendMailRegisterVol(done, doc, request.payload.senhaTemp, res),
          (done, doc, res) => sendMailLiderRegisterVol(done, doc, res)
        )
        .val((res1, res2) => {
          reply(res1 || res2);
        })
        .or(res => {
          reply(res);
        })
        .onerror((msg) => {
          return reply(Boom.badRequest(msg));
        });
    }
  });

  server.route({
    method: 'GET',
    path: '/voluntario/{id}',
    config: {
      description: 'Detalha um voluntario',
      validate: {
        params: {
          id: Joi.string().min(24).max(24).required().description('Id do voluntario'),
        }
      }
    },
    handler: ( request, reply ) => {
      const volunt = DB.voluntario;

        volunt.findById(request.params.id)
        .populate({path: 'comite', populate: {path: 'empresa'}})
        .populate('faixaEtaria faixaSalarial cidade')
        .exec((err, doc) => {
          if (err) throw err;

          if (!doc) {
            return reply(Boom.conflict(`Não foi encontrado nenhum voluntário com o id: ${request.params.id}`))
          }

          return reply(doc);
        });
    }
  });

  server.route({
    method: 'GET',
    path: '/voluntario',
    config: {
      description: 'Busca voluntarios'
    },
    handler: ( request, reply ) => {
      const volunt = DB.voluntario;
      const acoes = DB.acao;
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { nome: 1 };
      delete request.query.ordenacao;
      const filter = Filtro.formatar(request.query.filtro, ['faixaEtaria._id', 'faixaSalarial._id', 'comite'])
      volunt.find(filter)
        .sort(ordem)
        .lean()
        .populate({path: 'comite', populate: {path: 'empresa'}})
        .populate('faixaEtaria faixaSalarial cidade')
        .exec((err, docs) => {
        if(err) throw err;
        const promises = docs.map((item) => {
          return new Promise(function(resolve, reject) {
              acoes.aggregate([
                {
                  $match: {
                    participantes: { $elemMatch: {"voluntarioId" : item._id.toString()} },
                  }
                },
                {  $group: {
                  _id: null,
                  soma: { $sum: 1 }
                }
              }
            ],
              (err,qtd) => {
                if (err) throw err;
                item.quantidadeDeParticipacoes = qtd[0] ? qtd[0].soma : 0
                resolve();
              })
            })
        })
        Promise.all(promises)
        .then(function() {
          return reply(docs);
        })
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/voluntario/emitir-certificado',
    config: {
      description: 'Gera certificado em pdf',
      validate: {
          query: {
              voluntarioId: Joi.string().required().description('ID do voluntário é requerido'),
              dataInicialFiltro: Joi.string().required().description('Data incial'),
              dataFinalFiltro: Joi.string().required().description('Data final'),
          }
      },
    },
    handler: (request, reply) => {
      var volunt = DB.voluntario;

      volunt.findById(request.query.voluntarioId).populate('faixaEtaria faixaSalarial cidade').exec((err, doc) => {
        if(err) throw err;

        if(!doc){
          return reply(Boom.badRequest(`Voluntario com o id ${request.query.voluntarioId}, não foi encontrado.`))
        }

        cargaHoraria.POST(request.query, (erro, resp) => {
          if(err) throw err;

          if(!resp || resp.length < 1 ){
            return reply(Boom.badRequest("Não possui horas suficientes para gerar um certificado, no periodo especifiado"))
          }

          const dataIni = request.query.dataInicialFiltro.split('-')
          const dataFim = request.query.dataFinalFiltro.split('-')
          var opts = {
            nome: doc.nome,
            horas: resp[0].soma,
            dataIni: new Date(dataIni[0], dataIni[1] - 1, dataIni[2]),
            dataFim: new Date(dataFim[0], dataFim[1] - 1, dataFim[2]),
            cpf: doc.cpf
          }

          if(opts.horas > 0 ){
            geraCertificado(opts, (erroCertificado, path) => {
              if(erroCertificado) throw erroCertificado;

              return reply.file(path, {mode: 'attachment'})
            })
          } else {
            return reply(Boom.badRequest("Não possui horas suficientes para gerar um certificado, no periodo especifiado"))
          }
        });
      })
    }
  })

  server.route({
    method: 'GET',
    path: '/voluntario/quantidade',
    config: {
      description: 'Busca e conta a quantidade de voluntarios que cumprem determinada regra'
    },
    handler: (request, reply) => {
      const volunt = DB.voluntario;
      var query;
      var count = "";
      switch(Object.keys(request.query)[0]) {
        case "idComite":
          query = { "comites._id": types.ObjectId(request.query.idComite), bloqueado: false };
          count = "comites";
        break;
        case "idEmpresa":
          query = {"empresas._id": types.ObjectId(request.query.idEmpresa), bloqueado: false };
          count = "empresas";
        break;
        case "cidade":
          query = {"cidades.nome": { $regex: request.query.cidade, $options: 'i' }, bloqueado: false };
          count = "cidades";
        break;
        case "uf":
          query = {"cidades.uf": { $regex: request.query.uf, $options: 'i' }, bloqueado: false };
          count = "cidades";
        break;
      }

      if(!request.query || !query){
          return reply(Boom.badRequest("Um parametro de busca deve ser especificado"))
      }

      volunt.aggregate([
        { $lookup: {
          from: "comites",
          localField: "comite",
          foreignField: "_id",
          as: "comites"
        }},
        { $unwind: "$comites" },
        { $lookup: {
          from: "cidades",
          localField: "comites.cidade",
          foreignField: "_id",
          as: "cidades"
        }},
        { $unwind: "$cidades" },
        { $lookup: {
          from: "empresas",
          localField: "comites.empresa",
          foreignField: "_id",
          as: "empresas"
        }},
        { $unwind: "$empresas" },
        { $match: query },
        { $count: count }
      ],
      (err, docs) => {
        if(err) throw err;

        return reply(docs)
      });
    }
  })

  server.route({
    method: 'GET',
    path: '/voluntario/por-ano',
    config: {
      description: 'Busca e conta a quantidade de voluntarios cadastrados por ano',
      validate: {
        query: {
          ano: Joi.number().min(1900).max(2100).optional().description('Ano de consulta'),
          voluntarioAtivo: Joi.boolean().optional().description('Voluntário Ativado ou Desativado'),
        }
      }
    },
    handler: (request, reply) => {
      const volunt = DB.voluntario;
      const ano = request.query.ano;
      const bloqueado = request.query.voluntarioAtivo ;
      var query = {}
      if (ano && (bloqueado !== undefined)) {
        query = { "ano": { "$eq": ano }, bloqueado }
      } else {
        if (ano) {
          query = { "ano": { "$eq": ano } }
        } else {
          if (bloqueado !== undefined) {
            query = { "bloqueado": { "$eq": !bloqueado } }
          } else {
            query = {}
          }
        }
      }

      volunt.aggregate([
        {
          $project: {
            "ano": { "$year": "$dataCadastro" },
            "bloqueado": "$bloqueado",
          }
        },
        {
          $match: query
        },
        {
          $group: {
            "_id": null,
            count: { "$sum": 1 }
          }
        }
      ],
      (err, docs) => {
        if(err) throw err;
        if (docs.length === 0) {return reply({ quantidade: 0})}
        return reply({ quantidade: docs[0].count })
    });

    }
  })

  server.route({
    method: 'GET',
    path: '/voluntario/total-ativo',
    config: {
      description: 'Busca o total de voluntários ativos',
    },
    handler: (request, reply) => {
      const volunt = DB.voluntario;

      volunt.aggregate([
        {
          $match: {
            "bloqueado": false
          }
        },
        {
          $group: {
            "_id": null,
            count: { "$sum": 1 }
          }
        }
      ],
      (err, docs) => {
        if(err) throw err;
        if (docs.length === 0) {return reply({ quantidade: 0})}
        return reply({ quantidade: docs[0].count })
    });

    }
  })

  server.route({
    method: 'DELETE',
    path: '/voluntario/{id}',
    config: {
      description: 'Remove um voluntario'
    },
    handler: ( request, reply ) => {
      const volunt = DB.voluntario;

      volunt.findById(request.params.id, (err, doc) => {
        if (err) throw err;

        if(!doc) {
          return reply(Boom.unauthorized(`Não foi encontrado nenhum voluntario com o id:${request.params.id}`))
        }

        DB.acao.find({ "participantes.voluntarioId": request.params.id }, (errAcao, docAcao) => {
          if (errAcao) throw errAcao;

          if(docAcao.length > 0) {
            return reply(Boom.unauthorized('Não é possivel excluir o voluntario, pois ele tem vinculo com uma ação'))
          }

          DB.comite.find({ "liderSocial.voluntarioId": request.params.id }, (errComite, docComite) => {
            if (errComite) throw errComite;

            if(docComite) {
              return reply(Boom.unauthorized('Não é possivel excluir o voluntario, pois ele é lider de um comitê'))
            }

            volunt.remove({_id: types.ObjectId(request.params.id)}, (error, res) => {
              if(error) throw error;

              return reply(res);
            });
          });
        });
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/voluntario/autocomplete',
    config: {
      description: 'Autocomplete do voluntario pelo nome',
      validate: {
        query: {
          comite: Joi.string().description('ID do comite'),
          texto: Joi.string().allow('').description('Texto a ser pesquisado'),
          permitirBloqueado: Joi.boolean().description('Permiti a busca de voluntarios bloqueados')
        }
      }
    },
    handler: function(request, reply) {
      var query = {}
      if (request.query.texto)
        query['nomeNormalizado'] = { $regex: noAccents(request.query.texto).toLowerCase() };
      if (request.query.comite)
        query['comite'] = request.query.comite;
      if (!request.query.permitirBloqueado)
        query['bloqueado'] = false;

      DB.voluntario.find(
        query,
        '_id nome nomeNormalizado urlAvatar cargoNome email',
        { sort: { nomeNormalizado: 1 } },
        function(err, volunt) {
        if (err) return console.error(err);

        reply(volunt);
      });
    }
  });

  server.route({
    method: 'GET',
    path:'/voluntario/avatar',
    config: {
      description: 'Retorna um array de caminho de imagens',
      validate: {
        query: {
          quantidade: Joi.number().required().description("Quantidade do retorno")
        }
      }
    },
    handler: (request, reply) => {
      const volunt = DB.voluntario;

      var fields = { _id: 1, urlAvatar: 1 };

      var options = { limit: request.query.quantidade };
      volunt.findRandom({ usuarioSharePoint: true, "urlAvatar":{$ne:""} }, fields, options, (err, result) => {
        if (err) throw err;
        return reply({ succes: true, data: result });
      });
    }
  });

  server.route({
    method: 'POST',
    path: '/voluntario/loginv2',
    config: {
      description: 'Login SharePoint',
      validate: {
        payload: {
          login: Joi.string().description('Login'),
          senha: Joi.string().description('Senha')
        }
      }
    },
    handler: function (request, reply) {
      loginv2(request.payload.login, request.payload.senha, function(err, res) {
        if (err) return console.log(err);

        reply(res);
      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/voluntario/esqueci-senha',
    config: {
      description: 'Recuperar a senha do voluntário',
      validate: {
        payload: {
          emailCpf: Joi.string().required().description('Email/CPF para verificar se o voluntário é cadastrado'),
        }
      }
    },
    handler: function(request, reply) {
      const login = request.payload.emailCpf
      var tempPass = sr({length: 9});
      var operator = {
        $set: {
          senhaTemp: Crypto.createHash('md5').update(tempPass).digest('hex'),
          senha: ''
        }
      };

      ASQ()
        .then((done) => {
          DB.voluntario.findOne({ $or: [ { "cpf": login }, { "email": login } ]}, (err, volunt) => {
            if (err) return done.fail(err);
            if (volunt === null) { return done.fail(Boom.unauthorized('Email/CPF inválido.')) }
            if (volunt.usuarioSharePoint) {
              return done.fail(Boom.unauthorized('Não é possível realizar a troca de senha. Por gentileza, realizar o processo pelo Office 365.'))
            }
            return done(volunt);
          });
        })
        .then((done, volunt) => {
          DB.voluntario.update({ _id: volunt._id }, operator, function(error, result) {
            if (error) return done.fail(error);
            if (result.nModified < 1) { return done.fail({ message: 'Erro ao gerar nova senha. Tente novamente!' }) }
            return done(volunt);
          });
        })
        .then((done, volunt) => {
          const content = fs
          .readFileSync(path.dirname(__dirname)+'/templates/novaSenha-mail.html')
            .toString()
            .replace(/senhaTemp/g, tempPass)
            .replace(/nomeVoluntario/g, volunt.nome)

          mail(volunt.email, "Nova Senha Temporária - Portal Voluntariado", content, (erro, resposta) => {
            if (erro) throw erro;
            return reply({ message: 'Uma nova senha foi enviada para o seu email.' });
          });
        })
        .onerror((msg) => {
          return reply(msg);
        });
    }
  });

  server.route({
    method: 'POST',
    path: '/voluntario/mudar-senha',
    config: {
      description: 'Serviço que altera a sennha do voluntario',
      validate: {
        payload: {
          senhaAtual: Joi.string().required().description('Senha temporaria'),
          senhaNova: Joi.string().required().description('Nova senha'),
          senhaNovaConfirma: Joi.string().required().description('Confirmação de nova senha'),
          email: Joi.string().optional().description('Email do voluntario'),
          cpf: Joi.string().optional().allow("").description('Cpf do voluntario'),
        }
      }
    },
    handler: (request, reply) => {
      const data = request.payload

      if (data.senhaNova !== data.senhaNovaConfirma) {
        return reply(Boom.badRequest("A nova senha deve ser a igual a confirmação de senha"))
      }

      if (data.senhaNova === data.senhaAtual) {
        return reply(Boom.badRequest("A nova senha deve ser diferente da atual."))
      }

      const query = {}
      if (data.email) {
        query.email = data.email
      }
      if (data.cpf) {
        query.cpf = data.cpf
      }

      DB.voluntario.findOne(query, (err, doc) => {
        if (err) { throw err }

        if (doc.senha === data.senhaNova) {
          return reply(Boom.badRequest("Está senha senha já esta cadastrada, por favor digite uma senha diferente."))
        }

        if (Crypto.createHash('md5').update(data.senhaAtual).digest('hex') !== doc.senhaTemp) {
          return reply(Boom.badRequest("Senha atual está incorreta"))
        }

        const volunt = {
          $set: {
            senha: Crypto.createHash('md5').update(data.senhaNova).digest('hex'),
            senhaTemp: '',
          }
        }
        DB.voluntario.update({_id: types.ObjectId(doc._id)}, volunt, (error, resp) => {
          if (error) { throw error }

          return reply(resp)
        })
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/voluntario/login',
    config: {
      description: 'Login do voluntário',
      validate: {
        query: {
          login: Joi.string().required().description('Email/CPF para login'),
          senha: Joi.string().required().description('Senha para login'),
        }
      }
    },
    handler: function(request, reply) {
      const login = request.query.login

      ASQ()
        .then((done) => {
          DB.voluntario.findOne({ $or: [ { "cpf": login }, { "email": login } ]}, (err, volunt) => {
            if (err) return done.fail(err);
            if (volunt === null) { return done.fail(Boom.unauthorized('Email/CPF inválido.')) }
            if (volunt.bloqueado) {
              return done.fail(Boom.unauthorized('Usuário está bloqueado.'))
            }
            return done(volunt);
          });
        })
        .then((done, volunt) => {

          if (volunt.usuarioSharePoint) {
            loginv2(volunt.email, request.query.senha, (erro, res) => {
              if (erro) return done.fail(erro);
              if (res.status === 400) {return done.fail(Boom.unauthorized('Email/CPF ou senha inválidos.'))}
              reply({_id: volunt._id, nome: res.usuario, email: res.email })
            });
          }
          const cryptoSenha = request.query.senha
            ? Crypto.createHash('md5').update(request.query.senha).digest('hex')
            : "";
          if (cryptoSenha !== volunt.senha) {
              if (cryptoSenha !== volunt.senhaTemp) {
                return done.fail(Boom.unauthorized('Email/CPF ou senha inválidos.'))
              }
              var result = {}
              Object.assign(result, volunt._doc)
              result['temQueTrocarSenha'] = true
              return done(result)
            }
            return done(volunt)
        })
        .then((done, resposta) => {
          return reply(resposta)
        })
        .onerror((msg) => {
          return reply(msg);
        })
    }
  });

  server.route({
    method: 'GET',
    path: '/valida-cpf',
    config: {
      description: 'Verifica o CPF antes do cadastro',
      validate: {
        query: {
          cpf: Joi.string().required().description('CPF a ser validado'),
          usuarioIncompleto: Joi.boolean().optional().default(false).description('Valida se o CPF existe na base')
        }
      }
    },
    handler: function(request, reply) {

      DB.voluntario.findOne({ cpf: request.query.cpf }, (err, volunt) => {
        if (err) return console.error(err);

        if (volunt) {
          if (!request.query.usuarioIncompleto || (request.query.usuarioIncompleto && volunt.aceitouTermoEsseAno)) {
            return reply(Boom.badRequest('Já existe um voluntário cadastrado com este CPF'));
          }
        }

        validaCPF.verifica(request.query.cpf, res => reply(res))
      });
    }
  })

  server.route({
    method: 'GET',
    path: '/voluntario/valida-cpf',
    config: {
      description: 'Verifica o CPF no redirect',
      validate: {
        query: {
          cpf: Joi.string().required().description('CPF a ser validado')
        }
      }
    },
    handler: (request, reply) => validaCPF.verifica(request.query.cpf, res => reply(res))
  })

  server.route({
    method: 'GET',
    path: '/voluntario/verifica-bloqueio',
    config: {
      description: 'Verifica os usuários completos se eles foram bloquados',
    },
    handler: (request, reply) => {
      DB.voluntario.find({ bloqueado: false })
        .exec((err, docs) => {
        if(err) { throw err }
        var blockeds = []
        const doneUp = (go) => {
          if (go) { reply({success: 'ok', message: blockeds.length + ' voluntário(s) bloqueados'}) }
        }
        ASQ()
        .then((done) => {
          docs.map((vol, i) => {
            validaCPF.verifica(vol.cpf, (res, j) => {
              if (res.dataDemissao) {
                vol.bloqueado = true;
                blockeds.push(vol)
                if(docs.length === (j + 1)) { done() }
              }
              if(docs.length === (i + 1)) { setTimeout(() => { done() }, 2500) }
            }, i) // verificaPCF
          }) // map docs
        })
        .then((done) => {
          doneUp(blockeds.length === 0)
          blockeds.map((vol, i) => {
            new DB.voluntario(vol).save((erro, doc) => {
              if (erro) { throw erro }
              doneUp(blockeds.length === (i + 1))
            }) // save
          })
        }) // maps blockeds
      }) // callback .exec
    }
  })

  server.route({
    method: 'PUT',
    path: '/voluntario/atualiza-data-acesso',
    config: {
      description: 'Atualiza a data de último acesso do voluntário',
      validate: {
        payload: {
          _id: Joi.string().required().description('Id do voluntário'),
        }
      }
    },
    handler: function(request, reply) {
      var date = obtemDataAtual();
      var voluntario = {
        $set: {
          dataUltimoAcesso: date,
        }
      };

      DB.voluntario.update({_id: types.ObjectId(request.payload._id)}, voluntario, (err, res) => {
        if (err) throw err;

        return reply({ success: 'ok', date });
      });
    }
  })

  return next();
}

exports.register.attributes = {
    name: 'voluntario',
    version: require('../package.json').version
};

function enviaNotificacao(done, volunt) {
  var collection = DB.comite;
  collection.findById(volunt.comite, (err, res) => {
    if (err) throw err;
    if (!res.liderSocial) return done();

    var date = obtemDataAtual();
    var notificacao = new DB.notificacao({
      publicoAlvo: [res.liderSocial],
      texto: "Novo voluntário cadastrado",
      tipo: "Novo voluntário",
      dataCadastro: date
    });

    notificacao.save((error, resp) => {
      if (error) throw error;
      return done();
    });
  });

}

const sendMailRegisterVol = (done, doc, senhaTemp, ress) => {
  const textSenha = !doc.usuarioSharePoint
                ? `Sua senha de acesso é <strong>${senhaTemp}</strong>` // nao tem no contxto
                : ''
  const txtVoluntario = fs
    .readFileSync(`${path.dirname(__dirname)}/templates/voluntario-mail.html`)
    .toString()
    .replace(/{nome}/g, doc.nome)
    .replace(/{senha}/g, textSenha);

  mail(doc.email, "Bem vindo ao portal do voluntario", txtVoluntario, (err, res) => {
    if (err) throw err;

    return done(ress);
  });
}

const sendMailLiderRegisterVol = (done, doc, ress) => {
  const txtSuperior = fs
    .readFileSync(`${path.dirname(__dirname)}/templates/superior-mail.html`)
    .toString()
    .replace(/{nome}/g, doc.nome);

  mail(doc.emailSuperior, "Um associado da sua equipe foi cadastrado como voluntario", txtSuperior, (error, response) => {
    if (error) throw error;

    return done(ress);
  });
}

function obtemDataAtual() {
  var now = new Date();
  now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));
  return now;
}
