const request = require('request');
var ASQ = require('asynquence');

module.exports = function(login, senha, callback) {
  const baseUrl = `http://www.movimentogsg.com.br/loginSSO`;
  const uri = `${baseUrl}?login=${login}&password=${senha}`;

  ASQ()
  .then(function(done) {
    request(uri, function (error, response, body) {
      if (error || response.statusCode !== 200) done(error, response);

      // remove caracteres invalidados para JSON
      var res = JSON.parse(body.replace(/[\u0000-\u0019]+/g,""));

      done(null, res);
    });
  })
  .then(function(done, err, res) {
    callback(err, res);
    done();
  });
}
