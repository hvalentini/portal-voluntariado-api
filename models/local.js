module.exports = (Mongoose) => {
  var local = new Mongoose.Schema({
    'nome':               { type: String,   required: true },
    'nomeNormalizado':    { type: String,   required: false },
    'endereco':           { type: String,   required: true },
    'telefone':           { type: String,   required: true },
    'responsavelNome':    { type: String,   required: true },
    'responsavelEmail':   { type: String,   required: false },
    'observacao':         { type: String,   required: false },
    'responsavelNome2':   { type: String,   required: false },
    'responsavelEmail2':  { type: String,   required: false },
    'escola':             { type: Boolean,  required: true },
    'serie':              { type: String,   required:false },
    'qtdeSalas':          { type: Number,   required: false },
    'qtdeAlunos':         { type: Number,   required: false },
    'dataCadastro':       { type: Date,     required: true },
    'criadoPor':          { type: String,   required: true },
    'dataEdicao':         { type: Date,     required: false },
    'editadoPor':         { type: String,   required: false },
    '__v':                { type: Number,   select: false},
  }).index({ nome: "text" });

  return Mongoose.model('local', local, 'locais');
}
