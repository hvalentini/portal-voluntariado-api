const version = require('../package.json').version

const internals = {}

exports.register = (server, options, next) => {

  server.route({
    method: 'GET',
    path: '/images/{param*}',
    config: {
      cache: {
        expiresIn: 30 * 1000,
        privacy: 'private',
      },
      handler: {
        directory: {
          path: 'public/images',
          listing: true,
        },
      },
    },
  })

  return next()
}

exports.register.attributes = {
    name: 'static',
    version
};
