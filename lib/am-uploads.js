var fsExtra = require('fs.extra');
var fs = require('fs');
var scp = require('scp');
var path = require('path');
var ASQ = require('asynquence');
var DB = require('./db');
var Mongoose  = require('mongoose');

function scpFile(file) {
    return ASQ(function(done) {

        var options = {
            file: file,
            host: '192.168.110.57',
            user: 'root',
            path: '/nfs-apache/netsabe/uploads/'
        };

        scp.send(options, function(err) {
            if (err) console.log(err);
            done();
        });
    });
}

function copyFile(file) {
    return ASQ(function(done) {
        // var idAcao = "58a6e9ce7e1eab2c8091bd38"

        var id = file.filename
        var fileName = path.basename(file.path);
        var baseDir = `../portal-voluntariado-web/public/images/${id}`
        // var copyPath = path.join('/app/netsabe/uploads', fileName);
        var copyPath = path.join('/app/netsabe/uploads', fileName);
        if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
          copyPath = path.join(baseDir, fileName);
        }

        fs.access(baseDir, fs.F_OK, (err) => {
           if(err){
             fs.mkdir(baseDir)
           }

           fsExtra.copy(file.path, copyPath, { replace: true }, function(err) {
               if (err) { throw err; }
               //Ajusta as permissões do arquivo
               fsExtra.chmodSync(copyPath, '755');
               done();
           });
        });

        // fs.copy(file, copyPath, { replace: false }, function(err) {
    });
}

exports.register = function (server, options, next) {

    server.route({
        method: 'POST',
        path: '/upload/image',
        config: {
            payload: {
                output: 'file',
                parse: true,
                maxBytes: 209715200,
                allow: 'multipart/form-data'
            },
            description: 'Faz o upload dos arquivos',
            handler: function (request, reply) {
                //Array com os novos arquivos criados
                var filesCreated = [];
                var filePaths = [];

                for (var fileName in request.payload) {
                    // console.log('Path: ' + request.payload[fileName].path)
                    filesCreated.push({
                        originalName: fileName,
                        mediaPath: path.basename(request.payload[fileName].path)
                    });

                    filePaths.push(request.payload[fileName]);
                }

                ASQ()
                .seq.apply(null, filePaths.map(copyFile))
                .then(function(){
                    return reply(filesCreated);
                })
            }
        }
    });

    return next();
};

exports.register.attributes = {
    name: 'uploads-image'
};
