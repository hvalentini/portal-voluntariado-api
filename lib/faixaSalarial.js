var Joi = require('joi');
var Boom = require('boom');
var Mongoose = require('mongoose');
var DB = require('./db');
const Filtro = require('../utils/filtro');

exports.register = (server, options, next) => {

  server.route({
    method: 'GET',
    path: '/faixa-salarial',
    config: {
      description: 'Busca de faixas etarias',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      },
    },
    handler: (request, reply) => {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "nome": 1 };
      const filtro = Filtro.formatar(request.query.filtro);

      DB.faixaSalarial.find(filtro).sort(ordem).exec((err, objects) => {
        if (err) return console.error(err);
        if (!objects) { return reply(Boom.conflict("Não foram encontradas faixas saláriais para a pesquisa informada")) }
        reply(objects);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/faixa-salarial/{id}',
    config: {
      description: 'Detalha uma faixa salárial pelo id',
    },
    handler: (request, reply) => {
      DB.faixaSalarial.findOne({ _id: request.params.id }).exec((err, object) => {
        if (err) return console.error(err);
        if (!object) { return reply(Boom.conflict("O id da faixa salárial informado não foi encontrado")) }
        reply(object);
      })
    }
  });

  server.route({
    method: 'POST',
    path: '/faixa-salarial',
    config: {
      description: 'Cadastra uma faixa salárial',
      validate: {
          payload: {
              nome: Joi.string().required().description('nome da faixa salárial'),
              valor: Joi.number().description('valor da faixa salárial'),
          }
      },
    },
    handler: (request, reply) => {
      const nome = request.payload.nome;
      const query = { nome: { $regex : new RegExp(nome, "i") } }

      DB.faixaSalarial.find(query, (err, objects) => {
        if (objects.length > 0) {
          return reply(Boom.conflict(`Já existe uma faixa salárial com o nome ${nome}`));
        }

        const obj = new DB.faixaSalarial(request.payload)
        obj.save((err, docs) => {
          if (err) { return console.error(err); }

          reply({ success: true, data: docs });
        }); // save
      }); // obj.find
    }
  });

  server.route({
    method: 'PUT',
    path: '/faixa-salarial',
    config: {
      description: 'Edita uma faixa salárial',
      validate: {
          payload: {
              _id: Joi.string().required().description('id da faixa salárial'),
              nome: Joi.string().required().description('nome da faixa salárial'),
              valor: Joi.number().description('valor da faixa salárial'),
          }
      },
    },
    handler: (request, reply) => {
      const _id = request.payload._id;
      const nome = request.payload.nome;
      const query = { nome: { $regex : new RegExp(nome, "i") } }

      DB.faixaSalarial.find(query, (err, objects) => {
        console.log(objects);
        if (objects.length > 1 || (objects[0] && (_id != objects[0]._id))) {
          return reply(Boom.conflict(`Já existe uma faixa salárial com o nome ${nome}`));
        }

        DB.faixaSalarial.findByIdAndUpdate(_id, request.payload).exec()
          .then((object) => {
            reply({ success: true, data: Object.assign(object, request.payload)});
          }, (erro) => {
            console.error(erro);
            reply(Boom.badRequest(erro));
          });
      }); // obj.find
    }
  });

  server.route({
    method: 'DELETE',
    path: '/faixa-salarial/{id}',
    config: {
      description: 'Deleta uma faixa salárial',
    },
    handler: (request, reply) => {
      const id = objectId(request.params.id);
      DB.faixaSalarial.findOne({ _id: id }, (er, obj) => {
        if (er) { return console.error(er); }
        if (obj === null) { return reply(Boom.conflict("O id da faixa salárial informada não foi encontrada")); }

        DB.voluntario.findOne({ faixaEtaria: id }, (erro, voluntario) => {
          if (erro) { return console.error(erro); }
          if (voluntario !== null) { return reply(Boom.conflict("O id da faixa salárial informada está vinculado a um voluntário")); }

          obj.remove((err) => {
            if (err) return console.error(err);
            reply({ success: true, message: 'Faixa salárial excluída com sucesso'});
          }); // remove obj
        }); // voluntario
      }); //obj
    } // handler
  }); // route

  function objectId(id) {
    return Mongoose.Types.ObjectId(id);
  }

  return next();
}

exports.register.attributes = {
    name: 'faixaSalarial',
    version: require('../package.json').version
};
