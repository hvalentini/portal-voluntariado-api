const fsExtra = require('fs.extra');
const Request = require('request');
const Joi = require('joi');
const fs = require('fs');
const path = require('path');

const baseDirIMG = `public/images`
const baseDir = `${baseDirIMG}/avatars`

exports.register = (server, options, next) => {

    server.route({
        method: 'GET',
        path: '/avatar',
        config: {
            description: 'Faz o upload dos arquivos',
            cache: {
              expiresIn: 30 * 1000,
              privacy: 'private',
            },
            validate: {
              query: {
                email: Joi.string().description('email de referencia da imagem'),
              }
            },
            handler: (request, reply) => {
              const email = request.query.email
              const pathFile = path.join(baseDir, `${email}.jpg`);
              // return reply.redirect(`http://www.movimentogsg.com.br/userAvatar?email=${email}&simple=1`)
              try {
                stats = fs.statSync(pathFile);
                return reply.file(pathFile);
              }
              catch (e) {
              }

              fs.access(baseDirIMG, fs.F_OK, (err) => {
                 if(err){
                   fs.mkdir(baseDirIMG)
                 }
              });

              fs.access(baseDir, fs.F_OK, (err) => {
                 if(err){
                   fs.mkdir(baseDir)
                 }
              });

              return Request.get({url: `http://www.movimentogsg.com.br/userAvatar?email=${email}&simple=1`, encoding: 'binary'}, (err, response, body) => {
                fs.writeFile(pathFile, body, 'binary', (err) => {
                  if(err){
                    console.log(err);
                    throw err;
                  }
                  fsExtra.chmodSync(pathFile, '775');
                  reply.file(pathFile);
                });
              });
            }
          }
    });

    return next();
};

exports.register.attributes = {
    name: 'upload-avatar'
};
