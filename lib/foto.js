var DB = require('./db');

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/foto',
    config: {
      description: 'Busca de empresas',
    },
    handler: function (request, reply) {
      DB.empresa.find({}, function(err, empresas) {
        if (err) return console.error(err);
        reply(empresas);
      })
    }
  });


  server.route({
    method: 'POST',
    path: '/foto',
    config: {
      description: 'Cadastra de foto',
    },
    handler: function (request, reply) {
      var emp = new DB.empresa({ name: request.payload.name })
      emp.save(function(err) {
        if (err) return console.error(err);
        reply({ success: true });
      })
    }
  });

  return next();

}

exports.register.attributes = {
    name: 'foto',
    version: require('../package.json').version
};
