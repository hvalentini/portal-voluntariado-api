module.exports = (Mongoose) => {
  var tipoDeAcao = new Mongoose.Schema({
    'nome': { type: String, required: true, upsert: true, unique: true },
    'exclusivoParaLider': { type: Boolean, required: true, upsert: true },
    'informarQtdeCartas': { type: Boolean, required: true, upsert: true},
    'tipo': { type: Number, required: true , min: 0, max: 2 },
    '__v': { type: Number, select: false},
    'nomeNormalizado': { type: String },
  })

  /**
   * Tipo do tipo de ação
   * 0: Pontual
   * 1: Continua
   * 2: Gerencial
  **/
  return Mongoose.model('tipoDeAcao', tipoDeAcao, 'tipoDeAcoes');
}
