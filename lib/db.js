const Hoek = require('hoek');
const Mongoose = require('mongoose');

// connection MongoDB
var Config = require('../config.json').database.mongodb;
// var Config = require('../config.json').database.mongodbdev;

if (process.env.NODE_ENV === 'development') {
  Config = require('../config.json').database.mongodbdev;
}
if (process.env.NODE_ENV === 'test') {
  Config = require('../config.json').database.mongodbhomo;
}
// Mongoose.connect(Config.host, Config.db, Config.port); // conc sem usuário
Mongoose.connect(Config.host, Config.db, Config.port, Config.options);
Mongoose.Promise = global.Promise;

Mongoose.connection.on('connected', () => {
  console.log('Mongoose! Conectado em ',
    `${Config.host}:${Config.port}/${Config.db}`);
});

Mongoose.connection.on('disconnected', () => {
  console.log('Mongoose! Desconectado de ',
    `${Config.host}:${Config.port}/${Config.db}`);
});

Mongoose.connection.on('error', (erro) => {
  console.log('Mongoose! Erro na conexão: ' + erro);
});

process.on('SIGINT', () => {
  Mongoose.connection.close(() => {
    console.log('Mongoose! Desconectado pelo término da aplicação');
    process.exit(0);
  });
});

module.exports = {
  empresa: require('../models/empresa.js')(Mongoose),
  foto: require('../models/foto.js')(Mongoose),
  acao: require('../models/acao.js')(Mongoose),
  tipoDeAcao: require('../models/tipoDeAcao.js')(Mongoose),
  cidade: require('../models/cidade.js')(Mongoose),
  comite: require('../models/comite.js')(Mongoose),
  voluntario: require('../models/voluntario.js')(Mongoose),
  local: require('../models/local.js')(Mongoose),
  notificacao: require('../models/notificacao.js')(Mongoose),
  comiteLogAnual: require('../models/comiteLogAnual.js')(Mongoose),
  parametroSistema: require('../models/parametroSistema.js')(Mongoose),
  faixaEtaria: require('../models/faixaEtaria.js')(Mongoose),
  faixaSalarial: require('../models/faixaSalarial.js')(Mongoose),
  mongoose: Mongoose
};
