module.exports = (Mongoose) => {
  var parametroSistema = new Mongoose.Schema({
    'adminLogin':             { type: String, required: true },
    'adminSenha':             { type: String, required: true },
    'verbaPadraoAnoVigente':  { type: Number, required: true },
    '__v':                    { type: Number, select: false},
  })

  return Mongoose.model('parametroSistema', parametroSistema, 'parametroSistema');
}
