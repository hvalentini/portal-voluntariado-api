module.exports = function(DB, Boom) {
  var module = {};
  var acoes = DB.acao;
  module.POST =  function (query, callback) {
    const dataIni = new Date(query.dataInqicialFiltro);
    const dataFim = new Date(query.dataFinalFiltro);
    acoes.aggregate([
      {
        $match: {
          "participantes.voluntarioId" : query.voluntarioId,
          "dataInicial" : { "$gte": dataIni },
          "dataFinal" : { "$lte": dataFim },
        }
      },
      {  $group: {
          _id: null,
          soma: { $sum: "$tempoDuracao" }
        },
      }
    ],
    (err, docs) => { return callback(err, docs) })
  }

  return module;
}
