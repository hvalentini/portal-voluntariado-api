module.exports = (Mongoose) => {
  const voluntarioResumido = new Mongoose.Schema({
    'voluntarioId': { type: String },
    'nome': { type:String },
    'urlAvatar': { type: String },
    'cargo': { type:String },
    'email': { type: String },
  })

  return voluntarioResumido;
}
