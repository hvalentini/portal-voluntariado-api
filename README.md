# Portal Voluntariado API
_explicar projeto_

## Pré-requisitos

#### Node >=4.5.0
O projeto requer o `node >= 4.5.0`.
Utilizar o nvm para a última versão estável da `lts/argon` que atualmente é `4.7.0`.
Utilizar o comando `nvm ls` para listar as versões.

#### NPM 3
O projeto requer o npm no mínimo na versão 3.
Certifique-se de estar utilizando no mínimo a versão 3 com o comando abaixo:
`npm install -g npm@3`

#### Nodemon
Para executar o ambiente de dev é necessário ter o nodemon instalado.
Certifique-se de estar com o nodemon instalado com o comando abaixo:
`npm install -g nodemon`

#### RoboMongo / Acesso ao MongoDB
Este não é um pré-requisito para executar o projeto porém é ideal ter este software instalado para acessar o MongoDB.
[Dowload do RoboMongo](https://robomongo.org/download)
O MongoDB está disponível no endereço: `162.243.120.13:27017`


## TODO
_Explicar estrutura_
