//MongoClient
var noAccents = require('remove-accents');
var Mongoose = require('mongoose');
const voluntarioResumido = require('../models/schemas/voluntarioResumido')(Mongoose)
var types = require('mongoose').Types;
const xlsx = require('xlsx');
var path = require('path');
var DB = require('./db');
const Boom = require('boom');
var ASQ = require('asynquence')
var _ = require('lodash')
const validaCPF = require('./../utils/validaCPF.js')(Boom);
const tipoAcao = require('../utils/tipo_acao.json')
const faixaEtaria = require('../utils/faixa_etaria.json')
const faixaSalarial = require('../utils/faixa_salarial.json')

exports.register = (server, options, next) => {

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/voluntario',
    config: { description: 'Importa dados de voluntario para o banco de dados' },
    handler: (request, reply) => {
      var filePath = `${path.dirname(__dirname)}/public/spreadsheet_import/voluntario_data.xlsx`;
      var workbook  = xlsx.readFile(filePath);
      ASQ()
        .then((done) => { importCity(workbook, ['A', 'B'], 'L', done, filePath) })
        .then((done) => { importCompany(workbook, 'C', 'M', done, filePath) })
        .then((done) => { importCommittee(workbook, ['D', 'L', 'M', 'A', 'B'], 'N', done, filePath) })
        .then((done) => { importVolunteer(workbook, ['F', 'G', 'H', 'I', 'J', 'N'], 'O', reply, filePath) })
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/comite',
    config: { description: 'Importa dados de comite para o banco de dados'},
    handler: ( request, reply ) => {
      var filePath = `${path.dirname(__dirname)}/public/spreadsheet_import/comite_data.xlsx`;
      var workbook  = xlsx.readFile(filePath);

      ASQ()
        .then((done) => { importCity(workbook, ['A', 'B'], 'K', done, filePath) })
        .then((done) => { importCompany(workbook, 'C', 'L', done, filePath) })
        .then((done) => { importCommittee(workbook, ['D', 'K', 'L', 'A', 'B'], 'M', done, filePath) })
        .then((done) => { importLeader(workbook, ['F', 'G', 'H', 'I', 'J'], 'N', reply, filePath) })
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/tipo-acao',
    config: { description: 'Importa dados de comite para o banco de dados'},
    handler: ( request, reply ) => {
      var control = [];

      tipoAcao.forEach((element) => {
        (function(element, control, reply){
          DB.tipoDeAcao.find({ nome: { $regex : new RegExp(element.nome, "i") } }, (err, doc) => {
            if(err) { throw err }

            if(doc.length > 0) {
              control.push(doc)
              if(control.length === tipoAcao.length) { return reply({ sucess: true, data: control }) }
            } else {
              var toSave = new DB.tipoDeAcao(element)
              toSave.save((error, resp) => {
                if (error) { throw error }
                control.push(resp)
                if(control.length === tipoAcao.length) { return reply({ sucess: true, data: control }) }
              });
            }
          });
        })(element, control, reply)
      });
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/faixa-etaria',
    config: { description: 'Importa dados de comite para o banco de dados'},
    handler: ( request, reply ) => {
      var control = [];

      faixaEtaria.forEach((element) => {
        (function(element, control, reply){
          DB.faixaEtaria.find({ nome: { $regex : new RegExp(element.nome, "i") } }, (err, doc) => {
            if(err) { throw err }

            if(doc.length > 0) {
              control.push(doc)
              if(control.length === faixaEtaria.length) { return reply({ sucess: true, data: control }) }
            } else {
              var toSave = new DB.faixaEtaria(element)
              toSave.save((error, resp) => {
                if (error) { throw error }
                control.push(resp)
                if(control.length === faixaEtaria.length) { return reply({ sucess: true, data: control }) }
              });
            }
          });
        })(element, control, reply)
      });
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/faixa-salarial',
    config: { description: 'Importa dados de comite para o banco de dados'},
    handler: ( request, reply ) => {
      var control = [];

      faixaSalarial.forEach((element) => {
        (function(element, control, reply){
          DB.faixaSalarial.find({ nome: { $regex : new RegExp(element.nome, "i") } }, (err, doc) => {
            if(err) { throw err }

            if(doc.length > 0) {
              control.push(doc)
              if(control.length === faixaSalarial.length) { return reply({ sucess: true, data: control }) }
            } else {
              var toSave = new DB.faixaSalarial(element)
              toSave.save((error, resp) => {
                if (error) { throw error }
                control.push(resp)
                if(control.length === faixaSalarial.length) { return reply({ sucess: true, data: control }) }
              });
            }
          });
        })(element, control, reply)
      });
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/importacao/fix-avatar',
    config: { description: 'Ajusta as url do avatar no banco' },
    handler: (request, reply) => {
      var res = {}
      // ASQ()
      //   .then((done) => { importCity(workbook, ['A', 'B'], 'K', done, filePath) })
      //   .then((done) => { importCompany(workbook, 'C', 'L', done, filePath) })
      // DB.voluntario.find({ urlAvatar: /www.movimentogsg/i}).exec((err, objects) => {
      DB.voluntario.find({ urlAvatar: { $exists: true } }).exec((err, objects) => {
        if (err) { return console.error(err) }
        // if (!objects) { return reply(Boom.conflict("Não foram encontrados voluntarios para a pesquisa informada")) }

        objects.map(object => {
          object.urlAvatar = `/avatar?email=${object.email}`
          object.save()
        })

        res = { success: true, message: `${objects.length} voluntarios afetados com sucesso` }
        DB.acao.find().exec((err, objs) => {
          if (err) { return console.error(err) }
          // if (!objs) { return reply(Boom.conflict("Não foram encontradas ações para a pesquisa informada")) }

          objs.map(obj => {

            obj.criadaPor.urlAvatar = obj.criadaPor.urlAvatar ? `/avatar?email=${obj.criadaPor.email}` : ''
            obj.gostaram.map(user => {
              user.urlAvatar = user.urlAvatar ? `/avatar?email=${user.email}` : ''
            })
            obj.queremParticipar.map(user => {
              user.urlAvatar = user.urlAvatar ? `/avatar?email=${user.email}` : ''
            })
            obj.participantes.map(user => {
              user.urlAvatar = user.urlAvatar ? `/avatar?email=${user.email}` : ''
            })
            obj.save()
          })

          res.message2 = `${objs.length} acoes afetadas com sucesso`

          DB.comite.find({ liderSocial: { $exists: true, $ne: {} } }).exec((err, comites) => {
            if (err) { return console.error(err) }
            // if (!comites) { return reply(Boom.conflict("Não foram encontrados comites para a pesquisa informada")) }

            comites.map(comite => {
              comite.liderSocial.urlAvatar = `/avatar?email=${comite.liderSocial.email}`
              comite.save()
            })
            res.message3 = `${objs.length} comites afetados com sucesso`
            reply(res);
          })
        });
      });
    }
  });

  return next();
}

exports.register.attributes = {
    name: 'importacao',
    version: require('../package.json').version
};

/* Funções que fazem inserção do Banco de dados */

// Insere dados de cidade no banco de dados
function importCity(workbook, cell, celltoWrite, reply, filePath) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]],
      result    = filterExcel(workbook, cell, ['nome', 'uf'], 'nome'),
      dbData    = [],
      i = 0;

  for(i = 0; i < result.length; i++) {
    (function(i) {
      DB.cidade.findOne( { $and: [
        { nome: { $regex : new RegExp(result[i].nome, "i") } },
        { uf: { $regex : new RegExp(result[i].uf, "i") } }
      ]},
      function (err, resp) {
        if(err) throw err;
        if (resp) {
          dbData.push(resp)
          if(dbData.length === result.length ) { return writeCidadeId(workbook, celltoWrite ,dbData, reply, filePath) }
        }
        else {
          var cidade = new DB.cidade({
            uf: result[i].uf,
            nome: result[i].nome,
            nomeNormalizado: noAccents(result[i].cidade.toLowerCase()),
          })
          cidade.save((err, doc) => {
            if (err) { return console.error(err); }
            dbData.push(doc);
            if(dbData.length === result.length ) { return writeCidadeId(workbook, celltoWrite, dbData, reply, filePath) }
          });//save
        }
      });//findOne
    })(i)

  }//for
}

// Insere dados de empresa no banco de dados
function importCompany(workbook, cell, celltoWrite, reply, filePath) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]],
      result    = filterExcel(workbook, cell, ['nome'], 'nome'),
      dbData    = [],
      i         = 0;

  for(i = 0; i < result.length; i++) {
    (function(i) {
      DB.empresa.findOne({ nome: { $regex : new RegExp(result[i].nome, "i") } }, (err, resp) => {
        if(err) throw err;
        if (resp) {
          dbData.push(resp)
          if(dbData.length === result.length ) { return writeEmpresaId(workbook, celltoWrite, dbData, reply, filePath) }
        }
        else {
          const empresa  = new DB.empresa({
            nome: result[i].nome,
            nomeNormalizado: noAccents(result[i].nome).toLowerCase,
          })
          empresa.save((error, doc) => {
            if (error) { throw error }
            dbData.push(doc);
            if(dbData.length === result.length ) { return writeEmpresaId(workbook, celltoWrite, dbData, reply, filePath) }
          });//save
        }
      });//findOne
    })(i)
  }
}

// Insere dados de comite no banco de dados
function importCommittee(workbook, cell, celltoWrite, reply, filePath) {
  var result    = filterExcelCommittee(workbook, cell, ['unidade', 'cidade','empresa', 'cidadeNome', 'cidadeUF'], ['cidade', 'empresa']),
      dbData    = [],
      i         = 0;

  for(i = 0; i < result.length; i++) {

    (function(i, dbData, result, workbook) {
      DB.comite.findOne({
        cidade: Mongoose.Types.ObjectId(result[i].cidade),
        empresa: Mongoose.Types.ObjectId(result[i].empresa),
        unidade: (result[i].unidade || ''),
      }, (err, resp) => {
        if(err) throw err;
        if (resp) {
          resp['unidadeNormalizada'] = noAccents(resp['unidade'].E.toLowerCase()) || ''
          dbData.push(resp)
          if(dbData.length === result.length ) { return writeComiteId(workbook, cell, celltoWrite, dbData, reply, filePath) }
        }
        else {
          result[i]['verbaInicial'] = 3000.00
          result[i]['saldoAtual'] = 3000.00
          result[i]['unidadeNormalizada'] = noAccents(resultado[i].unidade.E.toLowerCase()) || ''
          const comite  = new DB.comite(result[i])
          comite.save((error, doc) => {
            if (error) { throw error }
            dbData.push(doc);
            if(dbData.length === result.length ) { return writeComiteId(workbook, cell, celltoWrite, dbData, reply, filePath) }
          });//save
        }
      });//findOne
    })(i, dbData, result, workbook)
  }//for
}

// Insere dados de voluntario no banco de dados - Importação de voluntario
function importVolunteer(workbook, cell, celltoWrite, reply, filePath) {
  var result    = filterExcelVolunteer(workbook, cell),
      dbData    = [],
      i         = 0;

  for(i = 0; i < result.length; i++) {

    (function(i, dbData, result, workbook) {
      if(result[i].cpf) {
        validaCPF.verifica(result[i].cpf, (res) => {
          if(!res.isBoom) {
            result[i].cargoId = res.cargoId;
            result[i].cargoNome = res.cargoNome
            result[i].centroResultadoId = res.centroResultadoId;
            result[i].centroResultadoNome = res.centroResultadoNome;
            result[i].associadoExecutivo = res.associadoExecutivo;
          }
          DB.voluntario.findOne({ email: { $eq: result[i].email.trim() } }, (err, resp) => {
            if(err) throw err;
            if (resp) {

              dbData.push(resp)
              if(dbData.length === result.length ) { return writeVolunteerId(workbook, celltoWrite, dbData, reply, filePath) }
            }
            else {
              const voluntario  = new DB.voluntario(result[i])
              voluntario.save((error, doc) => {
                if (error) { throw error }
                dbData.push(doc);
                if(dbData.length === result.length ) { return writeVolunteerId(workbook, celltoWrite, dbData, reply, filePath) }
              });//save
            }
          });//findOne
        })
      } else {
        DB.voluntario.findOne({ email: { $eq: result[i].email.trim() } }, (err, resp) => {
          if(err) throw err;
          if (resp) {

            dbData.push(resp)
            if(dbData.length === result.length ) { return writeVolunteerId(workbook, celltoWrite, dbData, reply, filePath) }
          }
          else {
            const voluntario  = new DB.voluntario(result[i])
            voluntario.save((error, doc) => {
              if (error) { throw error }
              dbData.push(doc);
              if(dbData.length === result.length ) { return writeVolunteerId(workbook, celltoWrite, dbData, reply, filePath) }
            });//save
          }
        });//findOne
      }
    })(i, dbData, result, workbook)

  }//for
}

// Insere dados de lider no banco de dados - Importação de comite
function importLeader(workbook, cell, celltoWrite, reply, filePath) {
  result    = filterExcelLeader(workbook, cell),
  dbData    = [],
  i         = 0;

  for(i = 0; i < result.length; i++) {

    (function(i, dbData, result, workbook) {
      if(result[i].cpf) {
        validaCPF.verifica(result[i].cpf, (res) => {
          if(!res.isBoom) {
            result[i].cargoId = res.cargoId;
            result[i].cargoNome = res.cargoNome;
            result[i].centroResultadoId = res.centroResultadoId;
            result[i].centroResultadoNome = res.centroResultadoNome;
            result[i].associadoExecutivo = res.associadoExecutivo;
          }

          DB.voluntario.findOne({ email: { $eq: result[i].email.trim() } }, (err, resp) => {
            if(err) throw err;
            if (resp) {
              result[i]._id = resp._id;
              DB.voluntario.update({_id: types.ObjectId(resp._id)}, { $set: result[i] }, (error, doc) => {
                if (error) { throw error }
                dbData.push(result[i]);
                if(dbData.length === result.length ) { return writeLeaderId(workbook, celltoWrite, dbData, reply, filePath) }
              });//update
            }
            else {
              const voluntario  = new DB.voluntario(result[i])
              voluntario.save((error, doc) => {
                if (error) { throw error }
                dbData.push(doc);
                if(dbData.length === result.length ) { return writeLeaderId(workbook, celltoWrite, dbData, reply, filePath) }
              });//save
            }
          });//findOne
        })
      } else {
        DB.voluntario.findOne({ email: { $eq: result[i].email.trim() } }, (err, resp) => {
          if(err) throw err;
          if (resp) {
            result[i]._id = resp._id;
            DB.voluntario.update({_id: types.ObjectId(resp._id)}, { $set: result[i] }, (error, doc) => {
              if (error) { throw error }
              dbData.push(result[i]);
              if(dbData.length === result.length ) { return writeLeaderId(workbook, celltoWrite, dbData, reply, filePath) }
            });//update
          }
          else {
            const voluntario  = new DB.voluntario(result[i])
            voluntario.save((error, doc) => {
              if (error) { throw error }
              dbData.push(doc);
              if(dbData.length === result.length ) { return writeLeaderId(workbook, celltoWrite, dbData, reply, filePath) }
            });//save
          }
        });//findOne
      }

    })(i, dbData, result, workbook)

  }//for
}

// Verifica se usuario é lider e o insere como lider de comite - Importação de comite
function checkLeaders(workbook, data, reply) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]];
  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1));
  var control = [];

  for(var i = 2; i <= total; i++) {

    (function(i, data, control, worksheet){
      if(worksheet[`E${i}`] && worksheet[`N${i}`] && noAccents(worksheet[`E${i}`].v).toLowerCase() == 'lider' ) {
        var objVolunteer = _.filter(data, (o) =>  o._id.toString().trim() ===  worksheet[`N${i}`].v.toString().trim())

        var insert = {
            liderSocial: {
              voluntarioId: objVolunteer[0]._id,
              nome: objVolunteer[0].nome,
              urlAvatar: objVolunteer[0].urlAvatar,
              cargo: '',
              email: objVolunteer[0].email,
            }
        }

        DB.comite.update({_id: objVolunteer[0].comite}, insert, (err, res) => {
          if(err) { throw err }
          control.push(objVolunteer)
          if( control.length === data.length ) return reply({ message: "Dados de voluntario(s) importados com sucesso", data })
        })
      }
    })(i, data, control, worksheet)

  }
}

/* Funções que filtram os dados do Excel */

// Filtra dados de Cidade e Empresa
function filterExcel(workbook, cols, template, compareTo) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]

  var workSheetJson = xlsx.utils.sheet_to_json(worksheet, {header:"A"});
  var total = workSheetJson.length;

  var result = [];
  for(var i = 1; i < total; i++) {
    if (_.findIndex(result, (o) => o[compareTo] ===  workSheetJson[i][cols[0]].toString().trim() ) === -1) {
      var obj = {};
      for(var j = 0; j < template.length; j++) {
        obj[template[j]] = workSheetJson[i][cols[j]] ? workSheetJson[i][cols[j]].toString().trim() : '';
      }
      result.push(obj)
    }
  }

  return result;
}

// Filtra dados de Comite
function filterExcelCommittee(workbook, cols, template) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]
  var workSheetJson = xlsx.utils.sheet_to_json(worksheet, {header:"A"});
  var total = workSheetJson.length;

  var result = [];
  for(var i = 1; i < total; i++) {
    if (_.findIndex(result, (o) => {
      if (!workSheetJson[i][cols[0]]) {
        return o.cidade === workSheetJson[i][cols[1]].toString().trim() && o.empresa === workSheetJson[i][cols[2]].toString().trim()
      }
      return o.unidade === workSheetJson[i][cols[0]].toString().trim() && o.cidade === workSheetJson[i][cols[1]].toString().trim() && o.empresa === workSheetJson[i][cols[2]].toString().trim()
    }) === -1) {
      var obj = {};
      for(var j = 0; j < template.length; j++) {
        obj[template[j]] = workSheetJson[i][cols[j]] ? workSheetJson[i][cols[j]].toString().trim(): '';
      }
      result.push(obj)
    }
  }

  return result;
}

// Filtra dados de Voluntario - Importação de voluntario
function filterExcelVolunteer(workbook, cols, template, compareTo) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]
  var workSheetJson = xlsx.utils.sheet_to_json(worksheet, {header:"A"});
  var total = workSheetJson.length;

  var result = [];
  for(var i = 1; i < total; i++) {
    if (checkMail(workSheetJson[i].J, workSheetJson[i].K)
        && _.findIndex(result, (o) => o.email ===  checkMail(workSheetJson[i].J, workSheetJson[i].K) ) === -1) {

      var usuarioSharePoint = checkMail(workSheetJson[i].J, workSheetJson[i].K).match(/algar/g) ? true : false;
      var email = checkMail(workSheetJson[i].J, workSheetJson[i].K);

      result.push({
        nome: fixName(workSheetJson[i].E),
        nomeNormalizado: noAccents(workSheetJson[i].E.toLowerCase()),
        cpf: checkCpf(workSheetJson[i].F),
        email: email,
        emailSuperior: '',
        comite: workSheetJson[i].N,
        tamanhoCamiseta: workSheetJson[i].G,
        telefoneProfissional: workSheetJson[i].H && (workSheetJson[i].H.match(/\d/g) || []).length > 8 ? workSheetJson[i].H.match(/\d/g).join('').substring(0,11) : "",
        telefonePessoal: workSheetJson[i].I && (workSheetJson[i].I.match(/\d/g) || []).length > 8 ? workSheetJson[i].I.match(/\d/g).join('').substring(0,11) : "",
        associadoExecutivo: false,
        aceitouTermo: false,
        aceitouTermoEsseAno: false,
        usuarioSharePoint: usuarioSharePoint,
        cadastradoPeloLider: false,
        urlAvatar: usuarioSharePoint ? `/avatar?email=${email}` : '',
        dataCadastro: new Date(),
      });
    }
  }

  return result;
}

// Filtra dados de Lider - Importação de comite
function filterExcelLeader(workbook, cols, template, compareTo) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]
  var workSheetJson = xlsx.utils.sheet_to_json(worksheet, {header:"A"});
  var total = workSheetJson.length;

  var result = [];
  for(var i = 1; i < total; i++) {
    if (checkMail(workSheetJson[i].J)
        && _.findIndex(result, (o) => o.email ===  checkMail(workSheetJson[i].J, workSheetJson[i].K) ) === -1) {

      var usuarioSharePoint = checkMail(workSheetJson[i].J, workSheetJson[i].K).match(/algar/g) ? true : false;
      var email = checkMail(workSheetJson[i].J);

      result.push({
        nome: fixName(workSheetJson[i].F),
        nomeNormalizado: noAccents(workSheetJson[i].F.toLowerCase()),
        cpf: checkCpf(workSheetJson[i].G),
        email: email,
        emailSuperior: '',
        comite: workSheetJson[i].M,
        tamanhoCamiseta: 'M',
        telefoneProfissional: workSheetJson[i].H && (workSheetJson[i].H.match(/\d/g) || []).length > 8 ? workSheetJson[i].H.match(/\d/g).join('').substring(0,11) : "",
        telefonePessoal: workSheetJson[i].I && (workSheetJson[i].I.match(/\d/g) || []).length > 8 ? workSheetJson[i].I.match(/\d/g).join('').substring(0,11) : "",
        associadoExecutivo: false,
        aceitouTermo: false,
        aceitouTermoEsseAno: false,
        usuarioSharePoint: checkMail(workSheetJson[i].J).match(/algar/g) ? true : false,
        cadastradoPeloLider: false,
        urlAvatar: usuarioSharePoint ? `/avatar?email=${email}` : '',
        dataCadastro: new Date(),
      });
    }
  }

  return result;
}

/* Funções qu escrevem o Id no excel*/

// Escreve Id de cidade na planilha
function writeCidadeId(workbook, celltoWrite, data, reply, filePath) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]

  worksheet[`${celltoWrite}1`] = {
    t: 's',
    v: 'Id Cidade',
    r: '<t xml:space="preserve">Id Cidade</t>',
    h: 'Id Cidade',
    w: 'Id Cidade'
  }

  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1))

  for(var i = 2; i <= total; i++) {
    var objCidade = _.filter(data, (o) => o.nome.toString().trim() ===  worksheet[`A${i}`].v.toString().trim())

    worksheet[`${celltoWrite}${i}`] = {
      t: 's',
      v: objCidade[0]._id,
      r: `<t xml:space="preserve">${objCidade[0]._id}</t>`,
      h: objCidade[0]._id,
      w: objCidade[0]._id
    }

  }

  worksheet["!ref"] = `A1:${celltoWrite}${total}`
  xlsx.writeFile(workbook,  filePath);

  return reply({ message: "Dados de cidade(s) importados com sucesso", data })
}

// Escreve Id de empresa na planilha
function writeEmpresaId(workbook, celltoWrite, data, reply, filePath) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]

  worksheet[`${celltoWrite}1`] = {
    t: 's',
    v: 'Id Empresa',
    r: '<t xml:space="preserve">Id Empresa</t>',
    h: 'Id Empresa',
    w: 'Id Empresa'
  }

  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1))
  for(var i = 2; i <= total; i++) {
    var objCidade = _.filter(data, (o) => o.nome.toString().trim() ===  worksheet[`C${i}`].v.toString().trim())

    worksheet[`${celltoWrite}${i}`] = {
      t: 's',
      v: objCidade[0]._id,
      r: `<t xml:space="preserve">${objCidade[0]._id}</t>`,
      h: objCidade[0]._id,
      w: objCidade[0]._id
    }

  }

  worksheet["!ref"] = `A1:${celltoWrite}${total}`
  xlsx.writeFile(workbook,  filePath);

  return reply({ message: "Dados de empresa(s) importados com sucesso", data })
}

// Escreve Id de comite na planilha
function writeComiteId(workbook, cols, celltoWrite, data, reply, filePath) {
  var worksheet = workbook.Sheets[workbook.SheetNames[0]]
  worksheet[`${celltoWrite}1`] = {
    t: 's',
    v: 'Id Comite',
    r: '<t xml:space="preserve">Id Comite</t>',
    h: 'Id Comite',
    w: 'Id Comite'
  }

  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1))
  for(var i = 2; i <= total; i++) {
    var objCidade;
    if(!worksheet[`${cols[0]}${i}`]) {
      objCidade = _.filter(data, (o) => {
        return o.cidade.toString().trim() ===  worksheet[`${cols[1]}${i}`].v.toString().trim() && o.empresa.toString().trim() ===  worksheet[`${cols[2]}${i}`].v.toString().trim()
      })
    } else {
      objCidade = _.filter(data, (o) => {
        return o.unidade.toString().trim() ===  worksheet[`${cols[0]}${i}`].v.toString().trim() && o.cidade.toString().trim() ===  worksheet[`${cols[1]}${i}`].v.toString().trim() && o.empresa.toString().trim() ===  worksheet[`${cols[2]}${i}`].v.toString().trim()
      })
    }

    if(objCidade.length > 0) {

      worksheet[`${celltoWrite}${i}`] = {
        t: 's',
        v: objCidade[0]._id,
        r: `<t xml:space="preserve">${objCidade[0]._id}</t>`,
        h: objCidade[0]._id,
        w: objCidade[0]._id
      }
    }

  }

  worksheet["!ref"] = `A1:${celltoWrite}${total}`
  xlsx.writeFile(workbook,  filePath);

  return reply({ message: "Dados de comite(s) importados com sucesso", data })
}

// Escreve Id de voluntario na planilha - Importação de voluntario
function writeVolunteerId(workbook, celltoWrite, data, reply, filePath) {

  var worksheet = workbook.Sheets[workbook.SheetNames[0]]

  worksheet[`${celltoWrite}1`] = {
    t: 's',
    v: 'Id Voluntario',
    r: '<t xml:space="preserve">Id Voluntario</t>',
    h: 'Id Voluntario',
    w: 'Id Voluntario'
  }

  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1))
  for(var i = 2; i <= total; i++) {
    var objVolunteer;
    if(worksheet[`F${i}`]) {
      objVolunteer = _.filter(data, (o) => {
        return o.email.toString().trim() === checkMail((worksheet[`J${i}`] || {v: ''}).v , (worksheet[`K${i}`] || {v: ''}).v)
          && o.cpf.toString().trim() === worksheet[`F${i}`].v.toString().trim()
          && o.nome.toString().trim() === fixName(worksheet[`E${i}`].v);
      })
    } else {
      objVolunteer = _.filter(data, (o) => {
        return o.email.toString().trim() === checkMail((worksheet[`J${i}`] || {v: ''}).v , (worksheet[`K${i}`] || {v: ''}).v).trim()
          && o.nome.toString().trim() === fixName(worksheet[`E${i}`].v);
      })
    }
    if(objVolunteer.length > 0) {
      worksheet[`${celltoWrite}${i}`] = {
        t: 's',
        v: objVolunteer[0]._id,
        r: `<t xml:space="preserve">${objVolunteer[0]._id}</t>`,
        h: objVolunteer[0]._id,
        w: objVolunteer[0]._id
      }
    }
  }

  worksheet["!ref"] = `A1:${celltoWrite}${total}`
  xlsx.writeFile(workbook, filePath);
  return reply({ message: "Dados de voluntario(s) importados com sucesso", data })
}

// Escreve Id de lider na planilha - Importação de comite
function writeLeaderId(workbook, celltoWrite, data, reply, filePath) {

  var worksheet = workbook.Sheets[workbook.SheetNames[0]]

  worksheet[`${celltoWrite}1`] = {
    t: 's',
    v: 'Id Voluntario',
    r: '<t xml:space="preserve">Id Voluntario</t>',
    h: 'Id Voluntario',
    w: 'Id Voluntario'
  }

  var total = parseInt(worksheet["!ref"].split(':')[1].slice(1))
  for(var i = 2; i <= total; i++) {
    var objVolunteer = [];
    if(worksheet[`F${i}`]) {
      objVolunteer = _.filter(data, (o) => {
        return o.email.toString().trim() === checkMail((worksheet[`J${i}`] || {v: ''}).v).trim();
      })
    }

    if(objVolunteer.length > 0) {
      worksheet[`${celltoWrite}${i}`] = {
        t: 's',
        v: objVolunteer[0]._id,
        r: `<t xml:space="preserve">${objVolunteer[0]._id}</t>`,
        h: objVolunteer[0]._id,
        w: objVolunteer[0]._id
      }
    }
  }

  worksheet["!ref"] = `A1:${celltoWrite}${total}`
  xlsx.writeFile(workbook, filePath);
  checkLeaders(workbook, data, reply)
}

/* Funções ultilitarias */

// Normaliza uma string de nome - cada palavra tem o primeiro caracter maisculo
function fixName(nome) {
 return nome.trim().toLowerCase().split(' ').map(element => element.substring(0,1).toUpperCase() + element.substring(1)).join(' ');
}

// Verifica se o cpf é valido
function checkCpf(cpf) {
 var add = 0
 var rev

 if (!cpf) {
   return ''
 }
 // Elimina CPFs invalidos conhecidos
 if (cpf.length !== 11 || cpf.match(new RegExp(cpf.charAt(0), 'g')).length == 11) {
   return ''
 }
 // Valida 1o digito

 for (var i = 0; i < 9; i += 1) {
   add += parseInt(cpf.charAt(i), 10) * (10 - i)
 }
 rev = 11 - (add % 11)
 if (rev === 10 || rev === 11) {
   rev = 0
 }
 if (rev !== parseInt(cpf.charAt(9), 10)) {
   return ''
 }

 // Valida 2o digito
 add = 0
 for (var i = 0; i < 10; i += 1) {
   add += parseInt(cpf.charAt(i), 10) * (11 - i)
 }
 rev = 11 - (add % 11)
 if (rev === 10 || rev === 11) {
   rev = 0
 }
 if (rev !== parseInt(cpf.charAt(10), 10)) {
   return ''
 }
 return cpf
}

// Verifica se o email é valido
function checkMail(primary, secondary) {
 if (primary) {
   if (primary.indexOf('@') !== -1) {
     return primary.toLowerCase().trim();
   } else {
     if(secondary) {
       if (secondary.indexOf('@') !== -1) {
         return secondary.toLowerCase().trim();
       }
     }
   }
 } else if(secondary) {
   if (secondary.indexOf('@') !== -1) {
     return secondary.toLowerCase().trim();
   }
 }
 return ""
}
