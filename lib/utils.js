var mail = require('../utils/send-mail.js');

exports.register = (server, options, next) => {

  server.route({
    method: ['GET', 'POST'],
    path: '/utils/mail',
    config: {
      description: 'Envia email',
    },
    handler: function (request, reply) {
      const req = request.query.para ? request.query : request.payload
      const para = req.para
      // console.log(req)
      delete req.para
        mail(para, 'Envio de email', 'este email foi enviado pela api', (err, info) => {
          if (err) { throw err }
          return reply(info)
        }, req)
    }
  });

  return next();
}

exports.register.attributes = {
    name: 'utils',
    version: require('../package.json').version
};
