const Connection = require('tedious').Connection;
const Request = require('tedious').Request;
const TYPES = require('tedious').TYPES;
const ASQ = require('asynquence');

var config = require('../config.json').database.SQLServer;
// var config = require('../config.json').database.SQLServerDev;

if (process.env.NODE_ENV === 'development') {
  config = require('../config.json').database.SQLServerDev
}
if (process.env.NODE_ENV === 'test') {
  config = require('../config.json').database.SQLServerHomo
}

module.exports = (Boom) => {
  const module = {};

  module.verifica = (cpf, callback, i) => {
    // Connection SQLServer
    const connection = new Connection(config);
    connection.on('connect', (err) => {
      ASQ()
        .then((done) => {
          executeStatement(done);
        })
        .then((done, result) => {
          if (!result || result.length === 0) {
            return callback(Boom.badRequest(
              'Não encontramos seu CPF. Este programa é para associados do Grupo Algar.'
            ), i)
          }
          return callback({
            nomeCompleto: fixText(result[0].nome_usuario),
            cargoId: result[0].tipo_cargo_fk,
            cargoNome: fixText(result[0].nome_cargo),
            centroResultadoId: null,
            centroResultadoNome: fixText(result[0].centro_resultado),
            associadoExecutivo: (result[0].executivo) ? result[0]
              .executivo : false,
            dataDemissao: result[0].data_demissao,
            dataNasc: result[0].data_nasc,
            cpf: result[0].cpf,
            genero: result[0].sexo, // verificar com o fernando o tratamento depois de pegar a base final
          }, i)
        })
    });

    const executeStatement = done => {
        request = new Request("SELECT TOP 1 " +
          "usr.nome_usuario, usr.cpf, usr.rg," +
          "usr.centro_resultado," +
          "usr.data_demissao," +
          "usr.data_nasc," +
          "usr.nome_cargo," +
          "usr.tipo_cargo_fk," +
          "usr.sexo," +
          "usr.executivo " +
          "FROM usuario_dados usr " +
          "WHERE usr.cpf <> '' and usr.cpf is not null and usr.cpf = @cpf " +
          "order by usr.data_demissao ASC, usr.usuario_pk DESC;", (err) => {
            if (err) {
              throw err
            }
          })

        const result = []
        request.on('row', (columns) => {
          const obj = {}
          columns.forEach((column) => {
            if (column.value !== null) {
              obj[column.metadata.colName] = column.value
            }
          })
          result.push(obj)
        })

        request.on('doneInProc', (rowCount, more, rows) => {
          done(result)
        })

        request.addParameter('cpf', TYPES.VarChar, cpf)

        connection.execSql(request)
      } // execute

    const fixText = Text => {
      return (Text) ? Text.trim().toLowerCase().split(' ').map(element =>
        element.substring(0, 1).toUpperCase() + element.substring(1)).join(
        ' ') : ''
    }

  }; // module.verifica

  return module;
}
