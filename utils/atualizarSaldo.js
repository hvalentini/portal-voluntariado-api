const ASQ = require('asynquence');

module.exports = (Mongoose, DB, Boom) => {
  const module = {};
  module.POST =  (id, callback) => {
    const _id = typeof id === 'string' ? Mongoose.Types.ObjectId(id) : id;
    var error;
    ASQ()
    .then((done) => {
      DB.comite.findOne({ _id: _id }).exec((err, object) => {
        if (err) return done.fail(Boom.badRequest(err))
        if (!object) return done.fail(Boom.badRequest('Não existe comite para esse ID'))
        done(object)
      })
    })
    .then((done, comite) => {
      DB.acao.aggregate([
        { $match: {
          comite: _id,
        }},
        { $group: {
          _id: "comite",
          total: { $sum: "$valorGasto" },
        }}
      ]).exec((err, objects) => {

        if (err) return done.fail(Boom.badRequest(err))
        // if (objects.length === 0) return done.fail(Boom.conflict('Não existem ações para esse comite'));
        const totalGasto = objects.length > 0 ? objects[0].total : 0
        comite.verbaInicial = comite.verbaInicial ? comite.verbaInicial : 0
        comite.verbaExtra = comite.verbaExtra ? comite.verbaExtra : 0
        comite.saldoAtual = (comite.verbaInicial + comite.verbaExtra) - totalGasto
        done(comite);
      })
    })
    .then((done, comite) => {
      comite.save((err, doc) => {
        if (err) done.fail(Boom.badRequest(err))
        callback(err, doc.saldoAtual);
        done();
      })
    })
    .or((err) => {
      callback(err, null)
    });
  };

  return module;
}
