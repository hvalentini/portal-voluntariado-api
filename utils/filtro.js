const Mongoose = require('mongoose');

/**
* @query - o filtro vindo da requisição
* @objectIds ? - array com os campos que dever ser tradados como objectId
*/

const formatar = (query, objectIds) => {
  if(!query) { return {} }

  const filtro = {}

  query = JSON.parse(query)
  Object.keys(query).map((key, index) => {
    const valor = query[key]
    // const q = regexObject(valor)
    // console.log('====', valor);
    if(valor === '' || valor === null) { return null }
    if(valor === true || valor === false)
      return filtro[key] = valor
    if (key === 'nomeNormalizado')
        return filtro[key] = regexObject(valor)
    if (key === 'tipo')
      return filtro[key] = valor
    if (objectIds && objectIds.indexOf(key) >= 0) {
      return filtro[key] = Mongoose.Types.ObjectId(valor)
    }
    if ((Number(valor) || Number(valor) === 0) && key !== 'nomeNormalizado' && key !== '')
      return filtro[key] = { $gte: Number(valor) }
    if (Date.parse(valor))
      return filtro = verificaDatas(query, key, filtro)
      // return filtro[key] = { $lte: new Date(valor).toISOString() }
    if (typeof valor === 'string') {
      if(key === 'statusAcao') {
        // return filtro = verificaDatas(query, valor, filtro)
        return null
      } //ESPECIFICO PARA DATAS DIFERENTES DATA INICIAL > E DATAFINAL
      if(key.indexOf('$text') >= 0) {
        const keys = key.split('_')
        const obj = {}
        filtro.$or = [{ $text: { $search: valor} }]
        obj[keys[1]] = regexObject(valor)
        return filtro.$or.push(obj)
      }
      return filtro[key] = regexObject(valor)
    }
    return filtro[key] = valor
  });

  return filtro;
}

function verificaDatas(query, key, filtro) {

  const status = query.statusAcao
  if (!query.statusAcao) {
    if (key === 'dataInicial') {
      return filtro['dataInicial'] = { $gte: new Date(query[key]) }
    }
    return filtro[key] = { $lte: new Date(query[key]) }
  }

  if (status === 'todas') return null

  if (status === 'andamento') {
    if (key === 'dataInicial') {
      filtro['dataInicial'] = { $lte: new Date(query[key]) }
    } else {
      filtro['dataFinal'] = { $gte: new Date(query[key]) }
    }
  }

  if (status == 'programadas' && key === 'dataInicial') {
    filtro['dataInicial'] = { $gte: new Date(query[key]) }
    // filtro['dataFinal'] = { $gte: new Date(dataFinal).toISOString() }
  }

  if(key == 'concluidas') {
    if (key === 'dataInicial') {
      filtro['dataInicial'] = query[key] ? { $gte: new Date(query[key]) } : { $lte: new Date() }
    } else {
      filtro['dataFinal'] = { $lte: new Date(query[key]) }
    }
  }

  // console.log(filtro)
  return filtro
}

function formatRegex(keyword) {
  return keyword.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&');
}

function regexObject(keyword) {
  return { $regex: formatRegex(keyword), $options: 'i'};
}

// Ajusta ids antigos do filtro ou da ordenação para um novo padrao exemplo no listar de ação 
const parseIds = (obj, oldIds, newIds) => {
  const newObj = {}
  Object.keys(obj).map((key, i) => {
    const value = obj[key]
    const index = oldIds.indexOf(key)
    if (index >= 0) {
      newObj[newIds[index]] = value
    } else {
      newObj[key] = value
    }
  })
  return newObj
}

module.exports = { formatar, parseIds };
