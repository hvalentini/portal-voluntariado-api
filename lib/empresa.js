var Joi = require('joi');
var Boom = require('boom');
var Mongoose = require('mongoose');
var DB = require('./db');
var types = require('mongoose').Types;
var noAccents = require('remove-accents');
const Filtro = require('../utils/filtro');

exports.register = function (server, options, next) {

  server.route({
    method: 'POST',
    path: '/empresa',
    config: {
      description: 'Cadastrar uma empresa',
      validate: {
          payload: {
              nome: Joi.string().required().description('Nome do usuário')
          }
      },
    },
    handler: function (request, reply) {
      // var emp = new DB.empresa({ nome: request.payload.nome.replace(/^\s+|\s+$/g,""),
      //                            nomeNormalizado: noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"") })
      // emp.save(function(err, empresas) {
      //   if (err)  return reply(Boom.conflict('Já existe uma empresa com esse nome'));
      //    return reply(empresas);
      // })

      const nome = request.payload.nome.replace(/^\s+|\s+$/g,"")
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")

      DB.empresa.find({ nomeNormalizado: nomeNormalizado }, (err, empresas) => {
        if (empresas.length > 0) {
          return reply(Boom.conflict('Já existe uma empresa com esse nome'));
        }

        const empresa = new DB.empresa({ nome,  nomeNormalizado });

        empresa.save((err, doc) => {
          if (err) { return console.error(err); }
          return reply(doc);
        });
      });

    }
  });

  server.route({
    method: 'GET',
    path: '/empresa',
    config: {
      description: 'Buscar empresas',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      }
    },
    handler: function (request, reply) {
      var ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : {nomeNormalizado: 1};
      var filtro = request.query.filtro ?  Filtro.formatar(request.query.filtro) : {};

      // console.log('fil====================>', filtro)

      DB.empresa.find(filtro).sort(ordem).exec(function(err, empresas) {
        if(err) return console.log(err);
        reply(empresas);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/empresa/{id}',
    config: {
      description: 'Detalhar empresas',
      validate: {
        params: {
          id: Joi.string().min(24).max(24).required().description('Id da empresa é requerido'),
        }
      },
    },
    handler: function (request, reply) {
      DB.empresa.findOne({_id: request.params.id} , function(err, empresas) {
        if (empresas === null) {
         return reply(Boom.conflict('Não existe empresa para esse ID')) ;
      }
      if (err) return console.log(err);
         return reply(empresas);
      })
    }
  });


  server.route({
    method: 'PUT',
    path: '/empresa',
    config: {
      description: 'Editar empresa',
      validate: {
          payload: {
              nome: Joi.string().required().description('Nome da empresa'),
              nomeNormalizado: Joi.string().required().description('Nome normalizado da empresa'),
              _id: Joi.string().required().description('Id da empresa'),
          }
      },
    },
    handler: function (request, reply) {
      const id = { _id : Mongoose.Types.ObjectId(request.payload._id) };
      const nome = request.payload.nome;
      const nomeNormalizado = noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")
      var editarEmpresa = {
        $set: {
          nome: request.payload.nome.replace(/^\s+|\s+$/g,""),
          nomeNormalizado: noAccents(request.payload.nome).toLowerCase().replace(/^\s+|\s+$/g,"")
        }
      }

      DB.empresa.find({ _id: { $ne: id }, nomeNormalizado: nomeNormalizado }, (err, empresas) => {
        if (empresas.length > 0) {
          return reply(Boom.conflict('Já existe uma empresa com esse nome'));
        }
        DB.empresa.update(id, editarEmpresa, (err, res) => {
          if(err) return console.log(err);
          reply({ res, message: 'O nome da empresa foi alterado com sucesso' });
        })
      })
    }
  });

  server.route({
    method: 'DELETE',
    path: '/empresa',
    config: {
      description: 'Deletar empresa',
      validate: {
        query: {
          _id: Joi.string().required().description('ID do local a ser deletado!')
        }
      }
    },
    handler: function(request, reply) {
      var id = {
        _id: Mongoose.Types.ObjectId(request.query._id),
      }
      DB.empresa.find(id, (error, removerEmpresa) => {
        if (removerEmpresa.length === 0){
          return reply(Boom.conflict('Id da empresa informado não foi encontrado'))
        }

        DB.comite.findOne({ empresa: types.ObjectId(request.query._id) }, function(err, comiteRelacionado) {
          if (comiteRelacionado !== null) {
            return reply(Boom.conflict('Há um comitê vinculado à essa empresa. Não é possível excluí-la'));
          }
          DB.empresa.remove(id, function(err,res){
            if (err) return console.error(err);
            return reply ({ res, message: 'A empresa foi excluída com sucesso.'});
          })
        })
      })
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/empresa/normalizar',
    config: {
      description: 'Normaliza nome de empresas',
    },
    handler: function (request, reply) {
      DB.empresa.find({}, (err, docs) => {
        if (err) { throw err }

        if(docs.length < 1) { return reply(Boom.unauthorized("Não a empresas cadastradas"))}

        var control = []

        docs.forEach((company) => {
          (function(control){
            var updateValue = {
              $set: {
                nomeNormalizado: noAccents(company.nome).toLowerCase(),
              }
            }
            DB.empresa.update({ _id: types.ObjectId(company._id) }, updateValue, (err, resp) => {
              control.push(resp)
              if(control.length === docs.length) { return reply({ message: "Empresas normalizadas com sucesso" }) }
            })
          })(control)
        });
      });
    }
  });

  return next();
}

exports.register.attributes = {
    name: 'empresa',
    version: require('../package.json').version
};

function obtemFiltro(query){
  var filtro = {};

  Object.keys(query).map(function(key, index) {
    console.log(key, index);

    if(typeof query[key] === 'number')
      filtro[key] = { $gte: query[key] };
    else if (typeof query[key] === 'string')
      filtro[key] = { $regex: query[key], $options: 'i'};
    else
      filtro[key] = query[key];
  });

  return filtro;
}
