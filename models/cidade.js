module.exports = (Mongoose) => {
  var cidade = new Mongoose.Schema({
    'nome': { type: String, required: true, upsert: true },
    'uf': { type: String, required: true, upsert: true },
    'nomeNormalizado': { type: String },
    '__v': { type: Number, select: false},
  })

  return Mongoose.model('cidade', cidade);
}
