const fsExtra = require('fs.extra');
const Request = require('request');
const Joi = require('joi');
const fs = require('fs');
const path = require('path');

const baseDirIMG = `public/images`
const baseDir = `${baseDirIMG}/avatars`

exports.register = (server, options, next) => {

    server.route({
        method: 'GET',
        path: '/avatar-convert',
        config: {
            description: 'Faz o upload dos arquivos',
            cache: {
              expiresIn: 30 * 1000,
              privacy: 'private',
            },
            validate: {
              query: {
                // email: Joi.string().description('email de referencia da imagem'),
              }
            },
            handler: (request, reply) => {
              const email = request.query.email
              const pathFile = path.join(baseDir, `${email}.jpg`);

              const uri = 'https://login.microsoftonline.com/extSTS.srf';
              const username = 'enesolucoesdev@algar.com.br';
              const password = 'Lafo3613';
              const endpoint = 'https://algarnet.sharepoint.com';
              const body = '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:a="http://www.w3.org/2005/08/addressing" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><s:Header><a:Action s:mustUnderstand="1">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action><a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo><a:To s:mustUnderstand="1">https://login.microsoftonline.com/extSTS.srf</a:To><o:Security s:mustUnderstand="1" xmlns:o="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><o:UsernameToken><o:Username>'
                + username + '</o:Username><o:Password>' + password + '</o:Password></o:UsernameToken></o:Security></s:Header><s:Body><t:RequestSecurityToken xmlns:t="http://schemas.xmlsoap.org/ws/2005/02/trust"><wsp:AppliesTo xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy"><a:EndpointReference><a:Address>'
                + endpoint + '</a:Address></a:EndpointReference></wsp:AppliesTo><t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType><t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType><t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType></t:RequestSecurityToken></s:Body></s:Envelope>'

              Request({
                headers: {
                  // 'Content-Length': contentLength,
                  'Content-Type': 'application/json; odata=verbose'
                },
                uri: uri,
                body: body,
                method: 'POST'
              }, function (err, res, body) {
                if (err) { throw err }
                console.log('=========> res')
                console.log(res)
                console.log('=========> body')
                console.log(body)
              });
              // return "oi";
              // $xml = new DOMDocument();
              // $xml->loadXML($response->raw_body);
              //
              // if ($xml === false) {
              //     return "Ocorreu um erro";
              // }
              //
              //     $els = "";
              //     $loginErrado = false;
              //     foreach ($xml->getElementsByTagNameNS('http://www.w3.org/2003/05/soap-envelope', 'Fault') as $element) {
              //         $loginErrado = true;
              //         //$els .=  'local name: ' . $element->localName . ', prefix: ' . $element->prefix  . ', value: ' . $element->nodeValue. "\n" ;
              //         $els .=  ' ' . $element->nodeValue. "\n" ;
              //     }
              //     //verifica se o login errado
              //     if ($loginErrado) {
              //         //retornar JSON
              //         return '{ "status": 400, "mensagem": "'. $els .'" }';
              //     }
              //
              //     $sToken = "";
              //     foreach ($xml->getElementsByTagNameNS('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'BinarySecurityToken') as $element) {
              //         $sToken = $element->nodeValue;
              //     }
            } // end handle
          }
    });

    return next();
};

exports.register.attributes = {
    name: 'avatar'
};
