const Joi = require('joi');
const DB = require('./db');
const Boom = require('boom');
const Mongoose = require('mongoose');
const Filtro = require('../utils/filtro');
const mail = require('../utils/send-mail');
const AtualizarSaldo = require('./../utils/atualizarSaldo.js')(Mongoose, DB, Boom);
var noAccents = require('remove-accents');
var types = require('mongoose').Types;

exports.register = (server, options, next) => {

  server.route({
    method: 'GET',
    path: '/comite',
    config: {
      description: 'Busca de comites',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta')
        }
      },
    },
    handler: (request, reply) => {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "empresa.nomeNormalizado": 1, "cidadeNome": 1, "unidade": 1 };
      const filtro = Filtro.formatar(request.query.filtro, ['empresa', 'empresa._id','cidade']);

      DB.comite.find(filtro).sort(ordem).populate('empresa').exec((err, objects) => {
        if (err) { return console.error(err) }
        if (!objects) { return reply(Boom.conflict("Não foram encontrados comitês para a pesquisa informada")) }
        reply(objects);
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/comite/listar',
    config: {
      description: 'Busca de comites',
      validate: {
        query: {
          filtro: Joi.string().description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta'),
          skip: Joi.string().optional().description('Quantidade de elementos ignorados'),
        }
      },
    },
    handler: (request, reply) => {
      console.log(request.query.ordenacao)
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "empresas.nome": 1, "cidadeNome": 1, "unidadeNormalizada": 1 };
      const filtro = Filtro.formatar(request.query.filtro, ['empresas._id','cidade']);
      var query = []
      query = [
        { $lookup: {
          from: "acoes",
          localField: "_id",
          foreignField: "comite",
          as: "acao"
        }},
        { $lookup: {
          from: "empresas",
          localField: "empresa",
          foreignField: "_id",
          as: "empresas"
        }},
        { $unwind: "$empresas" },
        { $lookup: {
          from: "cidades",
          localField: "cidade",
          foreignField: "_id",
          as: "cidades"
        }},
        { $unwind: "$cidades" },
        { $lookup: {
          from: "voluntarios",
          localField: "_id",
          foreignField: "comite",
          as: "voluntarios"
        }},
        { $project: {
          _id: 1,
          empresas: 1,
          cidade: 1,
          cidadeNome: 1,
          cidadeUF: 1,
          unidade: 1,
          unidadeNormalizada: 1,
          cidades: 1,
          liderSocial: 1,
          saldoAtual: 1,
          verbaInicial: 1,
          verbaExtra: 1,
          acoes: { $size: "$acao" },
          voluntarios: { $size: "$voluntarios" },
          beneficiadas: {
            $reduce: {
              input: "$acao",
              initialValue: 0,
              in:{ sum: { $sum: ["$$value.sum", "$$this.totalBeneficiados"] } }
            }
          },
        }},
        { $match: filtro },
        { $sort: ordem },
      ]
      if(request.query.skip) {
        query.push({ $skip: Number(request.query.skip) })
      }
      DB.comite.aggregate(query, (err, docs) => {
        if(err) throw err;

        return reply(docs);
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/comite/{id}',
    config: {
      description: 'Detalha um comite pelo id',
    },
    handler: (request, reply) => {
      DB.comite.aggregate([
        { $match: { _id: Mongoose.Types.ObjectId(request.params.id) }},
        { $lookup: {
          from: "acoes",
          localField: "_id",
          foreignField: "comite",
          as: "acao"
        }},
        // { $lookup: {
        //   from: "empresas",
        //   localField: "empresa",
        //   foreignField: "_id",
        //   as: "empresa"
        // }},
        // { $unwind: "$empresa" },
        { $lookup: {
          from: "cidades",
          localField: "cidade",
          foreignField: "_id",
          as: "cidades"
        }},
        { $unwind: "$cidades" },
        { $lookup: {
          from: "voluntarios",
          localField: "_id",
          foreignField: "comite",
          as: "voluntarios"
        }},
        { $project: {
          _id: 1,
          empresa: 1,
          cidade: 1,
          cidadeNome: 1,
          cidadeUF: 1,
          unidade: 1,
          cidades: 1,
          liderSocial: 1,
          saldoAtual: 1,
          verbaInicial: 1,
          verbaExtra: 1,
          acoes: { $size: "$acao" },
          voluntarios: { $size: "$voluntarios" },
          beneficiadas: {
            $reduce: {
              input: "$acao",
              initialValue: 0,
              in:{ sum: { $sum: ["$$value.sum", "$$this.totalBeneficiados"] } }
            }
          }
        }},
      ], (err, docs) => {
        if (err) { return console.error(err) }
        if (!docs || docs.length < 1) { return reply(Boom.conflict("O id do comitê informado não foi encontrado")) }
        reply(docs[0]);
      })
    }
  });

  server.route({
    method: 'POST',
    path: '/comite',
    config: {
      description: 'Cadastra um comite',
      validate: {
          payload: {
              empresa: Joi.string().required().description('Id da empresa à qual o comite pertence'),
              cidade: Joi.string().required().description('Id da cidade à qual o comite pertence'),
              unidade: Joi.string().optional().allow('').description('unidade do comite'),
              verbaInicial: Joi.number().description('verba inicial do comite'),
              verbaExtra: Joi.number().description('verba extra do comite'),
              liderSocial: Joi.object().description('lider social do comite'),
          }
      },
    },
    handler: (request, reply) => {
      const empresa = objectId(request.payload.empresa);
      const cidade = objectId(request.payload.cidade);
      const unidade = request.payload.unidade;
      const query = { empresa, cidade };
      const txtParaUnidade = unidade ? ` na unidade ${unidade}` : ''
      if (unidade) { query.unidade = { $regex : new RegExp(unidade, "i") } }

      DB.comite.find(query).populate('empresa').exec((err, objects) => {
        if (err) { throw err }
        if (objects.length > 0) {
          return reply(Boom.conflict(`Já existe um comite cadastrado na cidade especificada para esta empresa`));
        }

        const dadosParaSalvar = request.payload
        const verbaInicial = request.payload.verbaInicial ? request.payload.verbaInicial : 0;
        const verbaExtra = request.payload.verbaExtra ? request.payload.verbaExtra : 0;
        dadosParaSalvar['saldoAtual'] = verbaInicial + verbaExtra;

        DB.cidade.findById(cidade).exec((erro, resultado) => {
          if(erro) throw erro;

          if( !resultado ) {
            return reply(Boom.conflict(`Não foi encontrado uma cidade com o id ${cidade}`));
          }

          dadosParaSalvar['cidadeNome'] = resultado.nome;
          dadosParaSalvar['cidadeUF'] =  resultado.uf;

          DB.empresa.findById(empresa).exec((erroEmp, resultadoEmp) => {
            if(erroEmp) throw erroEmp;

            if( !resultadoEmp ) {
              return reply(Boom.conflict(`Não foi encontrado uma empresa com o id ${empresa}`));
            }

            if(request.payload.unidade) {
              dadosParaSalvar['unidadeNormalizada'] =  noAccents(request.payload.unidade).toLowerCase().replace(/^\s+|\s+$/g,"");
            } else {
              dadosParaSalvar['unidadeNormalizada'] =  ''
            }
            const obj = new DB.comite(dadosParaSalvar)
            obj.save((err, docs) => {
              if (err) { return console.error(err); }
              // return reply(docs);

              const lider = docs.liderSocial
              if (lider) {
                const emailLider = conteudoEmailLider(docs, lider)
                mail(lider.email, emailLider.titleLider, emailLider.txtLider, (error, response) => {
                  if (error) throw error;

                  const notificacao = new DB.notificacao({
                    publicoAlvo: lider,
                    tipo: 'Novo lider social',
                    texto: emailLider.txtLider,
                    dataCadastro: obtemDataAtual()
                  });

                  notificacao.save((errorNotif) => { if (errorNotif) console.error(errorNotif); });

                  reply({ success: true, data: docs });
                }); // maill
              }
              else{
                reply({ success: true, data: docs });
              }
            }); //save
          });
        });
      }); // comite.find
    } // handle
  });

  server.route({
    method: 'PUT',
    path: '/comite',
    config: {
      description: 'Edita um comite',
      validate: {
          payload: {
              _id: Joi.string().required().description('Id do comite'),
              empresa: Joi.string().required().description('Id da empresa à qual o comite pertence'),
              cidade: Joi.string().required().description('Id da cidade à qual o comite pertence'),
              unidade: Joi.string().optional().allow('').description('unidade do comite'),
              verbaAtual: Joi.number().description('verba atual do comite'),
              verbaInicial: Joi.number().description('verba inicial do comite'),
              verbaExtra: Joi.number().description('verba extra do comite'),
              liderSocial: Joi.object().optional().allow({}).description('lider social do comite'),
          }
      },
    },
    handler: (request, reply) => {
      const _id = request.payload._id;
      const empresa = objectId(request.payload.empresa);
      const cidade = objectId(request.payload.cidade);
      const unidade = request.payload.unidade;
      const query = { empresa, cidade };
      const txtParaUnidade = unidade ? ` na unidade ${unidade}` : ''
      if (unidade) { query.unidade = { $regex : new RegExp(unidade, "i") } };
      DB.comite.find(query, (er, objects) => {
        if (er) { return console.error(er); }
        if (objects.length > 1 && _id != objects[0]._id) {
          return reply(Boom.conflict(`Já existe um comite cadastrado na cidade especificada para esta empresa`));
        }

        DB.cidade.findById(cidade).exec((erro, resultado) => {
          if(erro) throw erro;

          if( !resultado ) {
            return reply(Boom.conflict(`Não foi encontrado uma cidade o id ${cidade}`));
          }

          request.payload['cidadeNome'] = resultado.nome
          request.payload['cidadeUF'] =  resultado.uf

          if(request.payload.unidade) {
            request.payload['unidadeNormalizada'] =  noAccents(request.payload.unidade).toLowerCase().replace(/^\s+|\s+$/g,"");
          } else {
            request.payload['unidadeNormalizada'] =  ''
          }

          DB.empresa.findById(empresa).exec((erroEmp, resultadoEmp) => {
            if(erroEmp) throw erroEmp;

            if( !resultadoEmp ) {
              return reply(Boom.conflict(`Não foi encontrado uma empresa com o id ${empresa}`));
            }

            var enviaEmailNotificacao = false

            DB.comite.find({ _id: _id }, (err, comite) => {
              if (err) throw err;
              // console.log('LIDER === ', request.payload)
              if ((!comite.liderSocial && request.payload.liderSocial) ||
                  (comite.liderSocial.voluntarioId != request.payload.liderSocial.voluntarioId)) {
                    enviaEmailNotificacao = true
              }
            })

            DB.comite.findByIdAndUpdate(_id, request.payload).populate('empresa').exec()
              .then((object) => {
                if (enviaEmailNotificacao) {

                  const obj = new DB.comite(request.payload)
                  const lider = request.payload.liderSocial
                  const emailLider = conteudoEmailLider(object, lider)
                  mail(lider.email, emailLider.titleLider, emailLider.txtLider, (error, response) => {
                    if (error) console.error('ERROR', error);

                    const notificacao = new DB.notificacao({
                      publicoAlvo: lider,
                      tipo: 'Novo lider social',
                      texto: emailLider.txtLider,
                      dataCadastro: obtemDataAtual()
                    });

                    notificacao.save((errorNotif) => { if (errorNotif) console.error(errorNotif); });
                  }); // maill
                }
                AtualizarSaldo.POST(request.payload._id, (err, saldoAtual) => {
                  if (err){
                    console.error(err);
                    return reply(err);
                  }
                  return reply({ success: true, data: Object.assign(object, request.payload)});
                });
              }, (error) => {
                console.error(error);
                return reply(Boom.badRequest(error));
              });
          });
        });
        // DB.comite.findById(_id).exec((err, object) => {
        //   const obj = new DB.comite(request.payload)
        //   obj.save((erro, docs) => {
        //     if (erro) { return console.error(erro); }
        //
        //     if (object.liderSocial.voluntarioId !== obj.liderSocial.voluntarioId) {
        //       const lider = docs.liderSocial
        //       const emailLider = conteudoEmailLider(docs, lider)
        //       mail(lider.email, emailLider.titleLider, emailLider.txtLider, (error, response) => {
        //         if (error) throw error;
        //
        //         reply({ success: true, data: docs });
        //       }); // maill
        //     }
        //     else {
        //       reply({ success: true, data: docs });
        //     }
        //   }); // save
        // }); //comite findById
      }); // comite.find
    } // handle
  });

  server.route({
    method: 'DELETE',
    path: '/comite',
    config: {
      description: 'Deleta um comitê',
      validate: {
          query: {
            id: Joi.string().required().length(24).description('ID do comite')
          }
      },
    },
    handler: (request, reply) => {
      const id = objectId(request.query.id);
      DB.comite.findOne({ _id: id }, (er, comite) => {
        if (er) { return console.error(er); }
        if (comite === null) { return reply(Boom.conflict("O id do comitê informado não foi encontrado")); }

        DB.acao.findOne({ comite: id }, (err, acao) => {
          if (err) { return console.error(err); }
          if (acao !== null) { return reply(Boom.conflict("O id do comitê informado está vinculado a uma ação ou voluntário")); }

          DB.voluntario.findOne({ comite: id }, (erro, voluntario) => {
            if (erro) { return console.error(erro); }
            if (voluntario !== null) { return reply(Boom.conflict("O id do comitê informado está vinculado a um voluntário")); }

            comite.remove((err) => {
              if (err) return console.error(err);
              reply({ success: true, message: 'Comitê excluído com sucesso'});
            }); // remove obj
          }); // voluntario
        }); // acao
      }); //comite
    } // handler
  }); // route

  server.route({
    method: 'GET',
    path: '/comite/pessoas-benificiadas',
    config: {
      description: 'Lista o total de pessoas benificadas por uma ação',
      validate: {
        query: {
          idVoluntario: Joi.string().optional().description('Id do comite'),
          idComite: Joi.string().optional().description('Id do voluntario')
        }
      }
    },
    handler: (request, reply) => {
      var comite = DB.comite;
      var query = {};

      if(request.query.idComite){
        query._id = { $eq: objectId(request.query.idComite)};
      }
      if(request.query.idVoluntario){
        query.acao = { $elemMatch: { "participantes.voluntarioId": request.query.idVoluntario} };
      }
      comite.aggregate([
        { $lookup: {
          from: "acaos",
          localField: "_id",
          foreignField: "comite",
          as: "acao"
        }},
        { $lookup: {
          from: "empresas",
          localField: "empresa",
          foreignField: "_id",
          as: "empresaNome"
        }},
        { $unwind: "$empresaNome" },
        { $lookup: {
          from: "cidades",
          localField: "cidade",
          foreignField: "_id",
          as: "cidadeLocal"
        }},
        { $unwind: "$cidadeLocal" },
        { $match: query },
        { $project: {
          _id: 1,
          empresa: 1,
          cidade: 1,
          cidadeNome: 1,
          cidadeUF: 1,
          unidade: 1,
          empresaNome: 1,
          cidadeLocal: 1,
          acao: 1,
          soma: {
            $reduce: {
              input: "$acao",
              initialValue: 0,
              in:{ sum: { $sum: ["$$value.sum", "$$this.totalBeneficiados"] } }
            }
          }
        } },
      ],
      (err, docs) => {
        if(err) throw err;

        return reply(docs);
      });
    }
  });

  server.route({
    method: ['GET', 'POST'],
    path: '/comite/normalizarUnidade',
    config: {
      description: 'Normaliza a unidade do comite',
    },
    handler: function (request, reply) {
      DB.comite.find({}, (err, docs) => {
        if (err) { throw err }

        if(docs.length < 1) { return reply(Boom.unauthorized("Não existem comites cadastrados"))}

        var control = []

        docs.forEach((committee) => {
          (function(control){
            var updateValue = {
              $set: {
                unidadeNormalizada: noAccents(committee.unidade).toLowerCase(),
              }
            }
            DB.comite.update({ _id: types.ObjectId(committee._id) }, updateValue, (err, resp) => {
              control.push(resp)
              if(control.length === docs.length) { return reply({ message: "Unidades normalizadas com sucesso" }) }
            })
          })(control)
        });
      });
    }
  });


  server.route({
    method: 'POST',
    path: '/comite/saldo-atual/{id}',
    config: {
      description: 'Atualizar saldo atual',
    },
    handler: (request, reply) => {
      AtualizarSaldo.POST(request.params.id, (err, saldoAtual) => {
        if (err){
          console.error(err);
          return reply(err);
        }
        reply({ success: true, data: saldoAtual });
      });
    }
  });

  // sincronizar o retorno da requisição para deixar o texto do e-mail em um só lugar
  function conteudoEmailLider(comite, lider) {
    const txtParaUnidade = comite.unidade ? ` na unidade ${comite.unidade}` : ''
    const titleLider = `Parabéns ${lider.nome}, você acaba de se tornar um líder social do Grupo Algar`
    const txtLider = `Parabéns ${lider.nome}, você acaba de se tornar o líder social do comitê da empresa ${comite.empresa.nome}` +
    ` em ${comite.cidadeNome} - ${comite.cidadeUF}${txtParaUnidade}.`

    return { titleLider, txtLider }
  }

  function objectId(id) {
    return Mongoose.Types.ObjectId(id);
  }

  return next();
}

exports.register.attributes = {
    name: 'comite',
    version: require('../package.json').version
};

function obtemDataAtual() {
  var now = new Date();
  now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));
  return now;
}
