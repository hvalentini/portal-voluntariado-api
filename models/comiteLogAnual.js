module.exports = (Mongoose) => {

  var comiteLogAnual = new Mongoose.Schema({
    'comiteId':           { type: Object,   required: true  },
    'verbaInicial':       { type: Number,   required: true  },
    'verbaExtra':         { type: Number,   required: false },
    'liderSocial':        { type: Object,   required: false },
    'saldoRemanescente':  { type: Number,   required: true  },
    'ano':                { type: Number,   required: true  },
    'dataCadastro':       { type: Date,     required: true  },
    '__v': { type: Number, select: false},
  })

  return Mongoose.model('comiteLogAnual', comiteLogAnual, 'comiteLogAnual');
}
