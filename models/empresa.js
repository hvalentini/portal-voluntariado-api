module.exports = (Mongoose) => {
  var empresa = new Mongoose.Schema({
    'nome': { type: String, required: true, unique:true, upsert: true },
    'nomeNormalizado': { type: String },
    '__v': { type: Number, select: false},
  })

  return Mongoose.model('empresa', empresa);
}
