var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport')
var configMail = require('../config.json').sendMail

var from = configMail.prod.from
var options = configMail.prod.options

if (process.env.NODE_ENV === 'development') {
  from = configMail.dev.from
  options = configMail.dev.options
}
if (process.env.NODE_ENV === 'test') {
  from = configMail.homo.from
  options = configMail.homo.options
}

/*
* Envia email para emails especificados
*
* parametros - para : string | assunto : string | corpo : string | callback : function
*/
module.exports = function (para, assunto, corpo, callback, thisOptions) {
  if (thisOptions) {
    options = Object.assign(options, thisOptions)
  }
  // console.log(options)
  var transporter = nodemailer.createTransport(smtpTransport(options));
  var mailOptions = {
      from: from,
      to: para, // list of receivers
      subject: assunto, // Subject line
      // text: corpo, // plaintext body
      html: corpo // html body
  };

  transporter.sendMail(mailOptions, function(error, info){
    return callback(error, info)
  });
}
