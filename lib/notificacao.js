var Joi = require('joi');
var Boom = require('boom');
var types = require('mongoose').Types;
var ASQ = require('asynquence');
var DB = require('./db');
var Filtro = require('../utils/filtro');


exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/notificacao',
    config: {
      description: 'Busca de Notificações do dashboard',
      validate: {
        query: {
          voluntarioId: Joi.string().required().length(24).description('ID do voluntário'),
          dataUltimoAcesso: Joi.string().description('Data do ultimo acesso do usuário')
        }
      }
    },
    handler: function (request, reply) {
      var dtUltimoAcesso = request.query.dataUltimoAcesso;
      ASQ()
      .then((done) => {
        DB.voluntario.findOne({_id: types.ObjectId(request.query.voluntarioId) })
          .populate({path: 'comite', populate: { path: 'empresa' }})
          .exec((error, volunt) => {
            if(error) { throw error }
            done(volunt)
          })
      })
      .then((done, volunt) => {
        var query = { '$or': [
          { "publicoAlvo.voluntarioId" : request.query.voluntarioId },
          { "tipo": "Mensagem Admin" },
          ]}

        if (volunt.comite.liderSocial && volunt.comite.liderSocial.voluntarioId == volunt._id) {
          query.$or.push({ "tipo": "Mensagem Admin Somente Líderes" })
        }

        DB.notificacao.find(query)
          .sort({ dataCadastro: -1 }).exec(function(err, notificacoes) {
            if (err) throw err

            var count = notificacoes.filter(dataCadastroMaiorAcesso.bind(this, dtUltimoAcesso || volunt.dataUltimoAcesso)).length;
            return reply({ 'notificacao': notificacoes, 'quantidade': count });
          });
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/notificacoes',
    config: {
      description: 'Busca as Notificações ',
      validate: {},
    },
    handler: function (request, reply) {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { dataCadastro: -1 };
      delete request.query.ordenacao;
      const filter = Filtro.formatar(request.query.filtro)
      DB.notificacao.find(filter)
        .sort(ordem).exec(function(err, notificacoes) {
          if (err) throw err
          return reply(notificacoes);
        });
    }
  });

  server.route({
    method: 'POST',
    path: '/notificacao',
    config: {
      description: 'Cadastra uma Notificação',
      validate: {
          payload: {
            publicoAlvo: Joi.any().required(),
            // publicoAlvo: Joi.object().required().keys({
            //   voluntario: Joi.string().required().description('ID do voluntário'),
            //   nome: Joi.string().description('Nome do voluntário'),
            //   urlAvatar: Joi.string().description('Url do Avatar'),
            //   cargo: Joi.string().description('Cargo do voluntário'),
            //   email: Joi.string().description('Email do voluntário')
            // }),
            texto: Joi.string().required().description('Texto da Notificação'),
            tipo: Joi.string().required().description('Tipo da Notificação'),
            urlAcaoClique: Joi.string().description('Url da ação do Click')
          }
      },
    },
    handler: function (request, reply) {
      var now = new Date();
      now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));

      var notific = new DB.notificacao({
        publicoAlvo: request.payload.publicoAlvo,
        texto: request.payload.texto,
        tipo: request.payload.tipo.trim(),
        urlAcaoClique: request.payload.urlAcaoClique,
        dataCadastro: now.toISOString()
      });

      notific.save(function(err, res) {
        if (err) throw err;
        console.log('salvou', res)
        reply({ success: true , notificacao: res });
      });
    }
  });

  return next();

}

exports.register.attributes = {
    name: 'notificacao',
    version: require('../package.json').version
};

function dataCadastroMaiorAcesso(dataUltimoAcesso, notific) {
  var ultimoAcesso = new Date(dataUltimoAcesso);
  var dataCadastro = notific.dataCadastro
  dataCadastro.setTime(dataCadastro - (dataCadastro.getTimezoneOffset() * 60 * 1000 * -1));
  ultimoAcesso.setTime(ultimoAcesso - (ultimoAcesso.getTimezoneOffset() * 60 * 1000 * -1));
  return new Date(dataCadastro) > new Date(ultimoAcesso);
}

function publicoAlvoFilter(id, notificacao) {
  return notificacao.publicoAlvo.filter((part) => {
    if (!part || !part.voluntarioId) { return false }
    return part.voluntarioId.toString() === id
  }).length > 0
}
