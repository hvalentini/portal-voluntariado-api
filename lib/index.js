var Glue = require('glue');
var Hapi = require('hapi');

var internals = {
  manifest: {
    server: {
      connections: {
        routes: {
            cors: true
        }
      }
    },
    connections: [{
      port: 8088,
      labels: ['api']
    }],
    registrations: [
      {
        plugin: 'blipp'
      },
      {
        plugin: {
          register: 'good',
          options: {
            ops: {
              interval: 60000,
            },
            reporters: {
              myConsoleReporter: [{
                module: 'good-console'
              }, 'stdout']
            }
          }
        }
      },
      {
        plugin: 'inert'
      },
      {
        plugin: './empresa'
      },
      {
        plugin: './foto'
      },
      {
        plugin: './acao'
      },
      {
        plugin: './tipoDeAcao'
      },
      {
        plugin: './cidade'
      },
      {
        plugin: './comite'
      },
      {
        plugin: './voluntario'
      },
      {
        plugin: './local'
      },
      {
        plugin: './notificacao'
      },
      {
        plugin: './comiteLogAnual'
      },
      {
        plugin: './parametroSistema'
      },
      {
        plugin: './faixaEtaria'
      },
      {
        plugin: './faixaSalarial'
      },
      {
        plugin: './upload-avatar'
      },
      {
        plugin: './uploads-gallery'
      },
      {
        plugin: './am-uploads'
      },
      {
        plugin: './importacao'
      },
      {
        plugin: './static'
      },
      {
        plugin: './avatar'
      },
      {
        plugin: './utils'
      },
    ]
  }
}

Glue.compose(internals.manifest, { relativeTo: __dirname }, function (err, pack) {
  if (err) {
    console.log('server.register err:', err);
  }
  pack.start(function(){
    console.log('✅  Server is listening on ' + pack.info.uri.toLowerCase());
  });
});
