var mongoose_random = require('mongoose-simple-random');

module.exports = (Mongoose) => {
  var voluntario = new Mongoose.Schema({
    'comite': { type: Mongoose.Schema.ObjectId, ref: 'comite', required: true },
    'cpf': { type: String },
    'nome': { type: String, required: true },
    'nomeNormalizado': { type: String, required: true },
    'cargoNome': { type: String },
    'cargoId': { type: Number },
    'centroResultadoNome': { type: String },
    'centroResultadoId': { type: Number },
    'email': { type: String, unique: true, required: true },
    'emailSuperior': { type: String },
    'telefoneProfissional': { type: String, default: "" },
    'telefonePessoal': { type: String, default: "" },
    'endereco': { type: String },
    'bairro': { type: String },
    'cidade': { type: Mongoose.Schema.ObjectId, ref: 'cidade' },
    'genero': { type: String, default: "" },
    'faixaEtaria': { type: Mongoose.Schema.ObjectId, ref: 'faixaEtaria', default: null },
    'faixaSalarial': { type: Mongoose.Schema.ObjectId, ref: 'faixaSalarial', default: null },
    'escolaridade': { type: String, required: false },
    'tamanhoCamiseta': { type: String, uppercase: true, required: true },
    'associadoExecutivo': { type: Boolean, required: true },
    'aceitouTermo': { type: Boolean, required: true },
    'aceitouTermoEsseAno': { type: Boolean, required: true },
    'usuarioSharePoint': { type: Boolean, required: true },
    'cadastradoPeloLider': { type: Boolean, required: true },
    'urlAvatar': { type: String, default: "" },
    'jaParticipouAcao': { type: Boolean, default: false },
    'jaParticipouAcaoEsseAno': { type: Boolean, default: false },
    'senha': { type: String, default: "" },
    'senhaTemp': { type: String, default: "" },
    'dataCadastro': { type: Date, required: true },
    'dataEdicao': { type: Date, default: null },
    'dataUltimoAcesso': { type: Date, default: null },
    'bloqueado': { type: Boolean, default: false },
    '__v': { type: Number, select: false},
  });

  voluntario.plugin(mongoose_random);

  return Mongoose.model('voluntario', voluntario);
}
