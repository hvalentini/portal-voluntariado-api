var Joi = require('joi');
var Boom = require('boom');
var DB = require('./db');
var Crypto = require('crypto');
var Mongoose = require('mongoose');
var ComiteLogAnual = require('./../utils/comiteLogAnual.js')(Mongoose, DB, Boom);
var AtualizarSaldo = require('./../utils/atualizarSaldo.js')(Mongoose, DB, Boom);
var ASQ = require('asynquence');

exports.register = function (server, options, next) {

  server.route({
    method: 'POST',
    path: '/parametro-sistema/login',
    config: {
      description: 'Login Admin',
      validate: {
        payload: {
          adminLogin: Joi.string().required().description('Login do Admin'),
          adminSenha: Joi.string().required().description('Senha do Admin')
        }
      }
    },
    handler: function (request, reply) {
      const login = request.payload.adminLogin;
      const senha = Crypto.createHash('md5').update(request.payload.adminSenha).digest('hex');

      DB.parametroSistema.findOne({}, (err, param) => {
        if (err) console.error(err);

        if (param.adminLogin !== login || param.adminSenha !== senha)
          return reply(Boom.unauthorized('Usuário ou senha invalido'));

        reply(param);
      });
    }
  });

  server.route({
    method: 'POST',
    path: '/parametro-sistema/novo-ano-vigente',
    config: {
      description: 'Login Admin',
      validate: {
        payload: {
          verbaPadraoAnoVigente: Joi.number().required().greater(0).description('Verba padrão para o novo ano vigente')
        }
      }
    },
    handler: function (request, reply) {
      const verba = request.payload.verbaPadraoAnoVigente;
      var res;
      var count = 0;

      DB.comite.find({}, (err, comites) => {
        ASQ()
        .then(function(done) {
          comites.forEach(function(item) {
            const logAnual = new DB.comiteLogAnual({
              comiteId: item._id,
              verbaInicial: item.verbaInicial ? item.verbaInicial : 0,
              liderSocial: item.liderSocial,
              saldoRemanescente: item.saldoAtual ? item.saldoAtual : 0
            });
            ComiteLogAnual.POST(logAnual, function(err, response) {
              if (err) {
                console.error(err);
                return done.fail(err);
              }
            });
          });
          done();
        })
        .then(function(done) {
          DB.comite.update({}, { $set: { verbaInicial: verba } }, { multi: true }, function(err) {
            if (err) {
              console.error(err);
              done.fail(Boom.badRequest(err));
            } else {
              done();
            }
          });
        })
        .then(function(done) {
          for (var i = 0; i < comites.length; i++) {
            var item = comites[i];

            DB.acao.aggregate([
              { $match: {
                comite: item._id,
              }},
              { $group: {
                _id: "comite",
                total: { $sum: "$valorGasto" },
              }}
            ]).exec((err, objects) => {
              if (err) return done.fail(Boom.badRequest(err))
              if (objects.length === 0) return done.fail(Boom.conflict('Não existem ações para esse comite'));

              AtualizarSaldo.POST(item._id, function(err, saldo) {
                if (err) {
                  console.error(err);
                  return done.fail(err);
                }
              });

              return done({ success: true });
            });
          }
        })
        .then(function(done, response) {
          DB.voluntario.update({ bloqueado: false }, { $set: { aceitouTermoEsseAno: false, jaParticipouEsseAno: false } },
            { multi: true },
            function(err) {
              if (err) return console.error(err);
            }
          );
          reply(response);

          done();
        })
        .or(function(err) {
          return reply(err);
        });
      });
    }
  });

    server.route({
    method: 'POST',
    path: '/parametro-sistema/mudar-senha',
    config: {
      description: 'Serviço que altera a sennha do usuário em parametro do sistema',
      validate: {
        payload: {
          senhaAtual: Joi.string().required().description('Senha atual'),
          senhaNova: Joi.string().required().description('Nova senha'),
          senhaNovaConfirma: Joi.string().required().description('Confirmação de nova senha'),
          email: Joi.string().optional().description('Usuário'),
        }
      }
    },
    handler: (request, reply) => {
      const data = request.payload

      if (data.senhaNova !== data.senhaNovaConfirma) {
        return reply({ ok: false, message: "A nova senha deve ser a igual a confirmação de senha" })
      }

      if (data.senhaNova === data.senhaAtual) {
        return reply({ ok: false, message: "A nova senha deve ser diferente da atual." })
      }

      const query = {}
      if (data.email) {
        query.adminLogin = data.email
      }

      DB.parametroSistema.findOne(query, (err, doc) => {
        if (err) { throw err }

        if (doc.adminSenha === data.senhaNova) {
          return reply({ ok: false, message: "Está senha senha já esta cadastrada, por favor digite uma senha diferente." })
        }

        if (Crypto.createHash('md5').update(data.senhaAtual).digest('hex') !== doc.adminSenha) {
          return reply({ ok: false, message: "Senha atual está incorreta" })
        }

        const user = {
          $set: {
            adminSenha: Crypto.createHash('md5').update(data.senhaNova).digest('hex'),
          }
        }
        DB.parametroSistema.update({_id: objectId(doc._id)}, user, (error, resp) => {
          if (error) { throw error }

          return reply(resp)
        })
      });
    }
  });

  function objectId(id) {
    return Mongoose.Types.ObjectId(id);
  }

  return next();

}

exports.register.attributes = {
    name: 'parametroSistema',
    version: require('../package.json').version
};
