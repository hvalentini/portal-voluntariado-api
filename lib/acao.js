var DB        = require('./db');
var Joi       = require('joi');
var Boom      = require('boom');
var Mongoose  = require('mongoose');
var types = require('mongoose').Types;
var ASQ       = require('asynquence');
const Filtro  = require('../utils/filtro');
var noAccents = require('remove-accents');
const AtualizarSaldo = require('./../utils/atualizarSaldo.js')(Mongoose, DB, Boom);
const Resumido = require('../models/schemas/voluntarioResumido')(Mongoose)
const reject = require('lodash/reject')
const find = require('lodash/find')

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/acao/listar',
    config: {
      description: 'Busca de acoes',
      validate: {
        query: {
          filtro: Joi.string().optional().allow('').description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta'),
          page: Joi.number().optional().description('Pagina'),
          limit: Joi.number().optional().description('Limit de resultados por pagina'),
          exclusivoParaLider: Joi.boolean().optional().description('Exclusivo para lider'),
        }
      },
    },
    handler: function (request, reply) {

      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "nome": 1};
      const filtro = Filtro.formatar(request.query.filtro, ['local', 'tipoAcao', 'participantes.voluntarioId', 'criadoPor.voluntarioId', 'comite', 'comite.empresa', 'comite.cidade', 'participantes.voluntarioID']);
      const filtroComite = obtemFiltroSubDoc(filtro, 'comite.cidade');
      Object.assign(filtroComite, obtemFiltroSubDoc(filtro, 'comite.empresa'))
      Object.assign(filtroComite, obtemFiltroSubDoc(filtro, 'comite.cidadeUF'))
      Object.assign(filtroComite, obtemFiltroSubDoc(filtro, 'comite.verbaExtra'))

      const participanteId = filtro['participantes.voluntarioId'] ? filtro['participantes.voluntarioId'] : null;
      delete filtro['comite.cidade']
      delete filtro['comite.empresa']
      delete filtro['comite.cidadeUF']
      delete filtro['comite.verbaExtra']
      delete filtro['participantes.voluntarioId']

      const page = request.query.page ? request.query.page : ''
      const limit = request.query.limit ? request.query.limit : ''
      var skip = page && limit ? page*limit : 0

      if (filtro.desativada && filtro.desativada === true ){
        filtro.desativada = true
      } else {
        filtro.desativada = false
      }

      DB.acao.find(filtro)
          .sort(ordem)
          .skip(skip)
          .limit(limit)
          .populate({ path: 'comite', match: filtroComite, populate: { path: 'empresa' } })
          .populate('tipoAcao').populate('local')
          .exec((err, objects) => {

        if (err) return console.error(err);


        //SOMENTE PARA QUANDO VOLUNTARIO NAO FOR LIDER DO COMITE
        var objFiltered = objects
        if(!request.query.exclusivoParaLider) {
          objFiltered = objects.filter((element) => {
            return element.tipoAcao.exclusivoParaLider !== true
          });
        }

        if (participanteId)
          objFiltered = objects.filter(participanteFilter.bind(this, participanteId));

        return reply(objFiltered.filter(comiteFilter));
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/acao',
    config: {
      description: 'Busca de acoes',
      validate: {
        query: {
          filtro: Joi.string().optional().allow('').description('Filtros para a Consulta'),
          ordenacao: Joi.string().description('Odernação para a Consulta'),
          page: Joi.number().optional().description('Pagina'),
          limit: Joi.number().optional().description('Limit de resultados por pagina'),
          exclusivoParaLider: Joi.boolean().optional().description('Exclusivo para lider'),
        }
      },
    },
    handler: (request, reply) => {
      const ordem = request.query.ordenacao ? JSON.parse(request.query.ordenacao) : { "nome": 1};
      oldIds = ['local', 'tipoAcao', 'comite', 'comite.empresa', 'comite.cidade']
      newIds = ['local._id', 'tipoAcao._id', 'comite._id', 'comite.empresa._id', 'comite.cidade._id', '_id', 'criadoPor._id', 'participantes._id']
      ids = oldIds.concat(newIds)
      const filtro = Filtro.formatar(request.query.filtro, ids);
      const newFilter = Filtro.parseIds(filtro, oldIds, newIds)
      const newOrder = Filtro.parseIds(ordem, oldIds, newIds)

      if (request.query.exclusivoParaLider === false) {
        newFilter['tipoAcao.exclusivoParaLider'] = JSON.parse(request.query.exclusivoParaLider)
      }

      if (!newFilter.desativada) {
        newFilter.desativada = false
      }

      const page = request.query.page ? request.query.page : ''
      const limit = request.query.limit ? request.query.limit : ''
      const skip = page && limit ? page * limit : 0

      const query = [
        { $lookup: {
          from: "comites",
          localField: "comite",
          foreignField: "_id",
          as: "comites"
        }},
        { $unwind: "$comites" },
        { $lookup: {
          from: "empresas",
          localField: "comites.empresa",
          foreignField: "_id",
          as: "empresas"
        }},
        { $unwind: "$empresas" },
        { $lookup: {
          from: "cidades",
          localField: "comites.cidade",
          foreignField: "_id",
          as: "cidades"
        }},
        { $unwind: "$cidades" },
        { $lookup: {
          from: "tipoDeAcoes",
          localField: "tipoAcao",
          foreignField: "_id",
          as: "tipoDeAcoes"
        }},
        { $unwind: "$tipoDeAcoes" },
        { $lookup: {
          from: "locais",
          localField: "local",
          foreignField: "_id",
          as: "locais"
        }},
        { $unwind: {
            path: "$locais",
            preserveNullAndEmptyArrays: true
          }
        },
        { $project: {
          _id: 1,
          nome: 1,
          comite: {
            _id: '$comites._id',
            empresa: {
              _id: '$empresas._id',
              nome: '$empresas.nome',
              nomeNormalizado: '$empresas.nomeNormalizado'
            },
            cidade: {
              _id: '$cidades._id',
              nome: '$cidades.nome',
              uf: '$cidades.uf',
              nomeNormalizado: '$cidades.nomeNormalizado'
            },
            cidadeNome: '$comites.cidadeNome',
            cidadeUF: '$comites.cidadeUF',
            unidade: '$comites.unidade',
            unidadeNormalizada: '$comites.unidadeNormalizada',
            verbaInicial: '$comites.verbaInicial',
            saldoAtual: '$comites.saldoAtual',
            verbaExtra: '$comites.verbaExtra',
            liderSocial: '$comites.liderSocial'
          },
          nomeNormalizado: 1,
          cadastroRetroativo: 1,
          acaoPontual: 1,
          dataInicial: 1,
          horaInicial: 1,
          dataFinal: 1,
          horaFinal: 1,
          tempoDuracao: 1,
          descricao: 1,
          totalBeneficiados: 1,
          totalParticipantes: 1,
          totalQueGostaram: 1,
          totalCartas: 1,
          valorGasto: 1,
          dataCadastro: 1,
          dataEdicao: 1,
          desativada: 1,
          local: '$locais',
          tipoAcao:  '$tipoDeAcoes',
          criadaPor: 1,
          participantes: 1,
          editadoPor: 1,
          queremParticipar: 1,
          gostaram: 1,
          galeria: 1,
          nomeTipoAcao: 1,
        }},
        { $match: newFilter },
        { $sort: newOrder },
      ]
      if(request.query.skip) {
        query.push({ $skip: Number(request.query.skip) })
      }
      if (!request.query.skip && request.query.page && request.query.limit) {
        query.push({ $skip: (request.query.page * request.query.limit) })
      }
      if(request.query.limit) {
        query.push({ $limit: Number(request.query.limit) })
      }

      DB.acao.aggregate(query, (err, docs) => {
        if(err) throw err;

        return reply(docs);
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/horas',
    config: {
      description: 'Carga horária voluntário',
      validate: {
          query: {
              voluntarioId: Joi.string().required().description('ID do voluntário é requerido'),
              dataInicialFiltro: Joi.date().required().description('Data incial'),
              dataFinalFiltro: Joi.date().required().description('Data final'),
          }
      },
    },
    handler: function (request, reply) {
      const acoes = DB.acao;
      const voluntarioId = request.query.voluntarioId;
      const dataInicialFiltro = request.query.dataInicialFiltro;
      const dataFinalFiltro = request.query.dataFinalFiltro;

      acoes.aggregate([
        {
          $match: {
          "participantes.voluntarioId" : voluntarioId,
          "dataInicial" : { "$gte": dataInicialFiltro },
          "dataFinal" : { "$lte": dataFinalFiltro  },
          }
        },
        {  $group: {
            _id: null,
            soma: { $sum: "$tempoDuracao" }
          }
        }
      ],
      (err,docs) => {
        if (err) throw err;

        if(!docs || docs.length < 1) {
          return reply({ hoursAvailable: false })
        }
        return reply (docs);
      })
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/{id}',
    config: {
      description: 'Detalhar ação',
    },

    handler: function (request, reply) {
      var query = {_id: Mongoose.Types.ObjectId(request.params.id) }

      DB.acao.find(query).populate('comite').populate('tipoAcao').populate('local').exec((err, acoes) => {
        if (err) return console.error(err);
        if(!acoes.length) {
          return reply(Boom.badRequest(`Id ${request.params.id} não existe.`));
        }
        return reply(acoes);
      });

    }
  });

  server.route({
    method: 'DELETE',
    path: '/acao',
    config: {
      description: 'Deleta uma ação',
      validate: {
          query: {
            id: Joi.string().required().length(24).description('ID da ação')
          }
      },
    },
    handler: function (request, reply) {
      var query = { _id: request.query.id }
      // var query = { _id: types.ObjectId(request.query.id) }
      DB.acao.findOne(query, function(err, acoes) {
        if (err) return console.error(err);
        if(!acoes) {
          return reply(Boom.badRequest(`Ação com id ${request.query.id} não existe.`));
        }
        acoes.desativada = true
        acoes.markModified('desativada');
        // acoes.save((err, doc) => {
        //   if (err) return console.error(err);
        //   reply({ success: true });
        // });
        const id = { _id : Mongoose.Types.ObjectId(request.query.id) }
        DB.acao.update(id, acoes, (err, res) => {
          if (err) return console.error(err);
          reply({ success: true });
        })


      });
    }
  });

  server.route({
    method: 'POST',
    path: '/acao',
    config: {
      description: 'Cadastra uma ação',
      validate: {
          payload: {
            nome: Joi.string().required().description('Nome da ação é obrigatório'),
            tipoAcao: Joi.string().required().description('Tipo da ação é obrigatório'),
            cadastroRetroativo: Joi.boolean().required().description('Cadastro retroativo é obrigatório'),
            acaoPontual: Joi.boolean().required().description('Ação pontual é obrigatório'),
            comite: Joi.string().required().description('Comite é obrigatório'),
            criadaPor: Joi.object().required().description('Comite é obrigatório'),
            descricao: Joi.string().required().description('Comite é obrigatório'),
            dataInicial: Joi.date().optional().allow("").description('Data inicial'),
            horaInicial: Joi.number().optional().allow(0).description('Hora inicial'),
            dataFinal: Joi.date().optional().allow("").description('Data final'),
            horaFinal: Joi.number().optional().allow(0).description('Hora final'),
            totalBeneficiados: Joi.number().optional().allow(0).description('Total Beneficiados'),
            totalParticipantes: Joi.number().optional().allow(0).description('Total de participantes'),
            totalCartas: Joi.number().optional().allow(0).description('Total Cartas'),
            valorGasto: Joi.number().optional().allow(0).description('Valor Gasto'),
            participantes: Joi.array().optional().allow([]).description('Participantes'),
            galeria: Joi.array().optional().allow([]).description('Galeria'),
            gostaram: Joi.array().optional().allow([]).description('Gostaram'),
            queremParticipar:Joi.array().optional().allow([]).description('Querem Participar'),
            local:Joi.string().optional().allow('').description('local'),
            // local: Joi.string()
            //           .when('cadastroRetroativo', { is: true, then: Joi.required() }),
            dataInicial: Joi.string()
                      .when('cadastroRetroativo', { is: true, then: Joi.required() }),
            dataFinal: Joi.string()
                      .when('cadastroRetroativo', { is: true, then: Joi.required() }),
            tempoDuracao: Joi.number()
                      .when('cadastroRetroativo', { is: true, then: Joi.required() }),
            valorGasto: Joi.number()
                      .when('cadastroRetroativo', { is: true, then: Joi.required() }),
          }
      },
    },

    handler: function (request, reply) {

      var tipoAcao = null
      const dataIni = new Date(request.payload.dataInicial)
      const dataFinal = new Date(request.payload.dataFinal)

       ASQ()
      .then((done) => {
        // Não pode existir dois registros com o mesmo nome, na mesma data, no mesmo local daquele comite
        DB.acao.find({
          nomeNormalizado: { $regex : new RegExp( noAccents(request.payload.nome).toLowerCase(), "i") },
          dataInicial: {
            $gte: new Date(dataIni.setHours(0, 0, 0, 0) - (dataIni.getTimezoneOffset() * 60 * 1000)).toISOString(),
            $lte: new Date(dataIni.setHours(23, 59, 59, 999) - (dataIni.getTimezoneOffset() * 60 * 1000)).toISOString()
          },
          dataFinal: {
            $gte: new Date(dataFinal.setHours(0, 0, 0, 0) - (dataFinal.getTimezoneOffset() * 60 * 1000)).toISOString(),
            $lte: new Date(dataFinal.setHours(23, 59, 59, 999) - (dataFinal.getTimezoneOffset() * 60 * 1000)).toISOString()
          },
          local: request.payload.local,
          comite: request.payload.comite,
        }, (err, acoes) => {
            if (acoes.length > 0) {
              return reply(Boom.conflict(`A ação ${request.payload.nome} já está cadastrada nesta mesma data, local e comite.`));
            }
            done();
          }
        )
      })
      .then((done) => {
        //Buscar tipo de açao.
        DB.tipoDeAcao.findOne({ _id: Mongoose.Types.ObjectId(request.payload.tipoAcao) }, function(err, tipos) {
          if (err || !tipos) {
            return reply(Boom.badRequest(`Id do tipo de Ação nao existe.`));
          }
          done(tipos)
        })
      })
      .then((done, tipos) => {
        if(!request.payload.acaoPontual && !tipos.exclusivoParaLider && request.payload.cadastroRetroativo && (!request.payload.participantes || request.payload.participantes.length==0)){
          return reply(Boom.conflict(`Obrigatório informar os participantes`));
        }

        if(!tipos.exclusivoParaLider && request.payload.cadastroRetroativo && !request.payload.totalBeneficiados) {
          return reply(Boom.conflict(`Obrigatório informar o total de Beneficiados.`));
        }
        if(tipos.informarQtdeCartas && request.payload.cadastroRetroativo && !request.payload.totalCartas) {
          return reply(Boom.conflict(`Obrigatório informar o total de Cartas.`));
        }

        const payload = request.payload
        payload.comite = Mongoose.Types.ObjectId(request.payload.comite)
        payload.tipoAcao = Mongoose.Types.ObjectId(request.payload.tipoAcao)
        payload.nomeTipoAcao = tipos.nome
        payload.local =  request.payload.local
                          ? Mongoose.Types.ObjectId(request.payload.local)
                          : null
        payload.dataCadastro = obtemDataAtual()
        payload.desativada = false
        payload.nomeNormalizado = noAccents(request.payload.nome).toLowerCase()
        payload.totalParticipantes = payload.participantes ? payload.participantes.length : 0
        payload.dataInicial = convertDate(payload.dataInicial)
        payload.dataFinal = convertDate(payload.dataFinal)

        if(payload.queremParticipar) {
          payload.queremParticipar = []
        }

        if(payload.gostaram) {
          payload.gostaram = []
        }

        const acao = new DB.acao(payload)
        acao.save(function(err, res) {
          if (err) return console.error(err);
          DB.acao.find({ _id: res._id }).populate('tipoAcao').exec((error, acoes) => {
            done(acoes);
          })
        })
      })
      .then((done, acoes) => {
        const res = acoes[0]
        if (request.payload.cadastroRetroativo) {
          var publicoAlvo = res.participantes ? res.participantes : [];
          var notific = new DB.notificacao({
            publicoAlvo: publicoAlvo,
            tipo: 'Você participou de uma ação',
            texto: `Você participou da ação ${res.nome}`,
            dataCadastro: obtemDataAtual()
          });
          notific.save(function(errorNotif) { if (errorNotif) console.error(errorNotif) });
          done(res)
        } else if (!res.tipoAcao.exclusivoParaLider) {
          DB.voluntario.find({ comite: res.comite}, (err, voluntarios) => {
            const volunts = voluntarios
            volunts.voluntariodId = volunts._id
            delete volunts._id
            if(voluntarios.length) {
            const publico = voluntarios.length ?
                            voluntarios.map((item, i) => {
                              return { voluntarioId: item._id }
                            })
                            : null
                          }

            var notific = new DB.notificacao({
              publicoAlvo: publico,
              tipo: 'Nova ação cadastrada pelo seu comitê',
              texto: `Seu comitê cadastrou uma nova ação ${res.nome}`,
              dataCadastro: obtemDataAtual()
            });
            notific.save(function(errorNotif) { if (errorNotif) console.error(errorNotif); });
            done(res);
          })
        } else {
          done(res);
        }

      })
      .then((done, res) => {
        if(request.payload.valorGasto > 0) {
          //atualizar saldo do comiteId
          AtualizarSaldo.POST(request.payload.comite, function(err) {
            if (err) console.error(err);
            done(res);
          });
        }else{
          done(res);
        }
      })
      .then((_, res) => {
        if(!res.cadastroRetroativo || !res.participantes.length) { return reply({ success: true, data: res }) }
          var control = [];
          res.participantes.forEach((part) => {
            var payload = {
              $set: {
                jaParticipouAcaoEsseAno: res.dataInicial.getFullYear() === new Date().getFullYear() ? true : undefined,
                jaParticipouAcao: true,
              }
            }
            DB.voluntario.update({ _id: Mongoose.Types.ObjectId(part.voluntarioId) }, payload, (err, resp) => {
              if(err) { throw err }
              control.push(resp)
              if( control.length === res.participantes.length ) {
                return reply({ success: true, data: res })
              }
            })
          });

      });
    }
  });

  server.route({
    method: 'PUT',
    path: '/acao',
    config: {
      description: 'Altera uma ação',
      validate: {
          payload: {
            _id: Joi.string().required().description('Id da ação é obrigatório'),
            nome: Joi.string().required().description('Nome da ação é obrigatório'),
            tipoAcao: Joi.string().required().description('Tipo da ação é obrigatório'),
            cadastroRetroativo: Joi.boolean().required().description('Cadastro retroativo é obrigatório'),
            acaoPontual: Joi.boolean().required().description('Ação pontual é obrigatório'),
            comite: Joi.string().required().description('Comite é obrigatório'),
            editadoPor: Joi.object().required().description('Nome do usuário que está editando é obrigatório.'),
            descricao: Joi.string().required().description('Comite é obrigatório'),
            dataInicial: Joi.date().optional().allow("").description('Data inicial'),
            horaInicial: Joi.number().optional().allow(0).description('Hora inicial'),
            dataFinal: Joi.date().optional().allow("").description('Data final'),
            horaFinal: Joi.number().optional().allow(0).description('Hora final'),
            totalBeneficiados: Joi.number().optional().allow(0).description('Total Beneficiados'),
            totalCartas: Joi.number().optional().allow(0).description('Total Cartas'),
            valorGasto: Joi.number().optional().allow(0).description('Valor Gasto'),
            participantes: Joi.array().optional().allow([]).description('Participantes'),
            gostaram: Joi.array().optional().allow([]).description('Gostaram'),
            queremParticipar: Joi.array().optional().allow([]).description('Querem Participar'),
            galeria: Joi.array().optional().allow([]).description('Galeria'),
            local: Joi.string().optional().allow(""),
            dataInicial: Joi.string().optional().allow(""),
            dataFinal: Joi.string().optional().allow(""),
            tempoDuracao: Joi.number().optional().allow(""),
            valorGasto: Joi.number().optional().allow(""),
          }
      },
    },
    handler: function (request, reply) {

      var query = { _id: Mongoose.Types.ObjectId(request.payload._id) }
      var req = request.payload

      ASQ()
      .then(function(done) {
        DB.acao.findOne(query , function(err, acoes) {
          if (acoes === null) {
            return reply(Boom.conflict('Não existe ação para esse ID')) ;
          }
          if (err) return console.log(err);
          done(acoes);
        })
      })
      .then(function(done, acoes) {
        //Caso usuario tenha mandado um novo tipo de ação.
        DB.tipoDeAcao.findOne({ _id: Mongoose.Types.ObjectId(request.payload.tipoAcao) }, function(err, tipos) {
          if (err || !tipos) {
            return reply(Boom.badRequest(`Id do tipo de Ação nao existe.`));
          }
          done(acoes, tipos);
        })

      })
      .then(function(done, acoes, tipos) {
        //validação tipo de ação e ação.

        if(tipos) {
          if(!tipos.exclusivoParaLider && request.payload.cadastroRetroativo && request.payload.totalBeneficiados==0) {
            return reply(Boom.conflict(`Obrigatório informar o total de Beneficiados.`));
          }
          if(tipos.informarQtdeCartas && request.payload.cadastroRetroativo && request.payload.totalCartas==0) {
            return reply(Boom.conflict(`Obrigatório informar o total de Cartas.`));
          }
          req.nomeTipoAcao = tipos.nome
        }
        done(acoes, tipos);
      })
      .then(function(done, acoes, tipos) {

        if(!req.acaoPontual && !tipos.exclusivoParaLider && req.cadastroRetroativo && !req.participantes.length && !acoes.participantes.length){
          return reply(Boom.conflict(`Obrigatório informar os participantes`));
        }

        const dataIni = new Date(req.dataInicial)
        const dataFinal = new Date(req.dataFinal)
        // Não pode existir dois registros com o mesmo nome, na mesma data, no mesmo local daquele comite
        const query = {
          nomeNormalizado: { $regex : new RegExp( noAccents(request.payload.nome).toLowerCase(), "i") },
          dataInicial: {
            $gte: new Date(dataIni.setHours(0, 0, 0, 0) - (dataIni.getTimezoneOffset() * 60 * 1000)).toISOString(),
            $lte: new Date(dataIni.setHours(23, 59, 59, 999) - (dataIni.getTimezoneOffset() * 60 * 1000)).toISOString()
          },
          dataFinal: {
            $gte: new Date(dataFinal.setHours(0, 0, 0, 0) - (dataFinal.getTimezoneOffset() * 60 * 1000)).toISOString(),
            $lte: new Date(dataFinal.setHours(23, 59, 59, 999) - (dataFinal.getTimezoneOffset() * 60 * 1000)).toISOString()
          },
          comite: req.comite,
          _id: { $ne: Mongoose.Types.ObjectId(request.payload._id) }
        }

        if (req.local) {
          query.local = req.local
        }

        DB.acao.find(query, (err, acoesRes) => {
          if (acoesRes && acoesRes.length) {
            return reply(Boom.conflict(`A ação "${req.nome}" já está cadastrada nesta mesma data, local e comite.`));
          }
          done(acoes);
        })
      })
      .then(function(done, acoes){

        const id = { _id : Mongoose.Types.ObjectId(req._id) }
        const comite = req.comite ? Mongoose.Types.ObjectId(req.comite) : acoes.comite
        const tipoAcao = req.tipoAcao ? Mongoose.Types.ObjectId(req.tipoAcao) : acoes.tipoAcao
        // const local =  req.local && req.local !== '' ? Mongoose.Types.ObjectId(req.local): null

        if(req.participantes.length) {
          req.totalParticipantes = req.participantes.length
        } else {
          req.participantes = []
          req.totalParticipantes = 0
        }

        if(!req.galeria.length) {
          req.galeria = []
        }

        req.nomeNormalizado = noAccents(req.nome).toLowerCase()
        req.gostaram = acoes.gostaram
        req.dataEdicao = obtemDataAtual()
        req.dataInicial = convertDate(req.dataInicial)
        req.dataFinal = convertDate(req.dataFinal)
        req.totalQueGostaram = acoes.totalQueGostaram
        req.local = req.local && req.local !== '' ? Mongoose.Types.ObjectId(req.local): null

        const acao = new DB.acao(req)

        DB.acao.update(id, acao, (err, res) => {
          if(err) return console.log(err);

          normalizaPartipantes(acoes.participantes, req.participantes, req, () => {
            console.log('callback')
            return done(res);
          })
        })
      })
      .then(function(done, res) {
        //atualizar saldo do comiteId
        AtualizarSaldo.POST(req.comite, function(err) {
          if (err) console.error(err);
         return reply({ succes: true, data: res })
        });
      })
    }
  });

  server.route({
    method: 'PUT',
    path: '/acao/participar',
    config: {
      description: 'Quer Participar de uma Ação',
      validate: {
        payload: {
          idAcao: Joi.string().required().length(24).description('ID da Ação'),
          idVoluntario: Joi.string().required().length(24).description('ID do Voluntário')
        }
      }
    },
    handler: function (request, reply) {
      const idAcao = { _id : Mongoose.Types.ObjectId(request.payload.idAcao) };
      const idVoluntario = { _id : Mongoose.Types.ObjectId(request.payload.idVoluntario) };

      var filtro1 = { "_id" : Mongoose.Types.ObjectId(request.payload.idAcao),"queremParticipar.voluntarioId": request.payload.idVoluntario};

      ASQ()
      .then(function(done) {
        //VERIFICAR SE O USUARIO JÁ ESTA PARTICIPANDO, SE SIM EXCLUIR O MESMO
        DB.acao.find(filtro1).exec((err, objects) => {
            if (err) return console.log(err);
            if(objects.length) {
              //deletar
              var queremParticipar = objects[0].queremParticipar
              objects[0].queremParticipar.forEach((item, i) => {
                if (item.voluntarioId == request.payload.idVoluntario) {
                  queremParticipar.splice(i, 1);
                }
              });
              DB.acao.update(idAcao, { queremParticipar: queremParticipar }, (error, res) => {
                if(error) return console.log(error);
                if (res.ok) {
                  done('ok')
                  return reply({ success: true })
                }
              });
            } else {
              done('')
            }
        });
      })
      .then(function(done, ok) {
        if(!ok) {
          DB.acao.aggregate([
            { $match: idAcao },
            { $lookup: {
              from: "comites",
              localField: "comite",
              foreignField: "_id",
              as: "comite"
            }},
            {
              $project: {
                comite: '$comite',
                queremParticipar: '$queremParticipar',
                nome: 1,
              }
            }], function (errorA, acoes) {
              console.log(acoes)
                  if(errorA) console.console.error(errorA);
                  if(!acoes || acoes.length === 0)
                    return reply(Boom.badRequest(`Não existe Ação com o ID [${request.payload.idAcao}]`));

                  DB.voluntario.findOne(idVoluntario, (errorV, voluntarios) => {
                    if(errorV) console.console.error(errorV);
                    if(!voluntarios)
                      return reply(Boom.badRequest(`Não existe Voluntário com o ID [${request.payload.idVoluntario}]`));

                    var volResumido = {
                      voluntarioId: voluntarios._id,
                      nome: voluntarios.nome,
                      urlAvatar: voluntarios.urlAvatar,
                      cargo: voluntarios.cargo,
                      email: voluntarios.email
                    };

                    DB.acao.update(idAcao, { $push: { queremParticipar: volResumido } }, (err, res) => {
                      if(err) return console.log(err);
                      if (res.ok) {

                        var liderResumido = { voluntarioId: acoes[0].comite[0].liderSocial.voluntarioId };

                        var notific = new DB.notificacao({
                          publicoAlvo: liderResumido,
                          tipo: 'Voluntário quer participar da ação',
                          texto: `O voluntário ${voluntarios.nome} deseja participar da ação ${acoes[0].nome}`,
                          dataCadastro: obtemDataAtual()
                        });

                        notific.save(function(errorNotif) { if (errorNotif) console.error(errorNotif); });

                        return reply({ success: true })
                      }
                    });

                  });
                })
          }
        })
      }
    // }
  });



//EDITAR
  server.route({
    method: 'PUT',
    path: '/acao/participou',
    config: {
      description: 'Participou de uma Ação',
      validate: {
        payload: {
          idAcao: Joi.string().required().length(24).description('ID da Ação'),
          idVoluntario: Joi.string().required().length(24).description('ID do Voluntário')
        }
      }
    },
    handler: function (request, reply) {
      const idAcao = { _id : Mongoose.Types.ObjectId(request.payload.idAcao) };
      const idVoluntario = { _id : Mongoose.Types.ObjectId(request.payload.idVoluntario) };

      var filtro1 = { "_id" : Mongoose.Types.ObjectId(request.payload.idAcao),"participantes.voluntarioId": request.payload.idVoluntario};

      ASQ()
      .then(function(done) {
        //VERIFICAR SE O USUARIO JÁ ESTA PARTICIPANDO, SE SIM EXCLUIR O MESMO
        DB.acao.find(filtro1).exec((err, objects) => {
            if (err) return console.log(err);
            if(objects.length) {
              //deletar
              var participantes = objects[0].participantes
              var total = objects[0].totalParticipantes
              objects[0].participantes.forEach((item, i) => {
                if (item.voluntarioId == request.payload.idVoluntario) {
                  participantes.splice(i, 1);
                  total = total - 1
                }
              });
              DB.acao.update(idAcao, { participantes: participantes, totalParticipantes: total }, (error, res) => {
                if(error) return console.log(error);
                if (res.ok) {
                  done('ok')
                  return reply({ success: true })
                }
              });
            } else {
              done('')
            }
        });
      })
      .then(function(done, ok) {
        if(!ok) {
          DB.acao.aggregate([
            { $match: idAcao },
            { $lookup: {
              from: "comites",
              localField: "comite",
              foreignField: "_id",
              as: "comite"
            }},
            {
              $project: {
                comite: '$comite',
                participantes: '$participantes',
                nome: 1,
              }
            }], function (errorA, acoes) {
                  if(errorA) console.console.error(errorA);
                  if(!acoes || acoes.length === 0)
                    return reply(Boom.badRequest(`Não existe Ação com o ID [${request.payload.idAcao}]`));

                  DB.voluntario.findOne(idVoluntario, (errorV, voluntarios) => {
                    if(errorV) console.console.error(errorV);
                    if(!voluntarios)
                      return reply(Boom.badRequest(`Não existe Voluntário com o ID [${request.payload.idVoluntario}]`));

                    var volResumido = {
                      voluntarioId: voluntarios._id,
                      nome: voluntarios.nome,
                      urlAvatar: voluntarios.urlAvatar,
                      cargo: voluntarios.cargo,
                      email: voluntarios.email
                    };
                    var total = acoes[0].participantes.length

                    DB.acao.update(idAcao, { $push: { participantes: volResumido }, totalParticipantes: total + 1 }, (err, res) => {
                      if(err) return console.log(err);
                      if (res.ok) {

                        var liderResumido = { voluntarioId: acoes[0].comite[0].liderSocial.voluntarioId };

                        var notific = new DB.notificacao({
                          publicoAlvo: liderResumido,
                          tipo: 'Voluntário participou da ação',
                          texto: `O voluntário ${voluntarios.nome} participou da ação ${acoes[0].nome}`,
                          dataCadastro: obtemDataAtual()
                        });

                        notific.save(function(errorNotif) { if (errorNotif) console.error(errorNotif); });

                        return reply({ success: true });
                      }
                    });

                  });
                })
          }
        })
      }
    // }
  });


  server.route({
    method: 'PUT',
    path: '/acao/gostar',
    config: {
      description: 'Gostar de uma Ação',
      validate: {
        payload: {
          idAcao: Joi.string().required().length(24).description('ID da Ação'),
          idVoluntario: Joi.string().required().length(24).description('ID do Voluntário'),
          type: Joi.boolean().required().description('Tipo like ou dislike'),
        }
      }
    },
    handler: function (request, reply) {
      const idAcao = { _id : Mongoose.Types.ObjectId(request.payload.idAcao) };
      const idVoluntario = { _id : Mongoose.Types.ObjectId(request.payload.idVoluntario) };

      DB.voluntario.findOne(idVoluntario, (errorV, voluntarios) => {
        if(errorV) console.console.error(errorV);
        if(!voluntarios)
          return reply(Boom.badRequest(`Não existe Voluntário com o ID [${request.payload.idVoluntario}]`));

          var volResumido = {
            voluntarioId: voluntarios._id.toString(),
            nome: voluntarios.nome,
            urlAvatar: voluntarios.urlAvatar,
            cargo: voluntarios.cargoNome,
            email: voluntarios.email
          };

        DB.acao.aggregate([
          { $match: idAcao },
          { $lookup: {
            from: "comites",
            localField: "comite",
            foreignField: "_id",
            as: "comite"
          }},
          { $unwind: "$comite" },
          {
            $project: {
              comite: '$comite',
              gostaram: '$gostaram',
              nome: '$nome'
            }
          }], function (errorA, acoes) {
          if(errorA) console.console.error(errorA);
          if(!acoes || acoes.length === 0)
            return reply(Boom.badRequest(`Não existe Ação com o ID [${request.payload.idAcao}]`));

          const obj = find(acoes[0].gostaram, {"voluntarioId": request.payload.idVoluntario })
          // console.log(obj);

          if (obj && !request.payload.type) {
            return DB.acao.update(idAcao, { $pull: { gostaram: obj }, $inc: { totalQueGostaram: -1 } } , (err, res) => {
              if(err) return console.log(err);
              return reply({ dislike: true });
            });
          }

          if (!obj && request.payload.type) {
            var liderResumido = []

            liderResumido.push({ voluntarioId: acoes[0].comite.liderSocial.voluntarioId });

            var notific = new DB.notificacao({
              publicoAlvo: liderResumido,
              tipo: 'Voluntário gostou da ação',
              texto: `O voluntário ${voluntarios.nome} gostou da ação ${acoes[0].nome}`,
              dataCadastro: obtemDataAtual()
            });

            return ASQ(notific)
            .all(
              (done) => {
                DB.acao.update(idAcao, { $push: { gostaram: volResumido }, $inc: { totalQueGostaram: 1 } }, (err, res) => {
                  if(err) return console.log(err);
                  done()
                });
              },
              (done, notific) => {
                notific.save(function(errorNotif) {
                  if (errorNotif) console.error(errorNotif);
                  done();
                });
              }
            )
            .val(() => {
              reply({ like: true });
            })
          }

          return reply({ like: false, dislike: false });

        })
      })
    }
  });


  server.route({
    method: 'GET',
    path: '/acao/realizadas-ano',
    config: {
      description: 'Lista a quantidade de ações realizadas por ano',
      validate: {
        query: {
          ano: Joi.number().optional().description('Ano de consulta'),

        }
      },
    },
    handler: function (request, reply) {
      const ano = request.query.ano
      const query = ano ? { dataFinal: { $gte: new Date(ano, 0, 1 ), $lte: new Date(ano, 11, 31) } } : {};
      query['desativada'] = false;

      DB.acao.find(query, (err, acoes) => {
        if (err) return console.error(err);
        reply({ quantidade: acoes.length });
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/realizadas-comite',
    config: {
      description: 'Lista a quantidade de ações realizadas por um comite',
      validate: {
        query: {
          // ano: Joi.number().optional().description('Ano de consulta'),
          idComite: Joi.string().optional().description('Id Comite'),
        }
      },
    },
    handler: function (request, reply) {
      const idComite = Mongoose.Types.ObjectId(request.query.idComite)
      const query = { comite: idComite, cadastroRetroativo: true }
      query['desativada'] = false;

      DB.acao.find(query, (err, acoes) => {
        if (err) return console.error(err);
        reply({ quantidade: acoes.length });
      });
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/pessoas-beneficiadas',
    config: {
      description: 'Lista o total de pessoas benificadas por uma ação ou voluntario',
      validate: {
        query: {
          ano: Joi.number().optional().description('Ano'),
          idVoluntario: Joi.string().optional().description('Id do comite'),
          idComite: Joi.string().optional().description('Id do voluntario')
        }
      }
    },
    handler: (request, reply) => {
      const query = { desativada: false };

      if (request.query.ano) {
        const ano = request.query.ano
        query.dataFinal = { "$gte": new Date(ano, 0, 1 ), "$lte": new Date(ano, 11, 31) }
      }
      if (request.query.idComite){
        query.comite = { $eq: Mongoose.Types.ObjectId(request.query.idComite)};
      }
      if (request.query.idVoluntario){
        query.participantes = { $elemMatch: { "voluntarioId": request.query.idVoluntario} };
      }
      DB.acao.aggregate([
        {
          $match: query
        },
        {  $group: {
          _id: null,
          quantidade: { $sum: "$totalBeneficiados" }
            }
          }
        ],
        (err,docs) => {
          if (err) throw err;
          return reply (docs);
        }
      )
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/pessoas-beneficiadas-por-ano',
    config: {
      description: 'Quantidade de ações pessoas beneficiadas por ano',
      validate: {
        query: {
          ano: Joi.number().description('Ano'),
          idComite: Joi.string().optional().description('Id Comite'),
        }
      },
    },
    handler: function (request, reply) {
      const consulta = {}
      const ano = request.query.ano;
      const queryComite = request.query.idComite
                          ? Mongoose.Types.ObjectId(request.query.idComite)
                          : null;
      const query = ano ? { "$gte": new Date(ano, 0, 1 ), "$lte": new Date(ano, 11, 31) }
                        : { "$gte": new Date("2000-01-01T11:41:38-02:00")};

      consulta.dataFinal = query;
      if(queryComite){
        consulta.comite = queryComite;
      }
      consulta.desativada = false;

      DB.acao.aggregate([
        {
          $match: consulta
        },
        {  $group: {
          _id: null,
          quantidade: { $sum: "$totalBeneficiados" }
            }
          }
        ],
        (err,docs) => {
          if (err) throw err;
          return reply (docs);
        }
      )
    }
  });

  server.route({
    method: 'GET',
    path: '/acao/galeria',
    config: {
      description: 'Retorna as fotos de acordo com a qtd informada pelo usuario',
      validate: {
        query: {
          qtd: Joi.number().required().description('Obrigatório informar a qtd de fotos'),
        }
      },
    },
    handler: function (request, reply) {
      const qtd = request.query.qtd ? request.query.qtd : 3;

      DB.acao.aggregate([
        { $match: {galeria: { $exists: true, $not: {$size: 0} }}},
        { $unwind: '$galeria' },
        { $sample: { size: 999 } },
        // { $limit: qtd },
        {
          $group:
            {
              _id: null,
              photos: { $push: "$galeria" },
            }
        },

      ],
      (err,docs) => {
        if (err) throw err;
        var galeria = []
        if (docs[0]) {
          galeria = docs[0].photos.filter(filtroGaleria).map(mapGaleria).slice(0, 3)
        }
        return reply (galeria);
      })
    }
  });

  return next();
}

exports.register.attributes = {
    name: 'acao',
    version: require('../package.json').version
};

function obtemDataAtual() {
  var now = new Date();
  now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));
  return now;
}

function convertDate(value) {
  var now = new Date(value)
  now.setTime(now - (now.getTimezoneOffset() * 60 * 1000));
  return now;
}

function comiteFilter(acao) {
  return acao.comite !== null;
}

function participanteFilter(id, acao) {
  return acao.participantes.filter(function(part) {
    const id1 = part.voluntarioId
    const id2 = id.toString()
    return id1 === id2
  }).length > 0
}

function obtemFiltroSubDoc(filtro, doc) {
  if (filtro[doc]) {
    var retorno = {};
    retorno[doc.split('.')[1]] = filtro[doc]
    return retorno
  }
  else
    return {}
}

function filtroGaleria(galeria) {
  return galeria.fotoDestaque
}

function mapGaleria(galeria) {
  return galeria.urlFoto
}

function normalizaPartipantes(antigo, novo, req, callback) {

  const removidos = reject(antigo, (element) => {
      return novo.filter((el) => el.voluntarioId === element.voluntarioId).length > 0
  }) // array com voluntarios que foram removidos da ação

  const inseridos = reject(novo, (element) => {
      return antigo.filter((el) => el.voluntarioId === element.voluntarioId).length > 0
  }) // array com voluntarios que foram inseridos na ação

  ASQ()
  .then((done) => {
    if(!inseridos || inseridos.length < 1) { return done() }

    const participouEsteAno = new Date(req.dataInicial) > new Date() || new Date(req.dataFinal) > new Date()
    const controle = []
    const dadosAlterados = {
      jaParticipouAcao: true,
    }

    if(participouEsteAno) {
      dadosAlterados.jaParticipouAcaoEsseAno = true
      }

    inseridos.forEach(volunt => {
      (function(volunt, participouEsteAno, controle, dadosAlterados, done) {
        DB.voluntario.update(
          { _id: Mongoose.Types.ObjectId(volunt.voluntarioId) },
          { $set: dadosAlterados },
          (err, resp) => {
            if (err) { throw err }
            controle.push(resp)

            if (controle.length === inseridos.length) { return done() }
          })
      })(volunt, participouEsteAno, controle, dadosAlterados, done)

    })

  })
  .then((done) => {
    if(!removidos || removidos.length < 1) { return done() }
    DB.acao.aggregate([
      { $match: { comite: Mongoose.Types.ObjectId(req.comite) } },
      { $unwind: "$participantes" },
      { $project: { dataInicial: 1, dataFinal: 1, participantes: 1, "_id": 0} },
    ],
    (err, doc) => {
      const controle = []
      const diaPrimeiro = new Date((new Date).getFullYear(), 0, 1)

      removidos.forEach(volunt => {
        (function(volunt, controle, doc, done) {
          const instancias = doc.filter(el => el.participantes.voluntarioId === volunt.voluntarioId)
          const jaParticipouAcao = instancias.length > 0
          const jaParticipouAcaoEsseAno = find(doc, (el) => {
            el.participantes.voluntarioId === volunt.voluntarioId && (el.dataInicial > diaPrimeiro || el.dataFinal > diaPrimeiro)
          })
          DB.voluntario.update(
            { _id: Mongoose.Types.ObjectId(volunt.voluntarioId) },
            { $set: { jaParticipouAcao, jaParticipouAcaoEsseAno } },
            (err, resp) => {
              if (err) { throw err }
              controle.push(resp)

              if (controle.length === removidos.length) { return done() }
          })
        })(volunt,controle, doc, done)
      })
    })
  })

  return callback()
}
