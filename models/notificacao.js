module.exports = (Mongoose) => {
  const voluntarioResumido = require('./schemas/voluntarioResumido')(Mongoose);

  var notificacao = new Mongoose.Schema({
    'publicoAlvo':    { type: Array,              },
    'texto':          { type: String,             required: true  },
    'tipo':           { type: String,             required: true  },
    'urlAcaoClique':  { type: String,             required: false },
    'dataCadastro':   { type: Date,               required: true  },
    '__v':            { type: Number,             select: false},
  })

  return Mongoose.model('notificacao', notificacao, 'notificacoes');
}
