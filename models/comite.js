module.exports = (Mongoose) => {
  const voluntarioResumido = require('./schemas/voluntarioResumido')(Mongoose)

  const schema = new Mongoose.Schema({
    'empresa': { type: Mongoose.Schema.ObjectId, ref: 'empresa', required: true },
    'cidade': { type: Mongoose.Schema.ObjectId, required: true },
    'cidadeNome': { type: String },
    'cidadeUF': { type: String },
    'unidade': { type: String },
    'verbaInicial': { type: Number },
    'saldoAtual': { type: Number },
    'verbaExtra': { type: Number },
    'unidadeNormalizada': { type: String },
    'liderSocial': { type: voluntarioResumido },
    '__v': { type: Number, select: false},
  });

  return Mongoose.model('comite', schema, 'comites');
}
