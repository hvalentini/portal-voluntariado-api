var htmlToPDF = require('html5-to-pdf');
var fs = require('fs');
var path = require('path');
var sr = require('simple-random');

/*
*
* Gera pdf a partir de um template html
*  parametros: opstions: object | callback: function
*
*/
module.exports = function (options, callback) {

  var cargaHoraria = ''
  if (options.horas % 1 !== 0) {
    var minutos = 60 * ( options.horas - parseInt(options.horas) )
    cargaHoraria = `${parseInt(options.horas)} horas e ${minutos} minutos`
  } else {
    cargaHoraria = `${parseInt(options.horas)} horas`
  }

  var content = fs
  .readFileSync(`${path.dirname(__dirname)}/templates/certificado.html`)
  .toString()
  .replace(/{nome}/g, options.nome)
  .replace(/{horas}/g, cargaHoraria)
  .replace(/{dataIni}/g, formatDate(options.dataIni))
  .replace(/{dataFim}/g, formatDate(options.dataFim));

  var rd = sr({length: 4});
  var outPath = `${path.dirname(__dirname)}/public/certificates/${rd}-certificado.pdf`;

  if (!fs.existsSync(`${path.dirname(__dirname)}/public/certificates`)) {
    fs.mkdirSync(`${path.dirname(__dirname)}/public/certificates`);
  }


  pdf = new htmlToPDF({
    inputBody: content,
    renderDelay: 5,
    outputPath: outPath,
    include: [
      {
        type: "css",
        filePath: `${path.dirname(__dirname)}/templates/configTemplate.css`,
      }
    ],
    options:{
      printBackground: true,
      marginsType: 1,
      landscape: true
    }
  });

  pdf.build( (error) => {
    return callback(error, outPath);
  });
}

function formatDate(date) {
  var day = date.getUTCDate()
  var month = date.getMonth() + 1
  const year = date.getFullYear()

  day = day > 9 ? day : `0${day}`
  month = month > 9 ? month : `0${month}`
  return `${day}/${month}/${year}`
}
