module.exports = (Mongoose) => {

  const voluntarioResumido = require('./schemas/voluntarioResumido')(Mongoose)

  var foto = new Mongoose.Schema({
    'urlFoto': { type: String },
    'fotoDestaque': { type:Boolean },
    '__v': { type: Number, select: false},
  })

  var acao = new Mongoose.Schema({
    'nome': { type: String, required: true },
    'nomeNormalizado': { type: String, required: true },
    'cadastroRetroativo': { type: Boolean, required: true },
    'acaoPontual': { type: Boolean, required: true },
    'dataInicial': { type: Date },
    'horaInicial': { type: Number, default: 0 },
    'dataFinal': { type: Date },
    'horaFinal': { type: Number, default: 0 },
    'tempoDuracao': { type: Number, default: 0 },
    'descricao': { type: String, required: true },
    'totalBeneficiados': { type: Number, default: 0 },
    'totalParticipantes': { type: Number, default: 0 },
    'totalQueGostaram': { type: Number, default: 0 },
    'totalCartas': { type: Number, default: 0 },
    'valorGasto': { type: Number, default: 0 },
    'dataCadastro': { type: Date },
    'dataEdicao': { type: Date },
    'desativada': { type: Boolean },
    'local': { type: Mongoose.Schema.ObjectId, ref: 'local' },
    'tipoAcao':  { type: Mongoose.Schema.ObjectId, ref: 'tipoDeAcao', required: true },
    'comite': { type: Mongoose.Schema.ObjectId, ref: 'comite', required: true },
    'criadaPor': voluntarioResumido,
    'participantes': [voluntarioResumido],
    'editadoPor': voluntarioResumido,
    'queremParticipar': [voluntarioResumido], //nao manda nas açoes.
    'gostaram': [voluntarioResumido], //nao manda nas açoes
    'galeria': [foto],
    'nomeTipoAcao': { type: String, required: true },
    '__v': { type: Number, select: false},
  })

  return Mongoose.model('acao', acao, 'acoes');
}
