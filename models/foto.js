module.exports = (Mongoose) => {
  var foto = new Mongoose.Schema({
    'foto': { type: String, required: true },
    'exibirPublicamente': { type: Boolean, required: true },
    '__v': { type: Number, select: false},
  });

  return Mongoose.model('foto', foto);
}
